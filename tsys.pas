unit tsys;

{$mode objfpc}{$H+}

interface

uses
  {$ifdef __DELPHI__}
  windows,
  {$else}
  //lclintf,
  LCLType,
  {$endif}
  Classes, SysUtils,  Dialogs, Controls, Process
  //,tstr
  ;


{** @abstract(Функция выводит на десктоп окно с заданным текстом и иконкой уведомления)
    @param(psTroubleDesc основное сообщение)
    @param(psDetail уточняющее сообщение)
    @return(@true, если сообщение успешно выведено, иначе @false)
    @code(notice('Сообщение','Операция завершилась корректно');)}
procedure notice(psTroubleDesc, psDetail : String);

{** @abstract(Функция выводит на десктоп окно с заданным текстом и двумя кнопками и
              предлагает произвести выбор из двух возможных действий.)
    @param(psTroubleDesc основное сообщение)
    @param(psDetail уточняющее сообщение)
    @return(@true, если была нажата кнопка 'Да', иначе @false)
    @code(warnYesNo('Внимание!','Все данные на диске C: будут уничтожены. Продолжить?');)}
function  warnYesNo(psTroubleDesc, psDetail : String) : Boolean;

{** @abstract(Функция выводит на десктоп окно с заданным текстом и иконкой ошибки)
    @param(psTroubleDesc основное сообщение)
    @param(psDetail уточняющее сообщение)
    @return(@true, если сообщение успешно выведено, иначе @false)
    @code(fatalError('Ошибка!','Программа аварийно завершена!'))}
procedure fatalError(psErrorDesc, psDetail : String);

{$I tconst.inc}

implementation

//var ghAppHandle : THandle;

procedure notice(psTroubleDesc, psDetail : String);
begin

  {$ifdef __DELPHI__}
  Notice:=MessageBox(ghAppHandle,PChar(ToSystemCodepage(psTroubleDesc+LF+psDetail)),
    PChar('Внимание!'),ciMB_APPLMODAL OR MB_OK OR MB_ICONINFORMATION)=IDOK;
  {$else}
  MessageDlg(psTroubleDesc,psDetail,mtInformation,[mbOK],'notice');
  {
  MessageDlg(psTroubleDesc,psDetail,
    mtInformation|mtConfirmation|mtCustom|mtError|mtWarning,
    [mbOK,
    mbYes,mbYesToAll,mbYesNo,mbYesNoCancel,mbNo,mbNoToAll,
    mbOKCancel,mbCancel,mbIgnore, mbRetry,mbClose,mbHelp,mbAll,mbAbortRetryIgnore,
    mbAbort]
    ,'warning');
   }
  {$endif}
end;


function warnYesNo(psTroubleDesc, psDetail : String) : Boolean;
begin

  {$ifdef __DELPHI__}
  WarnYesNoEx:=MessageBox(ghAppHandle,PChar(ToSystemCodepage(psTroubleDesc+LF+psDetail)),
    PChar('Внимание!'),ciMB_APPLMODAL OR MB_YESNO OR MB_ICONQUESTION)=IDYes;
  {$else}
  WarnYesNo:=(MessageDlg(psTroubleDesc,psDetail,mtConfirmation,[mbYes,mbNo],'warning')=mrYes);
  {$endif}
end;


procedure fatalError(psErrorDesc, psDetail : String);
begin

  {$ifdef __DELPHI__}
  FatalErrorEx:=MessageBox(ghAppHandle,PChar(ToSystemCodepage(psErrorDesc+LF+psDetail)),
    PChar('Серьезная ошибка'),ciMB_APPLMODAL OR MB_OK OR MB_ICONERROR)=IDOk;
  {$else}
  MessageDlg(psErrorDesc,psDetail,mtError,[mbOK],'error');
  {$endif}
end;


procedure EasyExec(psProgramm, psArguments : String; pblWait : Boolean = False; pblHideWindow : Boolean = False);
{$ifdef __DELPHI__}
var loStartupInfo : TStartupInfo;
    loProcInfo    : TProcessInformation;
{$else}
var loProc : TProcess;
{$endif}
begin

  {$ifdef __DELPHI__}
  //***** Подготовливаем записи loStartupInfo и loProcInfo к использованию
  loProcInfo.hProcess:=0;
  loStartupInfo.cb:=0;
  FillChar(loStartupInfo, SizeOf(loStartupInfo),#0);
  loStartupInfo.cb := SizeOf(loStartupInfo);
  FillChar(loProcInfo, SizeOf(loProcInfo),#0);
  if pblHideWindow then begin

    loStartupInfo.wShowWindow:=SW_HIDE;            //чтобы не было окна
    loStartupInfo.dwFlags:=STARTF_USESHOWWINDOW;   //чтобы не было окна
  end;

  //***** Попытаемся запустить программу
  if CreateProcess(
    PChar(psProgramm),
    PChar(psArguments),
    nil,
    nil,                   { безопасность по умолчанию }
    false,                 { не наследовать хэндлов }
    NORMAL_PRIORITY_CLASS, { флаги создания по умолчанию }
    nil,                   { переменные среды по умолчанию }
    nil,                   { текущая директория по умолчанию }
    loStartupInfo,         { стартовая информация }
    loProcInfo)            { а в эту запись получим информацию о созданом процессе }
  then
  begin

    if pblWait then begin

      //***** Подождем, пока она отработает
      WaitForSingleObject(loProcInfo.hProcess, INFINITE);
    end;

    //***** Убираем мусор
    CloseHandle(loProcInfo.hProcess);
    CloseHandle(loProcInfo.hThread);
    EasyRun:=True;
  end else begin

    //***** Выдаем сообщение об ошибке
    EasyRun:=False;
  end;
  {$else}
  loProc:=TProcess.Create(nil);
  loProc.Executable:=psProgramm;
  loProc.Parameters.Add(psArguments);

  if pblWait then
    loProc.Options:=loProc.Options + [poWaitOnExit];

  if pblHideWindow then
  begin

    loProc.ShowWindow:=swoHide;
    loProc.StartupOptions:=loProc.StartupOptions+[suoUseShowWindow];
  end;
  loProc.Execute;
  FreeAndNil(loProc);
  {$endif}
end;


{
var
  AProcess: TProcess;

// This is where our program starts to run
begin
  // Now we will create the TProcess object, and
  // assign it to the var AProcess.
  AProcess := TProcess.Create(nil);

  // Tell the new AProcess what the command to execute is.
  // Let's use the Free Pascal compiler (i386 version that is)
  AProcess.Executable:= 'ppc386';
  // Pass -h together with ppc386 (so we're executing ppc386 -h):
  AProcess.Parameters.Add('-h');
  // Previous versions of FPC did not yet have .Executable and .Parameters and
  // used the .CommandLine property. While this is still available, it is deprecated
  // so it should not be used.

  // We will define an option for when the program
  // is run. This option will make sure that our program
  // does not continue until the program we will launch
  // has stopped running.                vvvvvvvvvvvvvv
  AProcess.Options := AProcess.Options + [poWaitOnExit];

  // Now let AProcess run the command in .Commandline:
  AProcess.Execute;

  // This is not reached until ppc386 stops running.
  AProcess.Free;
end.
}


begin
  {$ifdef __DELPHI__}
  //ghAppHandle:=Application.Handle;
  {$else}
  //ghAppHandle:=TWin32WidgetSet(WidgetSet).AppHandle;
  {$endif}
end.

