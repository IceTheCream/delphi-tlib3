unit tdb;

interface

{$i profile.inc}
uses db,
     dbtables,
     DBITYPES,
     DBIPROCS,
     DBIERRS,
     forms,SysUtils,Windows, Classes,Registry, Graphics,Menus,
     DateUtils,ShellApi, ComCtrls,DBGrids,
     IBDatabase, IBCustomDataSet, IBQuery, ADODB, StdCtrls, Controls,
     tlib,tstr,twin;


type  TDBMode=(dmInsert,dmUpdate);

      TDBManager = class(TObject)
        private
          msMainSQL        : String;
          msPreparedSQL    : STring;
          msOrderField     : String;
          msOrderDirection : String;
          msDefaultFilter  : String;
          msFilterField    : String;
          msFilterCriteria : String;
          moQuery          : TADOQuery;
          moDataSource     : TDataSource;
          moGrid           : TDBGrid;
          mblFirstOpen     : Boolean;
          masParams        : array[1..16] of String;
          masValues        : array[1..16] of String;
          miParamCount     : Integer;
        public
          function getOrderField : String;
          procedure setDefaultFilter(psFilterLine : String);
          procedure setFilterField(psFilterField : String='');
          procedure setFilterCriteria(psFilterCriteria : String='');
          procedure setFilter(psFilterField : String; psFilterCriteria : String='');
          procedure setGrid(poGrid : TDBGrid);
          procedure setMainSQL(psSQL : String);
          procedure setOrderField(psOrderField : String);
          procedure setOrderDirection(pblDirection : Boolean = True);
          procedure setQuery(poQuery : TADOQuery);
          procedure addParameter(psParam,psValue : String);
          function  ChangeParameter(psParam,psValue : String) : Boolean;
          procedure ResetColumnTitles;
          procedure InitialSetColumnTitle;
          procedure FillPopUpMenuItems(poMenu : TPopUpMenu; poOnClick: TNotifyEvent);
          procedure Open;
          procedure Close;
          procedure Refresh;
          procedure Prepare;
          procedure Reorder(poColumn : TColumn);
          procedure UnplugGrid;
          procedure PlugGrid;
          constructor Create(psSQL : String; poQuery: TADOQuery;poGrid : TDBGrid = Nil);
          destructor Destroy; override;
      end;



const c_eEmptyStyle          = [];
      c_eSortAscendingStyle  = [fsBold];
      c_eSortDescendingStyle = [fsUnderline]; //fsStrikeOut

      ciSelectSQLIdx         = 1;
      ciInsertSQLIdx         = 2;
      ciUpdateSQLIdx         = 3;
      ciDeleteSQLIdx         = 4;
      ciCheckSQLIdx          = 5;

type TSQLArray = array[1..ciCheckSQLIdx] of String;


function  BDECopyRecord(p_oSrcTable,p_oDstTable : TDataset) : Boolean;
function  BDETryOpenTable(p_oTable : TTable; p_sTablePath, p_sTableName : String) : Boolean;
function  BDETryOpenQuery(p_oQuery : TQuery; p_sTablePath : String) : Boolean;
function  BDEQueryInitialize(p_oQuery : TQuery;p_sSQL : String; p_sTablePath : String='') : Boolean;
function  BDEInitAndTryOpenQuery(p_oQuery : TQuery; p_sSQL, p_sTablePath : String) : Boolean;
function  BDEFail(p_sMsg : String) : Boolean;
procedure BDERefreshQuery(p_oQuery: TQuery; p_blFlag: boolean; p_blRestPos : Boolean = True);
function  BDEGetLastID(p_oDataSet : TDataSet; p_sField : String = 'ID'):LongInt;
function  BDESetIDIncrement(p_oDataSet : TDataSet;var p_iID : LongInt; p_sField : String = 'ID') : Boolean;
function  BDECheckTableForID(p_oTable : TDataSet;p_sName : String = '') : Boolean;
function  BDEIsInstalled : Boolean;
function  BDERecodeTable(p_oTable : TTable;p_eRecodeType : TRecodeType; p_oProgress : TProgressBar=Nil) : Boolean;
function  BDEUnloadTable(p_oTable : TTable; p_sDstPath : String;p_eRecodeType : TRecodeType; p_oProgress : TProgressBar=Nil) : Boolean;
function  BDESyncTable(p_oTable : TTable; p_oQuery : TQuery) : Boolean; //.
function  BDEReplaceRecord(p_oSrc,p_oDst : TDataset) : Boolean;
function  BDEEasySeek(const p_oTable: TTable; const p_sValue: string): boolean;
function  BDEPackTable(p_oTable : TTable; p_sTablePath,p_sTableName : String; p_blLeaveOpen:Boolean=False) : Boolean;
function  BDECompareRecord(p_oSrcTable,p_oDstTable : TDataset) : Boolean;
function  DBFSetCodepageByte(p_sName : String; p_btCodepage : Byte=0) : Boolean;
function  DBFClearIndexFlag(p_sName : String) : Boolean;

function  IBCreateTransaction(var p_oIBTransaction : TIBTransaction;
                              p_oIBDataBase : TIBDataBase = nil) : Boolean;
function  IBCreateDataBase(var p_oIBDataBase    : TIBDataBase;
                           p_oIBTransaction : TIBTransaction;
                           p_sIBServer,
                           p_sIBBasePath,
                           p_sIBUSer : String;
                           p_iIBPass : Integer) : Boolean;
function  IBCreateDataSet(var p_oIBDataSet : TIBDataSet;
                          p_oIBDataBase : TIBDataBase;
                          p_oIBTRansaction : TIBTransaction;
                          p_sSelectSQL,
                          p_sInsertSQL,
                          p_sModifySQL,
                          p_sDeleteSQL,
                          p_sRefreshSQL : String) : Boolean;
function  IBTryOpenQuery(p_oQuery : TIBQuery) : Boolean;

procedure ADORefreshQuery(p_oQuery: TADOQuery; p_blRestPos : Boolean = True);
function  ADOQueryInitialize(p_oQuery : TADOQuery;p_sSQL : String) : Boolean;
function  ADOBuildConnectionString(psDriver,psDataBase,psDataSource,psUserID,psPassword : String) : String;
function  ADOGetCurrentID(poQuery : TADOQuery; psGenName : String) : Integer;

function  SQLiteODBCCreate(p_sRecordName,p_sDataBase : String) : Boolean;
function FireBirdODBCCreate(psRecordName,psDataBase,psUser,psPassword,psClientDLL: String; p_blEnforce : Boolean=False) : Boolean;

function PushBookMark(p_oDataSet : TDataSet) : Boolean;
function PopBookMark(p_oDataSet : TDataSet): Boolean;
function QueryInitialize(p_oQuery : TQuery;p_sSQL : String) : Boolean;
function FindColumn(p_sFieldName : String;p_oGrid : TDBGrid) : TColumn;

implementation

uses tvcl;

const c_iMaxBookmarks=32;

var u_aoBookmarks : array[1..c_iMaxBookmarks] of TBookmark;
    u_iStackPtr   : Integer;

{===<< BDE >>===}
function BDECopyRecord(p_oSrcTable,p_oDstTable : TDataset) : Boolean;
var l_blExcept : Boolean;
    l_iIdx     : Integer;
begin
  BDECopyRecord:=False;
  l_blExcept:=False;

  {---< ���� ������? >---}
  if (p_oSrcTable.State=dsBrowse) and (p_oDstTable.State=dsBrowse) then begin

    {---< ������� ������ >---}
    try
      p_oDstTable.Append;
    except
      on E: EDBEngineError do l_blExcept:=BDEFail(E.Message);
    end;

    {---< ������? >---}
    if (p_oDstTable.State=dsInsert) and not l_blExcept then begin

      {---< ������ ���� �� ����� ���������� ������ >---}
      l_iIdx:=0;
      while (l_iIdx<=Pred(p_oSrcTable.FieldCount)) and not l_blExcept do begin

        try
          p_oDstTable.Fields[l_iIdx].AsString:=p_oSrcTable.Fields[l_iIdx].AsString;
        except
          on E: EDBEngineError do l_blExcept:=not BDEFail(E.Message+' � ���� '+p_oDSTTable.Fields[l_iIdx].DisplayName);
        end;

        inc(l_iIdx);
      end; {- while (l_iIdx<=Pred(p_oSrc.FieldCount)) and not l_blExcept do begin -}

      {---< �������� ������ >---}
      try
        p_oDstTable.Post;
      except
        on E: EDBEngineError do l_blExcept:=BDEFail(E.Message);
      end;
      {---< ������? >---}
      BDECopyRecord:=(p_oDstTable.State=dsBrowse) and not l_blExcept;
    end; {- if (p_oDst.State=dsEdit) and not l_blExcept then begin -}
  end; {- if (p_oSrc.State=dsBrowse) and (p_oDst.State=dsBrowse) then begin -}
end; {- function CopyRec(p_oSrc,p_oDst : TDataset) : Boolean; -}


function BDETryOpenTable(p_oTable : TTable; p_sTablePath, p_sTableName : String) : Boolean;
var l_blOk : Boolean;
begin
  SlashIt(p_sTablePath);
  p_oTable.DatabaseName:=p_sTablePath;
  p_oTable.TableName:=p_sTableName;
  l_blOk:=FileExists(p_sTablePath+p_sTableName);
  if l_blOk then begin
    try
      p_oTable.Open;
    except
      on E: EDBEngineError do
        l_blOk:=BDEFail(E.Message);
    end; {- try -}
  end; {- if l_blOk then begin -}
  BDETryOpenTable:=l_blOk;
end; {- f BDETryOpenTable(p_oTable : TTable; p_sTablePath, p_sTableName : String) : Boolean; -}


function BDETryOpenQuery(p_oQuery : TQuery; p_sTablePath : String) : Boolean;
var l_blOk : Boolean;
begin
  l_blOk:=True;
  SlashIt(p_sTablePath);
  p_oQuery.DatabaseName:=p_sTablePath;
  try
    p_oQuery.Open;
  except
    on E: EDBEngineError do l_blOk:=BDEFail(E.Message);
  end;{- try -}
  BDETryOpenQuery:=l_blOk;
end; {- f BDETryOpenQuery(p_oQuery : TQuery; p_sTablePath : String) : Boolean; -}


function BDEQueryInitialize(p_oQuery : TQuery;p_sSQL : String; p_sTablePath : String='') : Boolean;
begin
  Result:=True;
  SlashIt(p_sTablePath);
  p_oQuery.DatabaseName:=p_sTablePath;
  if p_oQuery.State<>dsInactive then
    p_oQuery.Close;
  p_oQuery.SQL.Clear;
  if p_oQuery.ParamCount>0 then
    p_oQuery.Params.Clear;
  p_oQuery.SQL.Add(p_sSQL);
end;


function BDEInitAndTryOpenQuery(p_oQuery : TQuery; p_sSQL, p_sTablePath : String) : Boolean;
begin
  if p_oQuery.State<>dsInactive then
    p_oQuery.Close;
  
  BDEQueryInitialize(p_oQuery,p_sSQL);
  BDEInitAndTryOpenQuery:=BDETryOpenQuery(p_oQuery,p_sTablePath);
end; {- f BDETryOpenQuery(p_oQuery : TQuery; p_sTablePath : String) : Boolean; -}


function BDEFail(p_sMsg : String) : Boolean;
begin
  FatalError('��� ������ � �� �������� ������. �������� ������:',p_sMsg);
  Result:=False;
end; {- f BDEFail(p_sMsg : String) : Boolean; -}


procedure BDERefreshQuery(p_oQuery: TQuery; p_blFlag: boolean; p_blRestPos : Boolean = True);
var l_oMark: TBookMark;
begin
  with p_oQuery do begin
    if Active then begin
      l_oMark:=GetBookMark;
      try
        Close;
        Unprepare;
        Active:=True;
        if p_blFlag then begin
          try
            if p_blRestPos then
              GotoBookMark(l_oMark)
          except
            on EDatabaseError do First;
          end; {- try -}
        end else
          First;
      finally
        FreeBookmark(l_oMark);
      end; {- try -}
    end;{- if Active then begin -}
  end; {- with p_oQuery do begin -}
end; {- p BDERefreshQuery(p_oQuery: TQuery; p_blFlag: boolean; p_blRestPos : Boolean = True); -}


function BDEGetLastID(p_oDataSet : TDataSet; p_sField : String = 'ID'):LongInt;
begin
  p_oDataSet.DisableControls;
  with p_oDataSet do begin
    if RecordCount>0 then begin
      p_oDataSet.Last;
      Result:=p_oDataSet.FieldByName(p_sField).AsInteger;
      p_oDataSet.First;
    end else begin
      BDEGetLastID:=0;
    end; {- if RecordCount>0 then else -}
  end; {- with p_oDataSet do begin-}
  p_oDataSet.EnableControls;
end; {- f BDEGetLastID(p_oDataSet : TDataSet; p_sField : String = 'ID'):LongInt; -}


function BDESetIDIncrement(p_oDataSet : TDataSet;var p_iID : LongInt; p_sField : String = 'ID') : Boolean;
begin
  BDESetIDIncrement:=True;
  Inc(p_iID);
  p_oDataSet.FieldByName(p_sField).AsInteger:=p_iID;
end; {- f BDESetIDIncrement(p_oDataSet : TDataSet;var p_iID : LongInt; p_sField : String = 'ID') : Boolean; -}


function BDECheckTableForID(p_oTable : TDataSet;p_sName : String = '') : Boolean;
var l_oMark   : TBookmark;
    l_lLastID : LongInt;
    l_lRecNum : LongInt;
begin
  BDECheckTableForID:=True;
  l_lRecNum:=1;
  p_oTable.DisableControls;
  if p_oTable.State=dsBrowse then begin
    l_oMark:=p_oTable.GetBookmark;
    p_oTable.First;
    l_lLastID:=-1;
    while not p_oTable.Eof do begin
      if l_lLastID>=p_oTable.FieldByName('ID').AsInteger then begin

        if p_oTable is TTable then
          FatalError('��������!!! ��������� ����'#13+' � ������� '+
            TTable(p_oTable).TableName+' � ������ N '+IntToStr(l_lRecNum)+#13,
            '���������� � ������������.')
        else
          FatalError('��������!!! ��������� ����'#13+' � ������� '+
            p_sName+' � ������ N '+IntToStr(l_lRecNum)+#13,
            '���������� � ������������.');
        Result:=False;
        break;
      end else begin
        l_lLastID:=p_oTable.FieldByName('ID').AsInteger;
      end; {- if l_lLastID>=p_oTable.FieldByName('ID').AsInteger then else -}
      p_oTable.Next;
      Inc(l_lRecNum);
    end; {- while not p_oTable.Eof do begin -}
    p_oTable.GotoBookmark(l_oMark);
  end; {- if p_oTable.State=dsBrowse then begin -}
  p_oTable.EnableControls;
end; {- f BDECheckTableForID(p_oTable : TDataSet;p_sName : String = '') : Boolean; -}


function BDEIsAliasExists(p_sAlias : String) : Boolean;
var l_oAliases : TStringList;
    l_iIdx     : Integer;
begin
  l_oAliases:=TStringList.Create;
  Session.GetAliasNames(l_oAliases);
  Result:=False;
  for l_iIdx:=0 to Pred(l_oAliases.Count) do begin
    if l_oAliases[l_iIdx]=p_sAlias then begin
      Result:=True;
      break;
    end; {- if l_oAliases[l_iIdx]=p_sAlias then begin -}
  end; {- for l_iIdx:=0 to Pred(l_oAliases.Count) do begin -}
  FreeAndNil(l_oAliases);
end; {- f BDEIsAliasExists(p_sAlias : String) : Boolean; -}


function BDEIsInstalled : Boolean;
begin
  BDEIsInstalled:=False;
  with TRegistry.create do begin
    Rootkey := HKEY_LOCAL_MACHINE;
    try
      OpenKey('SOFTWARE\BORLAND\DATABASE ENGINE', false);
    finally
      CloseKey;
    end; {- try -}
    Free;
  end; {- with TRegistry.create do -}
end; {-f BDEIsInstalled : Boolean; -}


function BDERecodeTable(p_oTable : TTable;p_eRecodeType : TRecodeType; p_oProgress : TProgressBar=Nil) : Boolean;
var l_iIdx : Integer;
    l_blOk : Boolean;
begin
  if p_oTable.State=dsInactive then begin
    Result:=False;
  end else begin
    p_oTable.First;
    l_blOk:=True;
    while not p_oTable.Eof and l_blOk do begin
      if p_eRecodeType=rtAnsi2Oem then begin
        try
          p_oTable.Edit;
          for l_iIdx:=0 to Pred(p_oTable.Fields.Count) do begin
            if p_oTable.Fields[l_iIdx].DataType=ftString then begin
              p_oTable.Fields[l_iIdx].AsString:=ANSI2OEM(p_oTable.Fields[l_iIdx].AsString);
            end; {- if p_oTable.Fields[l_iIdx].DataType=ftString then begin -}
          end; {- for l_iIdx:=0 to Pred(p_oTable.Fields.Count) do begin -}
          p_oTable.Post;
        except
          FatalError('������ ������������� ������� '+p_oTable.Tablename+'!','�������� ����������');
          l_blOk:=False;
        end; {- try -}
      end; {- if p_eRecodeType=rtAnsi2Oem then begin -}
      p_oTable.Next;
      if p_oProgress<>Nil then
        p_oProgress.StepIt;
    end; {- while not p_oTable.Eof and l_blOk do begin -}
    Result:=l_blOk;
  end; {- if p_oTable.State=dsInactive then else -}
end; {- f BDERecodeTable(p_oTable : TTable;p_eRecodeType : TRecodeType; p_oProgress : TProgressBar=Nil) : Boolean; -}


function BDEUnloadTable(p_oTable : TTable; p_sDstPath : String;p_eRecodeType : TRecodeType; p_oProgress : TProgressBar=Nil) : Boolean;
var l_sSrcPath,
    l_sSrcName : String;
    l_oTable   : TTable;
begin
  BDEUnloadTable:=False;
  l_sSrcPath:=p_oTable.DatabaseName;
  SlashIt(l_sSrcPath);
  l_sSrcName:=p_oTable.Tablename;
  SlashIt(p_sDstPath);

  {---< ������� ������� >---}
  p_oTable.DisableControls;
  if p_oTable.State<>dsInactive then
    p_oTable.Close;

  {---< ������� ������� ������� >---}
  if FileExists(p_sDstPath+l_sSrcName) then
    SysUtils.DeleteFile(p_sDstPath+l_sSrcName);

  {---< ��������� ����������� ������� >---}
  if not EasyCopyFile(PChar(l_sSrcPath+l_sSrcName),PChar(p_sDstPath+l_sSrcName)) then begin
    FatalError('������ ����������� �����'#13+
    l_sSrcPath+l_sSrcName+' � '+p_sDstPath+l_sSrcName,'�������� ����������.')
  end else begin

    {---< ��������� ������������� ������� >---}
    l_oTable:=TTable.Create(Nil);
    l_oTable.Exclusive:=True;
    BDETryOpenTable(l_oTable,p_sDstPath,l_sSrcName);

    {---< ����������� ������� >---}
    Check(DbiPackTable(l_oTable.DbHandle, l_oTable.Handle, Nil, szDBASE, TRUE));

    if l_oTable.RecordCount >0 then begin

      if p_oProgress<>Nil then
      begin
        p_oProgress.Min:=0;
        p_oProgress.Position:=p_oProgress.Min;
        p_oProgress.Max:=l_oTable.RecordCount;
      end;

      {---< ������������ ������� >---}
      BDEUnloadTable:=BDERecodeTable(l_oTable,p_eRecodeType,p_oProgress);
    end else begin
      BDEUnloadTable:=True;
    end; {- if l_oTable.RecordCount >0 then else -}

    l_oTable.Close;
    FreeAndNil(l_oTable);

  end; {- if not EasyCopyFile(PChar(l_sSrcPath+l_sSrcName),PChar(p_sDstPath+l_sSrcName)) then -}

  {---< ��������� �������� ������� >---}
  try
    p_oTable.Open;
  except
    on E: EDBEngineError do BDEFail(E.Message);
  end; {- try -}
end; {- f BDEUnloadTable(p_oTable : TTable; p_sDstPath : String;p_eRecodeType : TRecodeType; p_oProgress : TProgressBar=Nil) : Boolean; -}


function BDESyncTable(p_oTable : TTable; p_oQuery : TQuery) : Boolean; //.
begin
  if p_oTable.State<>dsInactive then
  begin
    BDESyncTable:=p_oTable.Locate('ID',p_oQuery.FieldByName('ID').AsInteger,[])
  end else begin
    BDESyncTable:=False;
  end; {- if p_oTable.State<>dsInactive then -}
end; {- f BDESyncTable(p_oTable : TTable; p_oQuery : TQuery) : Boolean; -}


function BDEPackTable(p_oTable : TTable; p_sTablePath,p_sTableName : String; p_blLeaveOpen:Boolean=False) : Boolean;
var l_blOpen : Boolean;
    l_blResult : Boolean;
begin
  //l_blResult:=True;
  l_blOpen:=p_oTable.Active;
  if l_blOpen then
    p_oTable.Close;
  p_oTable.Exclusive:=True;
  l_blResult:=BDETryOpenTable(p_oTable,p_sTablePath,p_sTableName);
  if l_blResult then begin
    Check(DbiPackTable(p_oTable.DbHandle, p_oTable.Handle, Nil, szDBASE, TRUE));
    p_oTable.Close;
    p_oTable.Exclusive:=False;
    if l_blOpen or p_blLeaveOpen then
      l_blResult:=BDETryOpenTable(p_oTable,p_sTablePath,p_sTableName);
  end; {- if BDEPackTable then -}
  BDEPackTable:=l_blResult;
end; {- f BDEPackTable(p_oTable : TTable; p_sTablePath,p_sTableName : String; p_blLeaveOpen:Boolean=False) : Boolean; -}


function BDEReplaceRecord(p_oSrc,p_oDst : TDataset) : Boolean;
var l_blExcept : Boolean;
    l_iIdx     : Integer;
begin
  Result:=False;
  l_blExcept:=False;

  {---< ���� ������? >---}
  if (p_oSrc.State=dsBrowse) and (p_oDst.State=dsBrowse) then begin

    {---< ������� ������ >---}
    try
      p_oDst.Edit;
    except
      on E: EDBEngineError do l_blExcept:=BDEFail(E.Message);
    end;

    {---< ������? >---}
    if (p_oDst.State=dsEdit) and not l_blExcept then begin

      {---< ������ ���� �� ����� ���������� ������ >---}
      l_iIdx:=0;
      while (l_iIdx<=Pred(p_oSrc.FieldCount)) and not l_blExcept do begin
        try
          p_oDst.Fields[l_iIdx].AsString:=p_oSrc.Fields[l_iIdx].AsString;
        except
          on E: EDBEngineError do l_blExcept:=not BDEFail(E.Message+' � ���� '+p_oDST.Fields[l_iIdx].DisplayName);
        end;

        inc(l_iIdx);
      end; {- while (l_iIdx<=Pred(p_oSrc.FieldCount)) and not l_blExcept do begin -}

      {---< �������� ������ >---}
      try
        p_oDst.Post;
      except
        on E: EDBEngineError do l_blExcept:=BDEFail(E.Message);
      end;
      {---< ������? >---}
      Result:=(p_oDst.State=dsBrowse) and not l_blExcept;
    end; {- if (p_oDst.State=dsEdit) and not l_blExcept then begin -}
  end; {- if (p_oSrc.State=dsBrowse) and (p_oDst.State=dsBrowse) then begin -}
end; {- function ReplRec(p_oSrc,p_oDst : TDataset) : Boolean; -}


function BDECompareRecord(p_oSrcTable,p_oDstTable : TDataset) : Boolean;
var l_iIdx     : Integer;
    l_blDiff   : Boolean;
begin
  l_blDiff:=False;

  {---< ���� ������? >---}
  if (p_oSrcTable.State=dsBrowse) and (p_oDstTable.State=dsBrowse) then begin

    {---< ������ ���� �� ����� ���������� ������ >---}
    l_iIdx:=0;
    while (l_iIdx<=Pred(p_oSrcTable.FieldCount)) and not l_blDiff do begin
      l_blDiff:=p_oSrcTable.Fields[l_iIdx].AsString<>p_oDstTable.Fields[l_iIdx].AsString;
      inc(l_iIdx);
    end; {- while (l_iIdx<=Pred(p_oSrc.FieldCount)) and not l_blExcept do begin -}
  end;
  BDECompareRecord:=l_blDiff;
end;


function DBFClearIndexFlag(p_sName : String) : Boolean;
var l_oFile : File of byte;
    l_btFlag : Byte;
begin
  DBFClearIndexFlag:=False;
  AssignFile(l_oFile,p_sName);
  {$I-}
  Reset(l_oFile);
  if not IsIOError then begin
    Seek(l_oFile,28);
    if not IsIOError then begin
      l_btFlag:=0;
      write(l_oFile,l_btFlag);
      DBFClearIndexFlag:=not IsIOError;
    end; {- if not IsIOError then begin -}
    CloseFile(l_oFile);
  end; {- if not IsIOError then begin -}
end; {- f DBFClearIndexFlag(p_sName : String) : Boolean; -}


function DBFSetCodepageByte(p_sName : String; p_btCodepage : Byte=0) : Boolean;
var l_oFile : File of byte;
begin
  DBFSetCodePageByte:=False;
  AssignFile(l_oFile,p_sName);
  {$I-}
  Reset(l_oFile);
  if not IsIOError then begin
    Seek(l_oFile,29);
    if not IsIOError then begin
      write(l_oFile,p_btCodepage);
      DBFSetCodePageByte:=not IsIOError;
    end; {- if not IsIOError then begin -}
    CloseFile(l_oFile);
  end; {- if not IsIOError then begin -}
end; {- f DBFClearIndexFlag(p_sName : String) : Boolean; -}


{---< ������� ����� �� DelphiWorld 6 >---}
function BDEEasySeek(const p_oTable: TTable; const p_sValue: string): boolean;
var l_sExpValue: DBIKEYEXP;
begin
  Result := False;
  with p_oTable do begin
    if (Active) and (Length(IndexName) > 0) then begin
      StrPCopy(l_sExpValue,p_sValue);
      if (DbiGetRecordForKey(Handle, True, 0, strlen(l_sExpValue), @l_sExpValue, nil) = DBIERR_NONE) then
        Result := True
    end; {- if (Active) and (Length(IndexName) > 0) then begin -}
  end; {- with p_oTable do begin -}
end; {- f BDEEasySeek(const p_oTable: TTable; const p_sValue: string): boolean; -}


function DBFHasIndexFlag(p_sName : String) : Boolean;
var l_oFile : File of byte;
    l_btFlag : Byte;
begin
  DBFHasIndexFlag:=False;
  AssignFile(l_oFile,p_sName);
  {$I-}
  Reset(l_oFile);
  if not IsIOError then begin
    Seek(l_oFile,28);
    if not IsIOError then begin
      l_btFlag:=0;
      Read(l_oFile,l_btFlag);
      DBFHasIndexFlag:=not IsIOError and (l_btFlag=1);
    end; {- if not IsIOError then begin -}
    CloseFile(l_oFile);
  end; {- if not IsIOError then begin -}
end; {- f DBFHasIndexFlag(p_sName : String) : Boolean; -}


function IBCreateTransaction(var p_oIBTransaction : TIBTransaction;
                             p_oIBDataBase : TIBDataBase = nil) : Boolean;
begin
  IBCreateTransaction:=True;
  try
    p_oIBTransaction:=TIBTransaction.Create(Nil);
    p_oIBTransaction.AutoStopAction:=saCommit;
    p_oIBTransaction.Params.Clear;
    if p_oIBDataBase<>Nil then
      p_oIBTransaction.DefaultDatabase:=p_oIBDataBase;
    //p_oIBTransaction.Params.Add('isc_tpb_no_wait');
    p_oIBTransaction.Params.Add('isc_tpb_read_committed');
    //p_oIBTransaction.Params.Add('isc_tpb_rec_version');
    //p_oIBTransaction.Params.Text := 'isc_tpb_read_committed '+#13+
    //                                'isc_tpb_rec_version '+#13+
    //                               'isc_tpb_nowait';
  except
    IBCreateTransaction:=False;
  end;
end;


function IBCreateDataBase(var p_oIBDataBase    : TIBDataBase;
                          p_oIBTransaction : TIBTransaction;
                          p_sIBServer,
                          p_sIBBasePath,
                          p_sIBUSer : String;
                          p_iIBPass : Integer) : Boolean;
begin
  try
    p_oIBDataBase:=TIBDataBase.Create(Nil);
    with p_oIBDataBase do
    begin
      Params.Clear;
      SQLDialect := 3;
      Params.Add('user_name='+p_sIBUser);
      Params.Add('password='+IntToStr(p_iIBPass));
      Params.Add('lc_ctype=WIN1251');
      LoginPrompt:=False;
      DatabaseName:=p_sIBServer+':'+p_sIBBasePath;
      p_oIBDataBase.Connected:=True;
      if p_oIBTransaction<>Nil then
        p_oIBDataBase.DefaultTransaction:=p_oIBTransaction;
      p_oIBDataBase.Open;
      IBCreateDataBase:=p_oIBDataBase.Connected;
    end;
  except
    IBCreateDataBase:=False;
  end;
end;


function IBCreateDataSet(var p_oIBDataSet : TIBDataSet;
                         p_oIBDataBase : TIBDataBase;
                         p_oIBTRansaction : TIBTransaction;
                         p_sSelectSQL,
                         p_sInsertSQL,
                         p_sModifySQL,
                         p_sDeleteSQL,
                         p_sRefreshSQL : String) : Boolean;
begin
  IBCreateDataSet:=True;
  try
    p_oIBDataSet:=TIBDataSet.Create(Nil);
    p_oIBDataSet.Database:=p_oIBDataBase;
    if p_oIBTransaction<>Nil then
      p_oIBDataSet.Transaction:=p_oIBTRansaction;
    p_oIBDataSet.SelectSQL.Add(p_sSelectSQL);
    p_oIBDataSet.InsertSQL.Add(p_sInsertSql);
    p_oIBDataSet.ModifySQL.Add(p_sModifySQL);
    p_oIBDataSet.DeleteSQL.Add(p_sDeleteSQL);
    p_oIBDataSet.RefreshSQL.Add(p_sRefreshSQL);
  except
    IBCreateDataSet:=False;
  end;
end;


function IBTryOpenQuery(p_oQuery : TIBQuery) : Boolean;
//var l_blOk : Boolean;
begin
  IBTryOpenQuery:=True;
  //l_blOk:=True;
  try
    p_oQuery.Open;
  except
    IBTryOpenQuery:=False;
    // EIBClientError
    // EIBInterBaseError
    // EIBInterbaseRoleError
    //on E: EDBEngineError do l_blOk:=BDEFail(E.Message);
  end;
end;


procedure ADORefreshQuery(p_oQuery: TADOQuery; p_blRestPos : Boolean = True);
var l_oMark: TBookMark;
begin
  with p_oQuery do begin
    if Active then begin
      l_oMark:=GetBookMark;
      try
        Close;
        //Unprepare;
        Active:=True;
          if p_blRestPos then begin
          try
            GotoBookMark(l_oMark)
          except
            on EDatabaseError do First;
          end; {- try -}
        end else
          First;
      finally
        FreeBookmark(l_oMark);
      end; {- try -}
    end;{- if Active then begin -}
  end; {- with p_oQuery do begin -}
end; {- p BDERefreshQuery(p_oQuery: TQuery; p_blFlag: boolean; p_blRestPos : Boolean = True); -}


function ADOQueryInitialize(p_oQuery : TADOQuery;p_sSQL : String) : Boolean;
begin
  Result:=True;
  if p_oQuery.State<>dsInactive then
    p_oQuery.Close;
  p_oQuery.SQL.Clear;
  if p_oQuery.Parameters.Count>0 then
    p_oQuery.Parameters.Clear;
  p_oQuery.SQL.Add(p_sSQL);
end;


function ADOBuildConnectionString(psDriver,psDataBase,psDataSource,psUserID,psPassword : String) : String;
begin
  Result:=psDriver;
  if not IsEmpty(psDataSource) then
    Result:=Result+'Data Source='+psDataSource+';';
  if not IsEmpty(psDataBase) then
    Result:=Result+'Initial Catalog='+psDatabase+';';
  if not IsEmpty(psUserID) then
    Result:=Result+'User ID='+psUserID+';';
  if not IsEmpty(psPassword) then
    Result:=Result+'Password='+psPassword+';';
end;


function ADOGetCurrentID(poQuery : TADOQuery; psGenName : String) : Integer;
begin

  ADOQueryInitialize(poQuery,
    'SELECT GEN_ID('+psGenName+',0) as anewid from rdb$database');
  poQuery.Open;
  Result:=poQuery.FieldByName('anewid').AsInteger;
  poQuery.Close;
end;


function SqliteODBCCreate(p_sRecordName,p_sDataBase : String) : Boolean;
var l_oReg : TRegistry;
begin
  Result:=True;
  try
    l_oReg:=TRegistry.create;
    l_oReg.RootKey:=HKEY_CURRENT_USER;
    if not l_oReg.KeyExists('Software\ODBC\ODBC.INI\'+p_sRecordName) then
    begin

      l_oReg.OpenKey('Software\ODBC\ODBC.INI\ODBC Data Sources',true);
      l_oReg.WriteString(p_sRecordName,'SQLite3 ODBC Driver');
      l_oReg.CloseKey;

      l_oReg.OpenKey('Software\ODBC\ODBC.INI\'+p_sRecordName,true);
      l_oReg.WriteString('Driver', 'C:\windows\system32\sqlite3odbc.dll');
      l_oReg.WriteString('Description', '��� ����� ������������ ������');
      l_oReg.WriteString('Database',p_sDataBase);
      l_oReg.WriteString('TimeOut','');
      l_oReg.WriteString('StepAPI','0');
      l_oReg.WriteString('SyncPragma','NORMAL');
      l_oReg.WriteString('NoTXN','0');
      l_oReg.WriteString('ShortNames','0');
      l_oReg.WriteString('LongNames','0');
      l_oReg.WriteString('NoCreat','0');
      l_oReg.WriteString('NoWCHAR','0');
      l_oReg.WriteString('LoadExt','');
      l_oReg.CloseKey;
    end
    else
    begin
      l_oReg.OpenKey('Software\ODBC\ODBC.INI\'+p_sRecordName,true);
      l_oReg.WriteString('Database', p_sDataBase);
      l_oReg.CloseKey;

    end;
    FreeAndNil(l_oReg);
  except
    Result:=False;
  end;
end;


function FireBirdODBCCreate(psRecordName,psDataBase,psUser,psPassword,psClientDLL: String; p_blEnforce : Boolean=False) : Boolean;
var loReg : TRegistry;
begin
  Result:=True;
  try
    loReg:=TRegistry.create;
    loReg.RootKey:=HKEY_CURRENT_USER;
    if p_blEnforce or not loReg.KeyExists('Software\ODBC\ODBC.INI\'+psRecordName) then
    begin

      loReg.OpenKey('Software\ODBC\ODBC.INI\ODBC Data Sources',true);
      loReg.WriteString(psRecordName,'Firebird/InterBase(r) driver');
      loReg.CloseKey;

      loReg.OpenKey('Software\ODBC\ODBC.INI\'+psRecordName,true);
      loReg.WriteString('Driver','C:\\WINDOWS\\system32\\OdbcFb.dll');
      loReg.WriteString('Description', '');
      loReg.WriteString('Dbname',psDataBase);
      loReg.WriteString('Client',psClientDLL);
      loReg.WriteString('User',psUser);
      loReg.WriteString('Role','');
      loReg.WriteString('CharacterSet','WIN1251');
      loReg.WriteString('JdbcDriver','IscDbc');
      loReg.WriteString('ReadOnly','N');
      loReg.WriteString('NoWait','N');
      loReg.WriteString('LockTimeoutWaitTransactions','');
      loReg.WriteString('Dialect','3');
      loReg.WriteString('QuotedIdentifier','Y');
      loReg.WriteString('SensitiveIdentifier','N');
      loReg.WriteString('AutoQuotedIdentifier','N');
      loReg.WriteString('UseSchemaIdentifier','0');
      loReg.WriteString('SafeThread','Y');
      loReg.WriteString('Password',psPassword);
      loReg.CloseKey;
    end
    else
    begin
      loReg.OpenKey('Software\ODBC\ODBC.INI\'+psRecordName,true);
      loReg.WriteString('DbName', psDataBase);
      loReg.CloseKey;
    end;
    FreeAndNil(loReg);
  except
    Result:=False;
  end;
end;


function FindColumn(p_sFieldName : String;p_oGrid : TDBGrid) : TColumn;
var l_iIdx : Integer;
begin
  Result:=Nil;
  for l_iIdx:=0 to Pred(p_oGrid.Columns.Count) do
  begin
    if IsEqualString(p_sFieldName,p_oGrid.Columns[l_iIdx].FieldName) then
    begin
      Result:=p_oGrid.Columns[l_iIdx];
    end;
  end;



end;


function PushBookMark(p_oDataSet : TDataSet) : Boolean;
begin
  if u_iStackPtr<c_iMaxBookmarks then begin
    u_aoBookmarks[u_iStackPtr]:=p_oDataSet.GetBookmark;
    Inc(u_iStackPtr);
    Result:=True;
  end else begin
    Result:=False;
  end;
end;


function PopBookMark(p_oDataSet : TDataSet): Boolean;
begin
  if u_iStackPtr>1 then begin
    p_oDataSet.GotoBookmark(u_aoBookMarks[u_iStackPtr]);
    Dec(u_iStackPtr);
    Result:=True;
  end else begin
    Result:=False;
  end;
end;


function QueryInitialize(p_oQuery : TQuery;p_sSQL : String) : Boolean;
begin
  Result:=True;
  if p_oQuery.State<>dsInactive then
    p_oQuery.Close;
  p_oQuery.SQL.Clear;
  if p_oQuery.ParamCount>0 then
    p_oQuery.Params.Clear;
  p_oQuery.SQL.Add(p_sSQL);
end;


{$region 'TRASH1'}
(*
function ADO2BDECopyRecord(p_oSrcTable,p_oDstTable : TDataset) : Boolean;
var l_blExcept : Boolean;
    l_iIdx     : Integer;
begin
  BDECopyRecord:=False;
  l_blExcept:=False;

  {---< ���� ������? >---}
  if (p_oSrcTable.State=dsBrowse) and (p_oDstTable.State=dsBrowse) then begin

    {---< ������� ������ >---}
    try
      p_oDstTable.Append;
    except
      on E: EDBEngineError do l_blExcept:=BDEFail(E.Message);
    end;

    {---< ������? >---}
    if (p_oDstTable.State=dsInsert) and not l_blExcept then begin

      {---< ������ ���� �� ����� ���������� ������ >---}
      l_iIdx:=0;
      while (l_iIdx<=Pred(p_oSrcTable.FieldCount)) and not l_blExcept do begin
        try
          p_oDstTable.Fields[l_iIdx].AsString:=p_oSrcTable.Fields[l_iIdx].AsString;
        except
          on E: EDBEngineError do l_blExcept:=not BDEFail(E.Message+' � ���� '+p_oDSTTable.Fields[l_iIdx].DisplayName);
        end;

        inc(l_iIdx);
      end; {- while (l_iIdx<=Pred(p_oSrc.FieldCount)) and not l_blExcept do begin -}

      {---< �������� ������ >---}
      try
        p_oDstTable.Post;
      except
        on E: EDBEngineError do l_blExcept:=BDEFail(E.Message);
      end;
      {---< ������? >---}
      BDECopyRecord:=(p_oDstTable.State=dsBrowse) and not l_blExcept;
    end; {- if (p_oDst.State=dsEdit) and not l_blExcept then begin -}
  end; {- if (p_oSrc.State=dsBrowse) and (p_oDst.State=dsBrowse) then begin -}
end; {- function CopyRec(p_oSrc,p_oDst : TDataset) : Boolean; -}
 *)
(*
function IBODBCRegister(p_sConfig,p_sDataBase,p_sUser,p_sPassword : String) : Boolean;
var l_oReg : TRegistry;
begin
  l_oReg:=TRegistry.create;
  l_oReg.RootKey:=HKEY_CURRENT_USER;
  if not l_oReg.KeyExists('Software\ODBC\ODBC.INI\'+p_sConfig) then
    begin
      l_oReg.OpenKey('Software\ODBC\ODBC.INI\ODBC Data Sources',true);
      l_oReg.WriteString('JKH','Firebird/InterBase(r) driver');
      l_oReg.CloseKey;
      l_oReg.OpenKey('Software\ODBC\ODBC.INI\'+p_sConfig,true);
      //l_oReg.WriteString('DBQ',st1);
      l_oReg.WriteString('AutoQuotedIdentifier', 'N');
      l_oReg.WriteString('CharacterSet', 'CP1251');
      l_oReg.WriteString('Client', '');
      l_oReg.WriteString('Dbname',p_sDataBase);
      l_oReg.WriteString('Description', '');
      l_oReg.WriteString('Dialect', '3');
      l_oReg.WriteString('Driver', 'OdbcFb.dll');
      l_oReg.WriteString('JdbcDriver', 'IscDbc');
      l_oReg.WriteString('LockTimeoutWaitTransactions', '');
      l_oReg.WriteString('NoWait', 'N');
      l_oReg.WriteString('Password', p_sPassword);
      l_oReg.WriteString('QuotedIdentifier', 'Y');
      l_oReg.WriteString('ReadOnly', 'N');
      l_oReg.WriteString('Role', '');
      l_oReg.WriteString('SafeThread', 'Y');
      l_oReg.WriteString('SensitiveIdentifier', 'N');
      l_oReg.WriteString('User', p_sUser);
      l_oReg.WriteString('UseSchemaIdentifier', '0');
      l_oReg.CloseKey;
    end
    else
    begin
      l_oReg.OpenKey('Software\ODBC\ODBC.INI\JKH',true);
      l_oReg.WriteString('Dbname', p_sDataBase);
    end;

    try
      DM.ConnectionJKH.Connected:=True;
    except
      st := '������ ���������� � ����� FireBird!';
      if Application.MessageBox(PChar(st),'��������������',MB_OK+MB_ICONERROR)=IDOK then Halt;
    end;
  end
  else
  begin
    st := '����������� ���� ����'+#13+CurDir+CurBase;
    if Application.MessageBox(PChar(st),'��������������',MB_OK+MB_ICONWARNING)=IDOK then Halt;
  end;

  {********************************************}
   People_Open();
  {********************************************}
end;

*)


(*
function AddAlias() : Boolean;
var l_oAliasParams : TStringList;
begin
  l_oAliasParams := TStringList.Create;
  try
    with l_oAliasParams do
    begin
      Add('SERVER NAME=IB_SERVER:/PATH/DATABASE.GDB');
      Add('USER NAME=MYNAME');
    end;
    Session.AddStandardAlias('NewIBAlias', 'INTRBASE', MyList);
  finally
    FreeAndNil(l_oAliasParams);
  end;
end;

function BackUpDBF(p_oTable : TTable;p_sBackupPath : String;p_blPack : Boolean) : Boolean;
var l_blActive    : Boolean;
    l_sArcName    : String;
    l_sBackUpName : String;
begin
  Result:=False;
  SureSlash(p_sBackupPath);
  l_blActive:=p_oTable.Active;
  l_sBackUpName:=p_sBackupPath+'last_'+p_oTable.TableName;
  if p_oTable.State<>dsInactive then
    p_oTable.Close;
  if CopyFile(PChar(p_oTable.DatabaseName+p_oTable.TableName),
       PChar(l_sBackupName),False) then begin
    if p_blPack then begin
      l_sArcName:=
        D2S(YearOf(Now)-2000)+
        D2S(MonthOf(Now))+
        D2S(DayOf(Now))+'_'+
        D2S(HourOf(Now))+
        D2S(MinuteOf(Now))+
        '.zip';
        if ShellExecute(HInstance,PChar('open'),PChar(GetAppFolder+
          'zip.exe'), PChar('-9 -r '+l_sArcName+' '+l_sBackupName),
           PChar(p_sBackupPath),SW_SHOWDEFAULT)<=32 then begin
             FatalError('������!','������ �������� ��������� ����� �������, ������ N'
             +IntToStr(GetLastError));
        end else begin
          Result:=True;
        end;
    end else begin
      Result:=True;
    end;
  end else begin
    FatalError('������!','���������� ������� ��������� ����� ������� '+p_oTable.TableName);
  end;
  if l_blActive then begin
    if not TryOpenTable(p_oTable, p_oTable.DatabaseName,p_oTable.TableName) then
      FatalError('������!','���������� ������� ������� '+p_oTable.TableName+#13+
      '����� ���������� �����������.');
  end;
end;

function RestoreDBF(p_oTable : TTable;p_sBackupPath : String) : Boolean;
var l_blActive    : Boolean;
    l_sBackUpName : String;
begin
  Result:=False;
  SureSlash(p_sBackupPath);
  l_blActive:=p_oTable.Active;
  l_sBackUpName:=p_sBackupPath+'last_'+p_oTable.TableName;
  if p_oTable.State<>dsInactive then
    p_oTable.Close;

  if CopyFile(PChar(l_sBackupName),
              PChar(p_oTable.DatabaseName+p_oTable.TableName),False) then begin
    Result:=True;
  end else begin
    FatalError('������!','���������� ������������ ������� '+p_oTable.TableName+' �� ��������� �����.');
  end;
  if l_blActive then begin
    if not TryOpenTable(p_oTable, p_oTable.DatabaseName,p_oTable.TableName) then
      FatalError('������!','���������� ������� ������� '+p_oTable.TableName+#13+
      '����� ���������� �����������.');
  end;
end;

{==================================================================================
{ DBFLocate
{ ����� ��������, �� ��������� � �������� �����
{ ������ �� faq, ������ ��������� fieldname, �������� ����� ���� ���������
{================================================================================}
{---< ������� ����� �� DelphiWorld 6 >---}
function DBFLocate(const Table1: TTable; const sFld, sValue: string): boolean;
var

  bmPos: TBookMark;
  bFound: boolean;
  len: integer;
begin

  Result := False;
  if (not StrEmpty(sValue)) and (not StrEmpty(sFld)) then
  begin
    with Table1 do
    begin
      DisableControls;
      bFound := False;
      bmPos := GetBookMark;
      len := Length(sValue);
      First;

      while not EOF do
      begin
        if FieldByName(sFld).AsString <> sValue then
          Next
        else
        begin
          Result := True;
          bFound := True;
          Break;
        end;
      end;

      if (not bFound) then
        GotoBookMark(bmPos);

      FreeBookMark(bmPos);
      EnableControls;
    end;
  end;
end;

function EasyReindex(p_oTable : TTable;
                     p_sIdxName : String;
                     p_sDBPath : String;
                     p_sDBFName : String;
                     p_sIdxExpr : String;
                     p_oOptions : TIndexOptions
                     ) : Boolean;
var l_sExt  : String;
begin
{
Table1->IndexDefs->Clear();
Table1->IndexDefs->Add("indCodep","Code",TIndexOptions() << ixDescending);
Table1->Open();
Table1->IndexName="indCodep";
Table1->IndexFieldNames="Code";
}

  if EasyCopyFile(p_sDBPath+p_sDBFName,p_sDBPath+ExtractFileSpec(p_sDBFName)+'.bak') then begin
    if p_oTable.State<>dsInactive then
      p_oTable.Close;
      if ixNonMaintained in p_oOptions then
        l_sExt:='.ndx'
      else
        l_sExt:='.mdx';
      //DeleteFile(PChar(p_sDBPath+ExtractFileSpec(p_sDBFName)+l_sExt));
      p_oTable.Exclusive:=True;
      TryOpenTable(p_oTable,p_sDBPath,p_sDBFName);
      p_oTable.IndexDefs.Clear;
      //p_oTable.IndexDefs.Add(p_sIdxName,p_sIdxExpr,p_oOptions);
      p_oTable.AddIndex(p_sIdxName,p_sIdxExpr,p_oOptions);

      p_oTable.IndexName:=p_sIdxName;
      p_oTable.IndexFieldNames:=p_sIdxExpr;
      p_oTable.Close;
      p_oTable.Exclusive:=False;
  end;
end;

function EasyReIndex(p_oTable : TTable;
                     p_sIdxName : String;
                     p_sDBPath : String;
                     p_sDBFName : String;
                     p_sIdxExpr : String;
                     p_oOptions : TIndexOptions
                     ) : Boolean;
var l_blActive,
    l_blExclusive : Boolean;
    l_iIdx        : Integer;
    l_blOk,
    l_blIdxExists : Boolean;
    l_sDBFName,
    l_sIndexName,
    l_sBakName    : String;
    l_sTempName   : String;
begin
  Result:=False;
  l_blOk:=True;

  {---< ����� ������ >---}
  SureSlash(p_sDBPath);
  l_SDBFName:=p_sDBPath+p_sDBFName;
  l_sBakName:=ExtractFileSpec(p_sDBPath+p_sDBFName)+'.bak';
  l_sIndexName:=p_sDBPath+p_sIdxName;
  if ixNonMaintained in p_oOptions then begin
    l_sIndexName:=l_sIndexName+'.ndx';
  end else begin
    l_sIndexName:=l_sIndexName+'.mdx';
  end;

  {---< �������� ������� >---}
  l_blActive:=p_oTable.Active;
  l_blExclusive:=p_oTable.Exclusive;
  if l_blActive then begin
    p_oTable.Close;
  end;
  p_oTable.Exclusive:=True;
  p_oTable.DatabaseName:=p_sDBPath;

  {---< ������� ������� >---}
  if CopyFile(Pchar(l_sDBFName),Pchar(l_sBakName),False) then begin
    {---< ��� ��� ��������� ����� � ��������� >---}
    l_blIdxExists:=FileExists(l_sIndexName);

    {---< ������� ���� ������� � DBF >---}
    if ClearIndexFlag(l_sDBFName) then begin
      p_oTable.IndexName:='';

      {---< ���� ��������� ���� ������ >---}
      p_oTable.IndexFiles.Clear;// ��� �������� �������

      {---< ��������� ���� >---}
      try
        p_oTable.Open;
      except
        on E: EDBEngineError do l_blOk:=BDEFail(E.Message);
      end;
      if l_blOk then begin

        {---< ��������� def'�� >---}
        for l_iIdx:=0 to Pred(p_oTable.IndexDefs.Count) do
          p_oTable.IndexDefs.Delete(l_iIdx);

        {---< ����� � ��� ������? >---}
        if ixNonMaintained in p_oOptions then begin
          {---< �� �������������� - ndx >---}
          if l_blIdxExists then
            DeleteFile(PChar(l_sIndexName));
          p_oTable.AddIndex(l_sIndexName, p_sIDXExpr, p_oOptions);
          l_sTempName:=p_sDBPath+p_sIdxName+'.ndx';
          p_oTable.IndexFiles.Add(l_sTempName);
        end else begin
          {---< �������������� - mdx >---}
          if l_blIdxExists then
            DeleteFile(PChar(l_sIndexName));
          p_oTable.AddIndex(p_sIdxName, p_sIDXExpr, p_oOptions)
        end;
        p_oTable.IndexDefs.Update;
        p_oTable.IndexName := p_oTable.IndexDefs.Items[0].Name;

        p_oTable.Close;
        p_oTable.Exclusive:=l_blExclusive;
        Result:=True;
      end else begin
        Notice('���������� ����������������� �������',
        '��� �������������� ��������� ������ ������� ������� � ����������� ������');
                Application.Terminate;
        CopyFile(Pchar(l_sBakName),Pchar(l_sDBFName),False);
      end;
      if l_blActive then
        TryOpenTable(p_oTable,p_sDBPath,p_sDBFName);

    end else begin
      Notice('���������� ����������������� �������',
      '�� ������� ������� ������ ������');
    end;

  end else begin
    Notice('���������� ����������������� �������',
    '�� ������� ������� ��������� ����� dbf-�����');
  end;
end;
*)
{function ActualizeColumnMarks(p_oGrid : TDBGrid; p_sOrderField : String;p_blAscending : Boolean = True) : Boolean;
var l_iIdx: Integer;
begin
  for l_iIdx := 0 to Pred(p_oGrid.Columns.Count) do
  begin
    if p_oGrid.Columns.Items[l_iIdx].Field.DisplayName=p_sOrderField then
    begin
      p_oGrid.Columns.Items[l_iIdx].Title.Font.Style := p_oGrid.Columns.Items[l_iIdx].Title.Font.Style + [fsBold];
      if not p_blAscending then
        p_oGrid.Columns.Items[l_iIdx].Title.Font.Style := p_oGrid.Columns.Items[l_iIdx].Title.Font.Style - [fsStrikeOut];
    end;
  end;

end;
}
(*
var
  holdColor: TColor;
begin
  holdColor := dbgrideh1.Canvas.Brush.Color; {��������� ������������ ����}
  if dm.ISelContr.FieldByName('PER_NOMER_SVID').AsString = Edit1.text then
    begin
      dbgrideh1.Canvas.Font.Color := clBlack;
      dbgrideh1.Canvas.Brush.Color := clYellow;
      dbgrideh1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
      dbgrideh1.Canvas.Brush.Color := holdColor;
    end;
*){$endregion}

{ TDBManager }

procedure TDBManager.AddParameter(psParam, psValue : String);
begin

  inc(miParamCount);
  masParams[miParamCount]:=psParam;
  masValues[miParamCount]:=psValue;
end;


function TDBManager.ChangeParameter(psParam, psValue: String) : Boolean;
var liIdx : Integer;
begin

  Result:=False;
  for liIdx:=1 to miParamCount do
  begin

    if masParams[liIdx]=psParam then
    begin
      masValues[liIdx]:=psValue;
      Result:=True;
      break;
    end;
  end;
end;


procedure TDBManager.Close;
begin

  moQuery.Close;
end;


constructor TDBManager.Create(psSQL : String; poQuery: TADOQuery; poGrid : TDBGrid);
begin

  SetMainSQL(psSQL);
  SetOrderField('');
  setFilterField('');
  setFilterCriteria('');
  setQuery(poQuery);
  setGrid(poGrid);
  mblFirstOpen:=True;
  FillChar(masParams,sizeof(masParams),0);
  FillChar(masValues,sizeof(masValues),0);
  miParamCount:=0;
end;


destructor TDBManager.Destroy;
begin

  inherited;
  Close;
end;


procedure TDBManager.FillPopUpMenuItems(poMenu: TPopUpMenu; poOnClick: TNotifyEvent);
var liIdx  : Integer;
    loItem : TMenuItem;
begin

  if moGrid<>Nil then
  begin

    poMenu.Items.Clear;
    for liIdx:=0 to Pred(moGrid.Columns.Count) do
    begin

      loItem:=TMenuItem.Create(poMenu);
      loItem.Tag:=liIdx;
      loItem.Caption:=moGrid.Columns.Items[liIdx].Title.Caption;
      loItem.Checked:=moGrid.Columns.Items[liIdx].Visible;
      loItem.OnClick:=poOnClick;
      poMenu.Items.Add(loItem);
    end;
  end;
end;


function TDBManager.GetOrderField: String;
begin

  Result:=msOrderField;
end;


procedure TDBManager.InitialSetColumnTitle;
var liIdx: Integer;
begin

  if moGrid<>Nil then
  begin

    for liIdx := 0 to Pred(moGrid.Columns.Count) do
    begin

      if AnsiUpperCase(moGrid.Columns.Items[liIdx].Field.DisplayName)=
         AnsiUpperCase(msOrderField) then
      begin

        moGrid.Columns.Items[liIdx].Title.Font.Style := moGrid.Columns.Items[liIdx].Title.Font.Style + c_eSortAscendingStyle;
      end;
    end;
  end;
end;


procedure TDBManager.Open;
var liIdx : Integer;
begin

  Prepare;
  moQuery.SQL.Clear;
  moQuery.SQL.Add(msPreparedSQL);
  for liIdx:=1 to miParamCount do
  begin

    moQuery.Parameters.ParamByName(masParams[liIdx]).Value:=masValues[liIdx];
  end;
  moQuery.Open;
  if mblFirstOpen then
  begin

    mblFirstOpen:=False;
  end;
end;


procedure TDBManager.PlugGrid;
begin

  if moGrid<>Nil then
    moGrid.DataSource:=moDataSource;
end;


procedure TDBManager.Prepare;
begin

  msPreparedSQL:=msMainSQL;
  if not IsEmpty(msFilterCriteria) then
  begin

    msPreparedSQL:=msPreparedSQL+' WHERE '+msFilterField+'='+msFilterCriteria;
    if not IsEmpty(msDefaultFilter) then
      msPreparedSQL:=msPreparedSQL+' AND '+msDefaultFilter;
  end
  else
  begin

    if not IsEmpty(msDefaultFilter) then
      msPreparedSQL:=msPreparedSQL+' WHERE '+msDefaultFilter;
  end;

  if not IsEmpty(msOrderField) then
  begin

    msPreparedSQL:=msPreparedSQL+' ORDER BY '+msOrderField+' '+msOrderDirection;
  end;
end;


procedure TDBManager.Refresh;
begin

  Screen.Cursor:=crHourGlass;
  Close;
  Open;
  Screen.Cursor:=crDefault;
end;


procedure TDBManager.Reorder(poColumn: TColumn);
begin

  if poColumn<>Nil then
  begin

    //***** �����, ���� ������� ��� ������������ ��� ����������?
    if poColumn.Title.Font.Style*c_eSortAscendingStyle=c_eSortAscendingStyle then
    begin

      //***** ���. � � ����� ����������� �� ������������ ?
      if poColumn.Title.Font.Style*c_eSortDescendingStyle=c_eEmptyStyle then
      begin

        //***** �� ����������. ������ �� ��������.
        SetOrderField(poColumn.Field.DisplayName);
        SetOrderDirection(False);
        Refresh;
        poColumn.Title.Font.Style:=poColumn.Title.Font.Style+c_eSortDescendingStyle;
      end
      else
      begin

        //***** �� ��������. ������ �� ����������.

        SetOrderField(poColumn.Field.DisplayName);
        SetOrderDirection;
        Refresh;
        poColumn.Title.Font.Style:=poColumn.Title.Font.Style-c_eSortDescendingStyle;
      end;
    end
    else
    begin

      //***** ���, ���� ������� �� ������������. ������� ��� ���������
      ResetColumnTitles;

      //***** ����������� �� ����������� � ������� ���.
      SetOrderField(poColumn.Field.DisplayName);
      SetOrderDirection;
      Refresh;
      poColumn.Title.Font.Style:=(poColumn.Title.Font.Style+c_eSortAscendingStyle)  -c_eSortDescendingStyle;

    end;
  end
end;


procedure TDBManager.ResetColumnTitles;
var liIdx: Integer;
begin

  if moGrid<>Nil then
  begin

    for liIdx := 0 to Pred(moGrid.Columns.Count) do
    begin

      moGrid.Columns.Items[liIdx].Title.Font.Style :=
        moGrid.Columns.Items[liIdx].Title.Font.Style - [fsBold,fsUnderLine];
    end;
  end;
end;


procedure TDBManager.setDefaultFilter(psFilterLine: String);
begin

  msDefaultFilter:=psFilterLine;
end;


procedure TDBManager.setFilter(psFilterField, psFilterCriteria: String);
begin

  setFilterField(psFilterField);
  setFilterCriteria(psFilterCriteria);
end;


procedure TDBManager.setFilterCriteria(psFilterCriteria: String);
begin

  msFilterCriteria:=psFilterCriteria;
end;


procedure TDBManager.setFilterField(psFilterField: String);
begin

  msFilterField:=psFilterField;
  if IsEmpty(msFilterField) then
    SetFilterCriteria;
end;


procedure TDBManager.setGrid(poGrid: TDBGrid);
begin

  moGrid:=poGrid;
  if moGrid<>nil then
    moDataSource:=moGrid.DataSource;
end;


procedure TDBManager.setMainSQL(psSQL: String);
begin

  msMainSQL:=psSQL;
end;


procedure TDBManager.setOrderDirection(pblDirection: Boolean);
begin

  if pblDirection then
    msOrderDirection:='ASC'
  else
    msOrderDirection:='DESC'
end;


procedure TDBManager.setOrderField(psOrderField: String);
begin

  msOrderField:=psOrderField;
end;


procedure TDBManager.setQuery(poQuery: TADOQuery);
begin

  moQuery:=poQuery;
end;


procedure TDBManager.UnplugGrid;
begin

  if moGrid<>Nil then
    moGrid.DataSource:=Nil;
end;


begin

  u_iStackPtr:=1;
end.
