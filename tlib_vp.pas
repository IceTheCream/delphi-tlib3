unit tlib;

{$X+}

interface

{ifdef __VP__}
{I vp.inc}
{else}
{i profile.inc}
{endif}

uses SysUtils;

{$ifdef __VP__}
    type DWord = LongInt;
{$endif}


{$i const.inc}
const
      c_iJanuary   =  1;
      c_iFebruary  =  2;
      c_iMarch     =  3;
      c_iApril     =  4;
      c_iMay       =  5;
      c_iJuny      =  6;
      c_iJuly      =  7;
      c_iAugust    =  8;
      c_iSeptember =  9;
      c_iOctober   = 10;
      c_iNovember  = 11;
      c_iDecember  = 12;
      c_asMonths : array[c_iJanuary..c_iDecember] of String =
      (
       '������',
       '�������',
       '����',
       '������',
       '���',
       '����',
       '����',
       '������',
       '��������',
       '�������',
       '������',
       '�������'
      );

      c_cMoneyDelim = '.'; // ','
      c_cLF         = #13;
      c_cSlash      = '\';
      c_cSpace      = #32;
{===<< ����� ������� >>===}
procedure PurgeBuffer(var p_oBuffer);

{===<< �������� ������� >>===}
function IsFolderExists(p_sPath : String) : Boolean;
function GetFileSize(p_sName : String) : Cardinal;
function IsIOError : Boolean;
function EasyMkDir(p_sDirName : String) : Boolean;
//function PurgeFolder(p_sFolder : String; p_sMask : String) : Boolean;
//function EasyCopyFile(p_sFromFile,p_sToFile : String) : Boolean;
function EasyFindFirst(p_sFolder : String; p_sMask : String; p_iAttr : Integer) : String;
function EasyFindNext : String;
function EasyFindClose : Boolean;
function EasyFindGetAttr : Integer;
function EasyFindGainRec : TSearchRec;
function IsEasyFindOpen : Boolean;
//function EasyBackupFolder(p_sFrom,p_sTo,p_sFolder,p_sBackupName : String; p_sFileList : String; p_blWait : Boolean; p_sFileMask : String) : Boolean;
function EasyFileAge(p_sFileName : String) : TDateTime;

{===<< ��������� ������� >>===}

function InsertFoldersInFilesList(p_sLine,p_sFolder : String; p_sDelim : String) : String;
function SlashIt(var p_sLine : String; p_cSlash : Char) : Boolean;
function Cut(var p_sLine : String; p_btFrom : Integer; p_btHowMany : Integer) : String;
function ChopTail(p_sLine : String;p_iCount : Integer) : String;
function Repl(p_iHowMany : Integer; p_cChar : Char) : String;
function AlignFill(p_sLine : String; p_iWidth : Integer; p_btAlign : Byte; p_cFill : Char) : String;
function AlignRight(p_sLine : String; p_iWidth : Integer; p_cFill : Char) : String;
function LengthenDate(p_sDate : String; p_sAge : String) : String;
function IsEmpty(p_sLine : String) : Boolean;
function String2PChar(p_sLine : String) : PChar;
function AlignLeft(p_sLine : String; p_iWidth : Integer) : String;
function AlignCenter(p_sLine : String; p_iWidth : Integer) : String;
//function FindExtension(p_sFileName : String) : Integer;
//function CutExtension(p_sFileName : String) : String;
function ShortenDate(p_sDate : String) : String;
function IsInteger(p_sText : String) : Boolean;
function IsFloat(p_sText : String) : Boolean;
function IsRussianChar(p_cChar : Char) : Boolean;
function IsLatinChar(p_cChar : Char) : Boolean;
function IsDigit(p_cChar : Char) : Boolean;
function LongToStr(p_lValue : LongInt) : String;
function Bool2StrY(p_blValue : Boolean) : String;
function Bool2StrN(p_blValue : Boolean) : String;
function FindAndReplaceOneChar(p_sLine : String; p_cSrc,p_cDst : Char) : String;
function FindAndDeleteAllEntries(p_sLine : String; p_cChar : Char) : String;
function Fetch(var p_sLine : String; p_sDelim: String): String;
function DateCompact(p_dDate : TDateTime) : String;
function InsertAtLabel(Var p_sLine : String; p_sLabel,p_sPiece : String) : Boolean;
function StripLeadZeroes(var p_sLine : String): String;
function WordToStrZero(p_wDate : Word) : String;
function SeparateMils(p_lSum : LongInt) : String;
//function GetDateStampFileName : String;
//function ExtractFileFromResource(p_sResName,p_sFileName : String) : Boolean;
function HashString(p_sLine : String) : Longint;
function WriteOutSum(p_dValue : Double) : String;

function RoundEx(p_dValue : Double) : Double;

var g_sProgrammFolder : String;


implementation


var u_oEasySearchRec : TSearchRec;
    u_blEasySearchOpen : Boolean = False;


{===<< ����� ������� >>===}
procedure PurgeBuffer(var p_oBuffer);
begin
  FillChar(p_oBuffer,sizeof(p_oBuffer),0);
end; {- p PurgeBuffer(var p_oBuffer); -}


{===<< �������� ������� >>===}
function IsFolderExists(p_sPath : String) : Boolean;
var l_oSearch : TSearchRec;
    l_iRes    : Integer;
begin
  SlashIt(p_sPath,c_cSlash);
  p_sPath:=p_sPath+'*.*';
  l_iRes:=SysUtils.FindFirst(p_sPath,faDirectory,l_oSearch);
  IsFolderExists:=(l_iRes=0);
  SysUtils.FindClose(l_oSearch);
end; {- f IsFolderExists(p_sPath : String) : Boolean; -}


function GetFileSize(p_sName : String) : Cardinal;
var l_oRec : TSearchRec;
begin
  GetFileSize:=0;
  if SysUtils.FindFirst(p_sName,faAnyFile, l_oRec) = 0 then begin
    GetFileSize:=l_oRec.Size;
    SysUtils.FindClose(l_oRec);
  end; {- if SysUtils.FindFirst(p_sName,faAnyFile, l_oRec) = 0 then -}
end; {- f GetFileSize(p_sName : String) : Cardinal; -}


function IsIOError : Boolean;
begin
  IsIOError:=IOResult<>0;
end; {- IsIOError : Boolean; -}


function EasyMkDir(p_sDirName : String) : Boolean;
begin
  if IsFolderExists(p_sDirName) then begin
    EasyMKDir:=True;
  end else begin
    MkDir(p_sDirName);
    EasyMkDir:=not IsIOError;
  end; {- if IsFolderExists(p_sDirName) then begin -}
end; {- EasyMkDir(p_sDirName : String) : Boolean; -}


function EasyFindFirst(p_sFolder : String; p_sMask : String; p_iAttr : Integer) : String;
var l_sFilename : String;
begin
  EasyFindFirst:='';
  if not u_blEasySearchOpen then begin

    SlashIt(p_sFolder,c_cSlash);
    if FindFirst(p_sFolder+p_sMask,p_iAttr,u_oEasySearchRec)=0 then begin

      l_sFileName:=u_oEasySearchRec.Name;
      u_blEasySearchOpen:=True;

      while (l_sFileName='.') or (l_sFileName='..') do begin
        l_sFileName:=EasyFindNext;
      end; {- while (l_sFileName='.') or -}

      if not IsIOError then begin

        EasyFindFirst:=l_sFileName;
      end; {- if not IsIOError then begin -}
    end; {- if FindFirst(p_sFolder+p_sMask,p_iAttr,u_oEasySearchRec)=0 then begin -}
  end; {- if not u_blEasySearchOpen then begin -}
end; {- f EasyFindFirst(p_sFolder : String; p_sMask : String = '*.*'; p_iAttr : Integer = faAnyFile) : String; -}


function EasyFindNext : String;
begin
  EasyFindNext:='';
  if u_blEasySearchOpen then begin
    if FindNext(u_oEasySearchRec)=0 then begin
      if not IsIOError then begin
        EasyFindNext:=u_oEasySearchRec.Name;
      end; {- if not IsIOError then begin -}
    end; {- if FindNext(u_oEasySearchRec)=0 then begin -}
  end; {- if u_blEasySearchOpen then begin -}
end; {- f EasyFindNext : String; -}


function EasyFindClose : Boolean;
begin
  EasyFindClose:=True;
  if u_blEasySearchOpen then begin
    SysUtils.FindClose(u_oEasySearchRec);
    if not IsIOError then begin
      EasyFindClose:=True;
      u_blEasySearchOpen:=False;
    end; {- if not IsIOError then begin -}
  end; {- if u_blEasySearchOpen then begin -}
end; {- f EasyFindClose : Boolean; -}


function EasyFindGetAttr : Integer;
begin
  EasyFindGetAttr:=u_oEasySearchRec.Attr;
end;


function EasyFindGainRec : TSearchRec;
begin
  EasyFindGainRec:=u_oEasySearchRec;
end; {- f EasyFindGainRec : TSearchRec; -}


function IsEasyFindOpen : Boolean;
begin
  IsEasyFindOpen:=u_blEasySearchOpen;
end; {- f IsEasyFindOpen : Boolean; -}


function EasyFileAge(p_sFileName : String) : TDateTime;
begin
  EasyFileAge:=FileAge(p_sFileName);
end;

{===<< ��������� ������� >>===}

{---< ��������� ���� � ������� ����� � p_sLine >---}
function InsertFoldersInFilesList(p_sLine,p_sFolder : String; p_sDelim : String) : String;
var l_sLine    : String;
    l_sResLine : String;
    l_iPos     : Integer;
    l_iLastPos : Integer;
begin
  l_sResLine:='';
  l_iLastPos:=1;
  l_sLine:=Trim(p_sLine);
  l_iPos:=Pos(p_sDelim,l_sLine);

  while l_iPos>0 do
  begin
    l_sResLine:=l_sResLine+p_sFolder+Cut(l_sLine,l_iLastPos,l_iPos-l_iLastPos)+' ';
    Delete(l_sLine,1,1);
    l_iLastPos:=1;
    l_iPos:=Pos(p_sDelim,l_sLine);
  end; {- while l_iPos>0 do -}

  InsertFoldersInFilesList:=l_sResLine+p_sFolder+l_sLine;
end; {- function InsertFoldersInFilesList(p_sLine,p_sFolder : String; p_sDelim : String = ';') : String; -}


function SlashIt(var p_sLine : String;p_cSlash : Char) : Boolean;
begin
  p_sLine:=Trim(p_sLine);
  if p_sLine<>'' then begin
    if p_sLine[Length(p_sLine)]<>p_cSlash then
      p_sLine:=p_sLine+p_cSlash;
  end; {- if p_sLine<>'' then begin -}
  SlashIt:=True;
end; {- f SlashIt(var p_sLine : String) : Boolean; -}

function Cut(var p_sLine : String; p_btFrom : Integer; p_btHowMany : Integer) : String;
begin
  if p_btHowMany<0 then
  begin
    p_btHowMany:=Succ(Length(p_sLine)-p_btFrom);
  end;
  Cut:=Copy(p_sLine,p_btFrom,p_btHowMany);
  Delete(p_sLine,p_btFrom,p_btHowMany);
end; {- f Cut(var p_sLine : String; p_btFrom,p_btHowMany : Byte); -}


{---< ������� ���������� ������, ��������� �� �������� �������� >---}
{ p_cChar     - ������ ��� ����������
  p_btHowMany - ���������� �������� � ������ }
function Repl(p_iHowMany : Integer; p_cChar : Char) : String;
begin
  Repl:=StringOfChar(p_cChar,p_iHowMany);
end; {- f Repl(p_btHowMany : Byte; p_cChar : Char) : String; -}


function ChopTail(p_sLine : String; p_iCount : Integer) : String;
begin
  Delete(p_sLine,Length(p_sLine),p_iCount);
  ChopTail:=p_sLine;
end; {- f ChopTail(p_sLine : String;p_iCount : Integer = 1) : String; -}


function IsEmpty(p_sLine : String) : Boolean;
begin
  IsEmpty:=Length(Trim(p_sLine))=0;
end; {- f IsEmpty(p_sLine : String) : Boolean; -}


function String2PChar(p_sLine : String) : PChar;
var l_pcLine : PChar;
begin
  l_pcLine:=StrAlloc(Succ(Length(p_sLine)));
  StrPCopy(l_pcLine,p_sLine);
  String2PChar:=l_pcLine;
end; {- f String2PChar(p_sLine : String) : PChar; -}


{---< ������������� ������� ������������ ������. >---}
{ p_sLine   - ������ ��� ������������
  p_btWidth - ������ ������ ������;
  p_btAlign - ���� ������������ - c_iAlLeft, c_iAlRight,c_iAlCenter
  p_cFill   - ������ ����������. }
function AlignFill(p_sLine : String; p_iWidth : Integer; p_btAlign : Byte; p_cFill : Char) : String;
var l_sLine  : String;
    l_iWdt   : Integer;
    l_iLen  : Integer;
    l_iHalf : Integer;
begin
  l_sLine:=Trim(p_sLine);
  l_iLen:=Length(l_sLine);
  if l_iLen>0 then begin

    if l_iLen<p_iWidth then begin

      l_iWdt:=p_iWidth-l_iLen;
      if p_btAlign=c_iAlCenter then begin
        l_iHalf:=l_iWdt div 2;
        l_sLine:=Repl(l_iHalf,p_cFill)+l_sLine+Repl(l_iWdt-l_iHalf,p_cFill);
      end else begin
        if p_btAlign=c_iAlLeft then begin
          l_sLine:=l_sLine+Repl(l_iWdt,p_cFill);
        end else begin
          if p_btAlign=c_iAlRight then begin
            l_sLine:=Repl(l_iWdt,p_cFill)+l_sLine;
          end; {- if p_btAlign=c_iAlRight then begin -}
        end {- if p_btAlign=c_iAlLeft then begin / else  -}
      end; {- if p_btAlign=c_iAlCenter then begin / else  -}
    end; {- if l_iLen<p_iWidth then begin -}
  end else begin
    l_sLine:=Repl(p_iWidth,c_cSpace);
  end; {- if l_iLen>0 then begin -}
  AlignFill:=l_sLine;
end; {- f AlignFill(p_sLine : String; p_iWidth,p_iAlign : Byte = c_iAlLeft; p_cFill : Char = ' ') : String; -}


{---< ������� ������������ ������ �� ������ ���� � ����������� ��������� ������ >---}
{ p_sLine - ������ ��� ������������
  p_btWidth - ������ ������ ������; }
function AlignLeft(p_sLine : String; p_iWidth : Integer) : String;
begin
  AlignLeft:=AlignFill(p_sLine,p_iWidth,c_iAlLeft,#32);
end; {- f AlignLeft(p_sLine : String; p_btWidth : Byte); -}


{---< ������� ������������ ������ ������ � ����������� ��������� ����� >---}
{ p_sLine - ������ ��� ������������
  p_btWidth - ������ ������ ������; }
function AlignRight(p_sLine : String; p_iWidth : Integer; p_cFill : Char) : String;
begin
  AlignRight:=AlignFill(p_sLine,p_iWidth,c_iAlRight,p_cFill);
end; {- f AlignRight(p_sLine : String; p_btWidth : Byte); -}


{---< ������� ������������ ������ �� ������ >---}
{ p_sLine - ������ ��� ������������
  p_btWidth - ������ ������ ������; }
function AlignCenter(p_sLine : String; p_iWidth : Integer) : String;
begin
  AlignCenter:=AlignFill(p_sLine,p_iWidth,c_iAlCenter,#32);
end; {- f AlignLeft(p_sLine : String; p_btWidth : Byte); -}

(*
function FindExtension(p_sFileName : String) : Integer;
var l_iPos     : Integer;
    l_iPrevPos : Integer;
begin
  //FindExtension:=0;
  l_iPrevPos:=0;
  l_iPos:=Pos('.',p_sFileName);
  while l_iPos>0 do begin
    l_iPrevPos:=l_iPos;
    l_iPos:=PosEx('.',p_sFileName,Succ(l_iPos));
  end; {- while l_iPos>0 do begin -}
  FindExtension:=l_iPrevPos;
end; {- f FindExtension(p_sFileName : String) : Integer; -}

{---< ������� ���������� ��� ����� � ����� ��� ���������� >---}
{ p_sFileName - ������ ��� ����� }
function CutExtension(p_sFileName : String) : String;
begin
  CutExtension:=Copy(p_sFileName,1,Pred(FindExtension(p_sFileName)));
end; {- f CutExtension(p_sFileName : String) : String; -}
*)


{---< ������� ��������� ����� ���� Real � String >---}
{ p_rValue     - ����� ��� ��������
  p_btWidth    - ���������� ������ �� �������
  p_btDecimals - ���������� ������ ����� �������. }
function RealToStr(p_rValue : Real; p_btWidth,p_btDecimals : Byte) : String;
var l_sValue : String;
begin
  Str(p_rValue:p_btWidth:p_btDecimals,l_sValue);
  RealToStr:=Trim(l_sValue);
end; {- f RealToStr(p_rValue : Real; p_btWidth,p_btDecimals : Byte) : String; -}


(*
{---< ������� ��������� ��������� ������������� ����� � ��� Real >---}
{ p_sLine  - ����� � ��������� �������������
  p_rValue - �������� �� ��������� }

function StrToRealDef(p_sLine : String; p_rValue : Real) : Real;
var C      : Integer;
    P      : Integer;
    l_sRes : String;
begin
  p:=Pos(',',p_sLine);
  if p>0 then begin
    p_sLine[P]:='.';
  end;
  Val(p_sLine,l_sRes,C);
  if C<>0 then l_sRes:=p_rValue;
  StrToRealDef:=l_sRes;
end;

*)

{---< ������� ���������� �������� ������������� ���� >---}
{ p_sDate - ������� ������������� ���� }
function ShortenDate(p_sDate : String) : String;
begin
  ShortenDate:=Copy(p_sDate,1,6)+Copy(p_sDate,9,2);
end; {- f ShortenDate(p_sDate : String) : String; -}


{---< ������� ���������� ������� ������������� ���� - ������ 21 ��� >---}
{ p_sDate - �������� ������������� ���� }
function LengthenDate(p_sDate : String; p_sAge : String) : String;
begin
  LengthenDate:=Copy(p_sDate,1,6)+p_sAge+Copy(p_sDate,7,2);
end; {- f LengthenDate(p_sDate : String; p_sAge : String = '20') : String; -}


{---< ������� ���������, �������� �� ������ ����� ����� >---}
{ p_sText - ������ � ������ }
function IsInteger(p_sText : String) : Boolean;
var l_iIdx  : Integer;
    l_iLen  : Integer;
    l_sLine : String;
begin
  IsInteger:=True;
  l_iIdx:=1;
  l_sLine:=Trim(p_sText);
  l_iLen:=Length(l_sLine);
  while l_iIdx<=l_iLen do begin
    //if not (l_sLine[l_iIdx] in ['0','1','2','3','4','5','6','7','8','9']) then begin
    if not IsDigit(l_sLine[l_iIdx]) then begin
      IsInteger:=False;
      break;
    end;
    inc(l_iIdx);
  end;
end; {-f IsInteger(p_sText : String) : Boolean; }


{---< ������� ���������, �������� �� ������ Float >---}
{ p_sText    - ������ � ������ }
function IsFloat(p_sText : String) : Boolean;
var l_iIdx  : Integer;
    l_iLen  : Integer;
    l_sLine : String;
begin
  //IsFloat:=True;
  IsFloat:=True;
  l_iIdx:=1;
  l_sLine:=p_sText;
  l_iLen:=Length(l_sLine);
  while l_iIdx<=l_iLen do begin
    if not (l_sLine[l_iIdx] in ['0','1','2','3','4','5','6','7','8','9',',']) then begin
      IsFloat:=False;
      break;
    end; {- while l_iIdx<=l_iLen do begin }
    inc(l_iIdx);
  end;
end; {- f IsFloat(p_sText : String) : Boolean; }


function IsRussianChar(p_cChar : Char) : Boolean;
begin
  IsRussianChar := p_cChar in c_stRussian;
end; {- f IsRussianChar(p_cChar : Char) : Boolean; -}


function IsLatinChar(p_cChar : Char) : Boolean;
begin
  IsLatinChar := p_cChar in c_stLatin;
end;  {- f IsLatinChar(p_cChar : Char) : Boolean; -}


function IsDigit(p_cChar : Char) : Boolean;
begin
  IsDigit:=p_cChar in ['0','1','2','3','4','5','6','7','8','9'];
end;


function LongToStr(p_lValue : LongInt) : String;
var l_sValue : String;
begin
  Str(p_lValue,l_sValue);
  LongToStr:=l_sValue;
end; {- f LongToStr(p_lValue : LongInt) : String; -}


function Bool2StrN(p_blValue : Boolean) : String;
begin
  if p_blValue then Bool2StrN:='1'
  else Bool2StrN:='0';
end; {- f Bool2StrN(p_blValue : Boolean) : String; -}


function Bool2StrY(p_blValue : Boolean) : String;
begin
  if p_blValue then Bool2StrY:='��'
  else Bool2StrY:='���';
end; {- f Bool2StrY(p_blValue : Boolean) : String; -}


{---< ������� ������� �������� ������ � ������ � �������� ��� ������ �������� >---}
{ p_sLine   - c����� ��� ������
  p_cSrc    - ������� ������
  p_cDst    - ������ ��� ������ }
function FindAndReplaceOneChar(p_sLine : String;p_cSrc,p_cDst : Char) : String;
var l_iPos : Integer;
begin
  l_iPos:=Pos(p_cSrc,p_sLine);
  p_sLine[l_iPos]:=p_cDst;
  FindAndReplaceOneChar:=p_sLine;
end; {- FindAndReplaceOneChar(p_sLine : String;p_cSrc,p_cDst : Char) : String; }


function FindAndDeleteAllEntries(p_sLine : String; p_cChar : Char) : String;
var l_iPos  : Integer;
    l_sLine : String;
begin
  l_sLine:=p_sLine;
  l_iPos:=Pos(p_cChar,l_sLine);
  While l_iPos>0 do begin
    Delete(l_sline,l_iPos,1);
    l_iPos:=Pos(p_cChar,l_sLine);
  end;
  FindAndDeleteAllEntries:=l_sLine;
end; {- f FindAndDeleteOneChar(p_sLine : String; p_cChar : Char) : String; -}


{---< ������� ������� � ������ �������� ������, �������� � ���������� >---}
{---<  ������ ������. ������� ������ ������������ � var ���������� >---}
{ S      - �������� ������
  delim  - ����������� }
function Fetch(var p_sLine : String; p_sDelim: String): String;
begin
  Fetch:= '';
  if not IsEmpty(p_sLine) then begin
    if IsEmpty(p_sDelim) then begin
      Fetch:= p_sLine;
    end else begin
      if Pos(p_sDelim,p_sLine)=0 then begin
        Fetch:= p_sLine;
        p_sLine:= '';
      end else begin
        Fetch:=Copy(p_sLine, 1, Pos(p_sDelim,p_sLine)-1);
        p_sLine:=Copy(p_sLine, Pos(p_sDelim,p_sLine)+Length(p_sDelim), Length(p_sLine));
      end; {- if Pos(p_sDelim,p_sLine)=0 then else -}
    end; {- if IsEmpty(p_sDelim) then begin -}
  end; {- if not IsEmpty(p_sLine) then begin -}
end; {- f Fetch(var p_sLine : String; p_sDelim: String ): String; -}


function DateCompact(p_dDate : TDateTime) : String;
var l_wYear, l_wMonth,
    l_wDay, l_wHour,
    l_wMin, l_wSec,
    l_wMSec            : Word;
    l_sLine            : String;
begin
  DecodeTime(p_dDate,l_wHour,l_wMin,l_wSec,l_wMSec);
  DecodeDate(p_dDate,l_wYear,l_wMonth,l_wDay);
  l_sLine:='['+AlignRight(IntToStr(l_wYear),2,'0');
  l_sLine:=l_sLine+AlignRight(IntToStr(l_wMonth),2,'0');
  l_sLine:=l_sLine+AlignRight(IntToStr(l_wDay),2,'0')+'-';
  l_sLine:=l_sLine+AlignRight(IntToStr(l_wHour),2,'0');
  l_sLine:=l_sLine+AlignRight(IntToStr(l_wMin),2,'0');
  l_sLine:=l_sLine+AlignRight(IntToStr(l_wSec),2,'0')+']';
  DateCompact:=l_sLine;
end; {- f DateCompact : String; -}


function InsertAtLabel(Var p_sLine : String; p_sLabel,p_sPiece : String) : Boolean;
var l_iLblLen : Integer;
    l_iLblPos : Integer;
begin
  InsertAtLabel:=False;
  l_iLblPos:=Pos(p_sLabel,p_sLine);
  l_iLblLen:=Length(p_sLabel);
  if l_iLblPos>0 then begin

    if l_iLblPos+l_iLblLen<Length(p_sLine) then begin
      p_sLine:=Copy(p_sLine,1,Pred(l_iLblPos))+
      p_sPiece+
      Copy(p_sLine,l_iLblPos+l_iLblLen,Length(p_sLine)-Pred(l_iLblPos+l_iLblLen));
    end else begin
      p_sLine:=Copy(p_sLine,1,Pred(l_iLblPos))+p_sPiece;
    end; {- if l_iLblPos+l_iLblLen<Length(p_sLine) then else -}
    InsertAtLabel:=True;
  end; {- if l_iLblPos>0 then begin -}
end; {- f InsertAtLabel(Var p_sLine : String; p_sLabel,p_sPiece : String) : Boolean; -}


function StripLeadZeroes(var p_sLine : String): String;
var l_iIdx : Integer;
    l_iLen : Integer;
    l_blCondition : Boolean;
begin
  l_iIdx:=1;
  l_iLen:=Length(p_sLine);
  p_sLine:=Trim(p_sLine);
  l_blCondition:=True;
  if not IsEmpty(p_sLine) then begin
    while (l_iIdx<=l_iLen) and l_blCondition  do begin
      l_blCondition:=p_sLine[l_iIdx]='0';
      Inc(l_iIdx);
    end; {- while (l_iIdx<=l_iLen) and l_blCondition  do begin -}
    Delete(p_sLine,1,Pred(l_iIdx));
  end; {- if not IsEmpty(p_sLine) then begin -}
  StripLeadZeroes:=p_sLine;
end; {- f StripLeadZero(var p_sLine : String): String; -}


function WordToStrZero(p_wDate : Word) : String;
var l_sDate : String;
begin
  l_sDate:=IntToStr(p_wDate);
  if p_wDate<10 then
    l_sDate:='0'+l_sDate;
  WordToStrZero:=l_sDate;
end;


function SeparateMils(p_lSum : LongInt) : String;
var l_sRes : String;
    l_iIdx : Integer;
begin
  l_sRes:=IntToStr(p_lSum);
  l_iIdx:=Length(l_sRes);
  while l_iIdx>3 do begin
    Dec(l_iIdx,3);
    Insert(' ',l_sRes,Succ(l_iIdx));
  end;
  SeparateMils:=l_sRes;
end;

(*
function GetDateStampFileName : String;
begin
  GetDateStampFileName:=WordToStrZero(YearOf(Now)-2000)+
                        WordToStrZero(MonthOf(Now))+
                        WordToStrZero(DayOf(Now))+
                        WordToStrZero(HourOf(Now));
end;


function ExtractFileFromResource(p_sResName,p_sFileName : String) : Boolean;
var l_oResStream: TResourceStream;
    l_oFileStream: TFileStream;
begin
  Result:=True;
  try
    l_oResStream:=TResourceStream.Create(hInstance, p_sResName, RT_RCDATA) ;
    l_oFileStream:=TFileStream.Create(p_sFileName, fmCreate);
    l_oFileStream.CopyFrom(l_oResStream, 0) ;
  except
    Result:=False;
  end;
  FreeAndNil(l_oFileStream);
  FreeAndNil(l_oResStream);
end;

*)

function HashString(p_sLine : String) : Longint;
var l_iIdx : Integer;
    l_iResult : integer;
begin
  l_iResult:=0;
  for l_iIdx:=1 to Length(p_sLine) do
  begin
    l_iResult:=l_iResult+Ord(p_sLine[l_iIdx]);
  end;
end;


function RoundEx(p_dValue : Double) : Double;
  var
    l_dScaledFractPart,
    T1: Double;
    T2:integer;
  begin
    l_dScaledFractPart:=Frac(p_dValue)*100;
    T1:=Frac(l_dScaledFractPart);
    l_dScaledFractPart:=Int(l_dScaledFractPart);
    T2:=round(T1*10);
    if T2>=5 then
    begin
      l_dScaledFractPart:=l_dScaledFractPart+1;
    end;
    if T2<=-5 then
    begin
      l_dScaledFractPart := l_dScaledFractPart - 1;
    end;
    RoundEx := Int(p_dValue) + l_dScaledFractPart/100;
  end;


function CharToInt(p_cChar : Char) : Integer;
begin
  CharToInt:=Ord(p_cChar)-$30;
end;


function WriteOutSum(p_dValue : Double) : String;
const c_iGrpRubles    = 1;
      c_iGrpTousends  = 2;
      c_iGrpMillions  = 3;
      c_iGrpMilliards = 4;
      c_iFormOne      = 1; // ��������,�������,������
      c_iFormTwo      = 2; // ���������, ��������, ������
      c_iFormFive     = 3; // ����������, ���������, �����

var l_sSum      : String;
    l_sRubles   : String;
    l_sCopecks  : String;
    l_iPoint    : Integer;
    l_asGroups  : array[1..5] of String;
    l_iLength   : Integer;
    l_iGroupLen : Integer;
    l_iGroup    : Integer;
    l_blDone    : Boolean;
    l_sLine     : String;
    l_cOne,
    l_cTen,
    l_cHundred  : Char;
    l_iForm     : Integer;
begin
  l_sLine:='';
  l_iForm:=c_iFormOne;
  l_sSum:=RealToStr(RoundEx(p_dValue),10,2);
  if l_sSum[1]='-' then begin
    Delete(l_sSum,1,1);
    l_sLine:=l_sLine+'����� ';
  end;

  FillChar(l_asGroups,sizeof(l_asGroups),0);

  {---< �������� ����� � ������� >---}
  l_iPoint:=Pos(c_cMoneyDelim,l_sSum);
  if l_iPoint>0 then begin
    l_sRubles:=Cut(l_sSum,1,Pred(l_iPoint));
    Delete(l_sSum,1,1);
    l_sCopecks:=l_sSum;
  end else begin
    l_sCopecks:='0';
  end;

  {---< �������� ����� �� ������ �� ��� ����� � ������ >---}
  l_iGroup:=1;
  l_blDone:=False;
  while not l_blDone do begin

    l_iLength:=Length(l_sRubles);
    if l_iLength<4 then begin

      {---< ������ ������ ������ >---}
      l_asGroups[l_iGroup]:=l_sRubles;
      l_blDone:=True;
    end else begin

      if l_iLength>3 then begin
        {---< ������ ������ ������ >---}
        l_asGroups[l_iGroup]:=Cut(l_sRubles,l_iLength-2,3);
      end;
    end;
    Inc(l_iGroup);
  end;

  {---< ������ ������ ��������� � ��������� ��� � ����������� ��������� >---}
  for l_iGroup:=High(l_asGroups) downto Low(l_asGroups) do begin

    l_iGroupLen:=Length(l_asGroups[l_iGroup]);

    if l_iGroupLen>0 then begin
      l_cOne:=#0;
      l_cTen:=#0;
      l_cHundred:=#0;
      case l_iGroupLen of
        1: begin
          l_cOne:=l_asGroups[l_iGroup,1];
        end;
        2: begin
          l_cTen:=l_asGroups[l_iGroup,1];
          l_cOne:=l_asGroups[l_iGroup,2];
        end;
        3: begin
          l_cHundred:=l_asGroups[l_iGroup,1];
          l_cTen:=l_asGroups[l_iGroup,2];
          l_cOne:=l_asGroups[l_iGroup,3];
        end;

      end;

      {---< ���� ���� �����.. >---}
      if l_cHundred<>#0 then begin
        case l_cHundred of
          '0': l_sLine:=l_sLine+'';
          '1': l_sLine:=l_sLine+'��� ';
          '2': l_sLine:=l_sLine+'������ ';
          '3': l_sLine:=l_sLine+'������ ';
          '4': l_sLine:=l_sLine+'��������� ';
          '5': l_sLine:=l_sLine+'������� ';
          '6': l_sLine:=l_sLine+'�������� ';
          '7': l_sLine:=l_sLine+'������� ';
          '8': l_sLine:=l_sLine+'��������� ';
          '9': l_sLine:=l_sLine+'��������� ';
        end;
      end;

      {---< ���� ���� �������.. >---}
      if l_cTen<>#0 then begin
        case l_cTen of
          '0': l_sLine:=l_sLine+'';
          //'1': l_sLine:=l_sLine+'';
          '2': l_sLine:=l_sLine+'�������� ';
          '3': l_sLine:=l_sLine+'�������� ';
          '4': l_sLine:=l_sLine+'����� ';
          '5': l_sLine:=l_sLine+'��������� ';
          '6': l_sLine:=l_sLine+'���������� ';
          '7': l_sLine:=l_sLine+'��������� ';
          '8': l_sLine:=l_sLine+'����������� ';
          '9': l_sLine:=l_sLine+'��������� ';
        end;

        if l_cTen='1' then begin
          case l_cOne of
            '0': l_sLine:=l_sLine+'������ ';
            '1': l_sLine:=l_sLine+'����������� ';
            '2': l_sLine:=l_sLine+'���������� ';
            '3': l_sLine:=l_sLine+'���������� ';
            '4': l_sLine:=l_sLine+'������������ ';
            '5': l_sLine:=l_sLine+'���������� ';
            '6': l_sLine:=l_sLine+'����������� ';
            '7': l_sLine:=l_sLine+'���������� ';
            '8': l_sLine:=l_sLine+'������������ ';
            '9': l_sLine:=l_sLine+'������������ ';
          end;
          l_cOne:=#0;
          l_iForm:=c_iFormFive;
        end;
      end;

      if (l_iGroup=c_iGrpMilliards) or
         (l_iGroup=c_iGrpMillions) or
         (l_iGroup=c_iGrpRubles) then begin
        case l_cOne of
          '0': begin  l_sLine:=l_sLine+' ';       l_iForm:=c_iFormFive; end;
          '1': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormOne;  end;
          '2': begin  l_sLine:=l_sLine+'��� ';    l_iForm:=c_iFormTwo;  end;
          '3': begin  l_sLine:=l_sLine+'��� ';    l_iForm:=c_iFormTwo;  end;
          '4': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormTwo;  end;
          '5': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormFive; end;
          '6': begin  l_sLine:=l_sLine+'����� ';  l_iForm:=c_iFormFive; end;
          '7': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormFive; end;
          '8': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormFive; end;
          '9': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormFive; end;
        end;
      end else begin
        if l_iGroup=c_iGrpTousends then begin
          case l_cOne of
            '0': begin  l_sLine:=l_sLine+' ';       l_iForm:=c_iFormFive; end;
            '1': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormOne;  end;
            '2': begin  l_sLine:=l_sLine+'��� ';    l_iForm:=c_iFormTwo;  end;
            '3': begin  l_sLine:=l_sLine+'��� ';    l_iForm:=c_iFormTwo;  end;
            '4': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormTwo;  end;
            '5': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormFive; end;
            '6': begin  l_sLine:=l_sLine+'����� ';  l_iForm:=c_iFormFive; end;
            '7': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormFive; end;
            '8': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormFive; end;
            '9': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormFive; end;
          end;
        end;
      end;

      {---< � ������ ����������� � ���������� >---}
      if l_iGroup=c_iGrpRubles then begin
        case l_iForm of
          c_iFormOne:  l_sLine:=l_sLine+' ����� ';
          c_iFormTwo:  l_sLine:=l_sLine+' ����� ';
          c_iFormFive: l_sLine:=l_sLine+' ������ ';
        end;
      end;
      if l_iGroup=c_iGrpTousends then begin
        case l_iForm of
          c_iFormOne:  l_sLine:=l_sLine+'������ ';
          c_iFormTwo:  l_sLine:=l_sLine+'������ ';
          c_iFormFive: l_sLine:=l_sLine+'����� ';
        end;
      end;
      if l_iGroup=c_iGrpMillions then begin
        case l_iForm of
          c_iFormOne:  l_sLine:=l_sLine+'������� ';
          c_iFormTwo:  l_sLine:=l_sLine+'�������� ';
          c_iFormFive: l_sLine:=l_sLine+'��������� ';
        end;
      end;
      if l_iGroup=c_iGrpMilliards then begin
        case l_iForm of
          c_iFormOne:  l_sLine:=l_sLine+'�������� ';
          c_iFormTwo:  l_sLine:=l_sLine+'��������� ';
          c_iFormFive: l_sLine:=l_sLine+'���������� ';
        end;
      end;
    end;
  end;

  {---< �������� ��������� >---}
  //l_iLength:=Length(l_sCopecks);
  l_cTen:=l_sCopecks[1];
  l_cOne:=l_sCopecks[2];
  case l_cTen of
    '0': l_sLine:=l_sLine+'';
    //'1': l_sLine:=l_sLine+'';
    '2': l_sLine:=l_sLine+'�������� ';
    '3': l_sLine:=l_sLine+'�������� ';
    '4': l_sLine:=l_sLine+'����� ';
    '5': l_sLine:=l_sLine+'��������� ';
    '6': l_sLine:=l_sLine+'���������� ';
    '7': l_sLine:=l_sLine+'��������� ';
    '8': l_sLine:=l_sLine+'����������� ';
    '9': l_sLine:=l_sLine+'��������� ';
  end;
  if l_cTen='1' then begin
    case l_cOne of
      '0': l_sLine:=l_sLine+'������ ';
      '1': l_sLine:=l_sLine+'����������� ';
      '2': l_sLine:=l_sLine+'���������� ';
      '3': l_sLine:=l_sLine+'���������� ';
      '4': l_sLine:=l_sLine+'������������ ';
      '5': l_sLine:=l_sLine+'���������� ';
      '6': l_sLine:=l_sLine+'����������� ';
      '7': l_sLine:=l_sLine+'���������� ';
      '8': l_sLine:=l_sLine+'������������ ';
      '9': l_sLine:=l_sLine+'������������ ';
    end;
    l_cOne:=#0;
  end;

  case l_cOne of
    '0': begin  l_sLine:=l_sLine+' ';       l_iForm:=c_iFormFive; end;
    '1': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormOne;  end;
    '2': begin  l_sLine:=l_sLine+'��� ';    l_iForm:=c_iFormTwo;  end;
    '3': begin  l_sLine:=l_sLine+'��� ';    l_iForm:=c_iFormTwo;  end;
    '4': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormTwo;  end;
    '5': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormFive; end;
    '6': begin  l_sLine:=l_sLine+'����� ';  l_iForm:=c_iFormFive; end;
    '7': begin  l_sLine:=l_sLine+'���� ';   l_iForm:=c_iFormFive; end;
    '8': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormFive; end;
    '9': begin  l_sLine:=l_sLine+'������ '; l_iForm:=c_iFormFive; end;
  end;
  case l_iForm of
    c_iFormOne:  l_sLine:=l_sLine+' ������� ';
    c_iFormTwo:  l_sLine:=l_sLine+' ������� ';
    c_iFormFive: l_sLine:=l_sLine+' ������ ';
  end;

  WriteOutSum:=l_sLine;
end;


begin
  g_sProgrammFolder:=ExtractFileDir(ParamStr(0));
  SlashIt(g_sProgrammFolder,c_cSlash);
end.





