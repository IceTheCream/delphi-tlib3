unit tcfg;

interface

{$i profile.inc}

uses Classes,
     SysUtils,
     Forms,
     Graphics,
     //Grids,
     DBGrids,
     IniFiles,

{$ifdef __LAZARUS__}
     controls,
{$endif}

     //tlib,
     tstr;

const csColumnVisibilitySuffix = '.visible';
      csFontDelim              = ';';

function SerializeFont(poFont : TFont) : String;
procedure DeSerializeFont(poFont : TFont;psLine : String);
procedure IniSaveForm(poForm : TForm{;  poIni : TIniFile = Nil});
procedure IniReadForm(poForm : TForm; {psIniName : String;} poIni : TIniFile=Nil);
procedure iniSaveFormEx(poForm : TForm);
procedure iniReadFormEx(poForm : TForm);

procedure IniSaveColumns(poGrid : TDBGrid; poIni : TIniFile = Nil);
procedure IniReadColumns(poGrid : TDBGrid; poIni : TIniFile = Nil);
function  IniOpen(psName : String) : Boolean;
function  IniObject : TIniFile;
function  IniReadInt(psSection,psName : String;piDefault : Integer = 0) : Integer;
function  IniReadReal(psSection,psName : String;prDefault : Real = 0.00) : Real;
function  IniReadString(psSection,psName : String; psDefaultStr : String = '') : String;
function  IniReadBool(psSection,psName : String;pblDefault : Boolean = False) : Boolean;
procedure IniWriteInt(psSection,psName : String; piValue : Integer);
procedure IniWriteString(psSection,psName,psLine : String);
procedure IniWriteBool(psSection,psName : String; pblValue : Boolean);
procedure IniClose;
procedure IniSaveIntArray(psSection,psName : String; paiArray : array of Integer);
function  IniReadArraySize(psSection,psName : String) : Integer;
procedure IniReadIntArray(psSection,psName : String; var paiArray : array of Integer; piCount : Integer);
procedure IniReadFont(psSection,psName : String;poFont : TFont);
procedure IniWriteFont(psSection,psName : String;poFont : TFont);

var lsPath  : String;

implementation


var ublFileOpened : Boolean = False;
    loXIniFile : TIniFile;


function SerializeFont(poFont : TFont) : String;
var liStyle : Integer;
begin
  with poFont do begin
    Result:=Name+csFontDelim+
            IntToStr(CharSet)+csFontDelim+
            IntToStr(Height)+csFontDelim;
    case Pitch of
      fpDefault : Result:=Result+'D'+csFontDelim;
      fpVariable: Result:=Result+'V'+csFontDelim;
      fpFixed   : Result:=Result+'F'+csFontDelim;
    end;
    Result:=Result+IntToStr(Size)+csFontDelim;
    liStyle:=0;
    if Style*[fsBold]<>[] then liStyle:=liStyle or 1;
    if Style*[fsItalic]<>[] then liStyle:=liStyle or 2;
    if Style*[fsUnderline]<>[] then liStyle:=liStyle or 4;
    if Style*[fsStrikeOut]<>[] then liStyle:=liStyle or 8;
    Result:=Result+IntToStr(liStyle);
  end;
end;


{---< ��������������� ��������� ������ �� ������ >---}
procedure DeSerializeFont(poFont : TFont; psLine : String);
var lsLine,
    liCode  : String;
    liPos,
    liStyle : Integer;
begin

  lsLine:=psLine;
  if not IsEmpty(Trim(lsLine)) then
  begin

    {---< ��� ������ >---}
    liPos:=Pos(csFontDelim,lsLine);
    if liPos>0 then begin
      poFont.Name:=Cut(lsLine,1,Pred(liPos));
      Delete(lsLine,1,1);

      {---< ������ >---}
      liPos:=Pos(csFontDelim,lsLine);
      if liPos>0 then begin
        poFont.Charset:=StrToIntDef(Cut(lsLine,1,Pred(liPos)),1);
        Delete(lsLine,1,1);

        {---< ������ >---}
        liPos:=Pos(csFontDelim,lsLine);
        if liPos>0 then begin
          poFont.Height:=StrToIntDef(Cut(lsLine,1,Pred(liPos)),1);
          Delete(lsLine,1,1);

          {---< Pitch >---}
          liPos:=Pos(csFontDelim,lsLine);
          if liPos>0 then begin
            liCode:=Cut(lsLine,1,Pred(liPos));
            case liCode[1] of
              'D': poFont.Pitch:=fpDefault;
              'V': poFont.Pitch:=fpVariable;
              'F': poFont.Pitch:=fpFixed;
            end;
            Delete(lsLine,1,1);

            {---< ������ >---}
            liPos:=Pos(csFontDelim,lsLine);
            if liPos>0 then begin
              poFont.Size:=StrToIntDef(Cut(lsLine,1,Pred(liPos)),1);
              Delete(lsLine,1,1);

              {---< ����� >---}
              liStyle:=StrToIntDef(lsLine,3);
              poFont.Style:=[];
              if liStyle and 1 = 1 then poFont.Style:=poFont.Style+[fsBold];
              if liStyle and 2 = 2 then poFont.Style:=poFont.Style+[fsItalic];
              if liStyle and 4 = 4 then poFont.Style:=poFont.Style+[fsUnderline];
              if liStyle and 8 = 8 then poFont.Style:=poFont.Style+[fsStrikeOut];
            end;
          end;
        end;
      end;
    end;
  end;
end;


procedure IniSaveForm(poForm : TForm{; poIni : TIniFile=Nil});
begin

  if ublFileOpened then begin

    IniWriteInt(poForm.Name,'LEFT',poForm.Left);
    IniWriteInt(poForm.Name,'TOP',poForm.Top);
    if (poForm.BorderStyle=bsSizeable) or
       (poForm.BorderStyle=bsSizeToolWin) then begin

      IniWriteInt(poForm.Name,'WIDTH',poForm.Width);
      IniWriteInt(poForm.Name,'HEIGHT',poForm.Height);
    end;
  end;
end;


procedure IniSaveFormEx(poForm : TForm);
begin

  if ublFileOpened then begin

    IniWriteInt('form','left',poForm.Left);
    IniWriteInt('form','top',poForm.Top);
    if (poForm.BorderStyle=bsSizeable) or
       (poForm.BorderStyle=bsSizeToolWin) then begin

      IniWriteInt('form','width',poForm.Width);
      IniWriteInt('form','height',poForm.Height);
    end;
  end;
end;


procedure IniReadForm(poForm : TForm; poIni : TIniFile=Nil);
var liVal : Integer;
begin

  if (poIni=Nil) and (ublFileOpened) then
    poIni:=loXIniFile;

  try

    if poIni<>Nil then begin

      liVal:=IniReadInt(poForm.Name,'LEFT');
      if liVal>=0 then
        poForm.Left:=liVal;

      liVal:=IniReadInt(poForm.Name,'TOP');
      if liVal>=0 then
        poForm.Top:=liVal;

      if (poForm.BorderStyle=bsSizeable) or
         (poForm.BorderStyle=bsSizeToolWin) then begin

        liVal:=IniReadInt(poForm.Name,'WIDTH',800);
        if liVal>=0 then
          poForm.Width:=liVal;

        liVal:=IniReadInt(poForm.Name,'HEIGHT',600);
        if liVal>=0 then
          poForm.Height:=liVal;
      end;
    end;
  except
  end;
end;


procedure iniReadFormEx(poForm : TForm);
begin

  if ublFileOpened then begin

    poForm.Left:=IniReadInt('form','left',poForm.Left);
    poForm.Top:=IniReadInt('form','top',poForm.Top);
    if (poForm.BorderStyle=bsSizeable) or
       (poForm.BorderStyle=bsSizeToolWin) then begin

      poForm.Width:=IniReadInt('form','width',poForm.Width);
      poForm.Height:=IniReadInt('form','height',poForm.Height);
    end;
  end;
end;



procedure IniSaveColumns(poGrid : TDBGrid; poIni : TIniFile = Nil);
var liIdx    : Integer;
    loColumn : TColumn;
begin

  if (poIni=Nil) and (ublFileOpened) then
    poIni:=loXIniFile;
	
  try
  
    if poIni<>Nil then begin
	
      for liIdx:=0 to Pred(poGrid.Columns.Count) do begin
	  
        loColumn:=poGrid.Columns.Items[liIdx] as TColumn;
        if loColumn.FieldName<>'' then begin
		
          poIni.WriteInteger(poGrid.Name,loColumn.FieldName,
            loColumn.Width);
          if loColumn.Visible then
            poIni.WriteInteger(poGrid.Name,loColumn.FieldName+csColumnVisibilitySuffix,1)
          else
            poIni.WriteInteger(poGrid.Name,loColumn.FieldName+csColumnVisibilitySuffix,0);
        end;
      end;
    end;
  except
    //???
  end;
end;


procedure IniReadColumns(poGrid : TDBGrid; poIni : TIniFile = Nil);
var liIdx    : Integer;
    loColumn : TColumn;
    //i : integer;
begin

  if (poIni=Nil) and (ublFileOpened) then
  
    poIni:=loXIniFile;
  try
  
    if poIni<>Nil then begin
	
      for liIdx:=0 to Pred(poGrid.Columns.Count) do begin

        loColumn:=poGrid.Columns.Items[liIdx] as TColumn;
        if loColumn.FieldName<>'' then begin

          //i:=poIni.ReadInteger(poGrid.Name,loColumn.FieldName,
          //  poGrid.Columns.Items[liIdx].Width);
          //loColumn.Width:=i;
          loColumn.Width:=poIni.ReadInteger(poGrid.Name,loColumn.FieldName,
            poGrid.Columns.Items[liIdx].Width);
          loColumn.Visible:=(poIni.ReadInteger(poGrid.Name,loColumn.FieldName+csColumnVisibilitySuffix,1)=1);
        end;
      end;
    end;
  except
    //????
  end;
end;


function IniOpen(psName : String) : Boolean;
begin

  Result:=False;
  if not ublFileOpened then begin

    loXIniFile:=TIniFile.Create(psName);
    ublFileOpened:=True;
    Result:=True;
  end;
end;


procedure IniWriteString(psSection, psName, psLine : String);
begin

  if ublFileOpened then
    loXIniFile.WriteString(psSection,psName,psLine);
end;


procedure IniWriteInt(psSection, psName : String; piValue : Integer);
begin

  if ublFileOpened then
    loXIniFile.WriteInteger(psSection,psName,piValue);
end;


procedure IniWriteBool(psSection, psName : String; pblValue : Boolean);
begin

  if ublFileOpened then
    if pblValue then
      loXIniFile.WriteInteger(psSection,psName,1)
    else
      loXIniFile.WriteInteger(psSection,psName,0);
end;


function IniReadString(psSection, psName : String; psDefaultStr : String = '') : String;
begin
  
  Result:=psDefaultStr;
  if ublFileOpened then
    Result:=loXIniFile.ReadString(psSection,psName,psDefaultStr);
end;


function IniReadInt(psSection, psName : String; piDefault : Integer = 0) : Integer;
begin

  Result:=piDefault;
  if ublFileOpened then
    Result:=loXIniFile.ReadInteger(psSection,psName,piDefault);
end;


function IniReadReal(psSection, psName : String; prDefault : Real = 0.00) : Real;
begin

  Result:=prDefault;
  if ublFileOpened then
    Result:=loXIniFile.ReadFloat(psSection,psName,prDefault);
end;


function IniReadBool(psSection, psName : String; pblDefault : Boolean = False) : Boolean;
begin

  Result:=pblDefault;
  if ublFileOpened then
    if pblDefault then
      Result:=loXIniFile.ReadInteger(psSection,psName,1)=1
	  
    else
      Result:=loXIniFile.ReadInteger(psSection,psName,0)=1;
end;


function IniObject : TIniFile;
begin

  Result:=loXIniFile;
end;


procedure IniClose;
begin

  if ublFileOpened then begin
  
    FreeAndNil(loXIniFile);
    ublFileOpened:=False;
  end;
end;


procedure IniSaveIntArray(psSection, psName : String; paiArray : array of Integer);
var liIdx,
    liCount : Integer;
begin

  if ublFileOpened then begin
  
    liCount:=Length(paiArray);
    IniWriteInt(psSection,psName+'Count',liCount);
    for liIdx:=0 to liCount do begin
	
      IniWriteInt(psSection,psName+''+IntToStr(liIdx),paiArray[liIdx]);
    end;
  end;
end;


function IniReadArraySize(psSection, psName : String) : Integer;
begin

  Result:=0;
  if ublFileOpened then
    Result:=IniReadInt(psSection,psName+'Count');
end;


procedure IniReadIntArray(psSection, psName : String; var paiArray : array of Integer; piCount : Integer);
var liIdx : Integer;
begin

  if ublFileOpened then begin
  
    for liIdx:=0 to Pred(piCount) do begin
	
      paiArray[liIdx]:=IniReadInt(psSection,psName+''+IntToStr(liIdx));
    end;
  end;
end;


procedure IniReadFont(psSection, psName : String; poFont : TFont);
var lsLine : String;
begin

  if ublFileOpened then begin
  
    lsLine:=IniReadString(psSection,psName,'');
    DeSerializeFont(poFont,lsLine);
  end;
end;


procedure IniWriteFont(psSection, psName : String; poFont : TFont);
begin

  if ublFileOpened then
    loXIniFile.WriteString(psSection,psName,SerializeFont(poFont));
end;


end.
