// @abstract(������ ��� ������ � WinAPI ���������� TLib 4.0)
// @author(���������� �.�.  pakhomenkov@gmail.com)
unit twin;


interface

{$WARN SYMBOL_PLATFORM OFF }
{$i profile.inc}


uses Windows, SysUtils, Registry, ComObj, ShlObj, ShellApi, ActiveX,
      Classes, Graphics, Forms

{$ifdef __LAZARUS__}
    ,Win32Int, InterfaceBase, jwawindows, jwawinuser, fileutil
{$endif}

{$ifdef __DELPHI__}
,MMSystem{-}, Types{-}
{$endif}
;

{$i tconst.inc}

type  
      TDummyUnionName    = record
        case Integer of
          0: (uTimeout: UINT);
          1: (uVersion: UINT);
      end;
      
	  PNewNotifyIconData = ^TNewNotifyIconData;
      TNewNotifyIconData = record
        dwSize           : DWord;
        hnWnd            : HWnd;
        uID              : UInt;
        uFlags           : UInt;
        uCallbackMessage : UInt;
        hIcon            : HIcon;
        szTip            : array [0..127] of Char;  {Version 5.0 is 128 chars, old ver is 64 chars}
        dwState          : DWord;                   {Version 5.0}
        dwStateMask      : DWord;                   {Version 5.0}
        szInfo           : array [0..255] of Char;  {Version 5.0}
        DummyUnionName   : TDummyUnionName;
        szInfoTitle      : array [0..63] of Char;   {Version 5.0}
        dwInfoFlags      : DWord;                   {Version 5.0}
      end;

      TRecodeType=(rtNoRecode,rtOEM2ANSI,rtANSI2OEM);

      TWinVersion = (wvUnknown,wv95,wv98,wvME,wvNT3,wvNT4,wvW2K,wvXP,wv2003);

{** @abstract(������� ������������ ������ ������� ���������, ��������� � psProgramm)
    @param(psProgramm ���� � ����������� ���������)
    @param(psArguments ��������� ���������)
    @param(pblWait ���� True, �� �������� ��������� ����� �������, ���� ���������� ��������� ����������,
	               ���� False - ��������� ����������.)
    @param(pblHideWindow ���� True, �� ���� ���������� ��������� ����� ������,
	                     ���� False - ����� ��������)
    @return(@true, ���� ��������� ������� �����������, ����� @false)
    @code(easyRun('C:\Windows\System32\calc.exe','',False,False);)}
//function  easyRun(psProgramm,psArguments : String; pblWait : Boolean = False; pblHideWindow : Boolean = False) : Boolean;

(*
{** @abstract(������� ������� �� ������� ���� � �������� ������� � ������� �����������)
    @param(psTroubleDesc �������� ���������)
    @param(psDetail ���������� ���������)
    @return(@true, ���� ��������� ������� ��������, ����� @false)
    @code(notice('���������','�������� ����������� ���������');)}
function  notice(psTroubleDesc, psDetail : String) : Boolean;

{** @abstract(������� ������� �� ������� ���� � �������� ������� � ����� �������� �
              ���������� ���������� ����� �� ���� ��������� ��������.)
    @param(psTroubleDesc �������� ���������)
    @param(psDetail ���������� ���������)
    @return(@true, ���� ���� ������ ������ '��', ����� @false)
    @code(warnYesNo('��������!','��� ������ �� ����� C: ����� ����������. ����������?');)}
function  warnYesNo(psTroubleDesc, psDetail : String) : Boolean;

{** @abstract(������� ������� �� ������� ���� � �������� ������� � ������� ������)
    @param(psTroubleDesc �������� ���������)
    @param(psDetail ���������� ���������)
    @return(@true, ���� ��������� ������� ��������, ����� @false)
    @code(fatalError('������!','��������� �������� ���������!'))}
function  fatalError(psErrorDesc, psDetail : String) : Boolean;
*)

{** @abstract(������� ���������� ���� � ����� Program Files (x86))
    @return(������, ���������� ����)
    @code(s:=getProgramFolder();)}
function  getProgramFilesFolder : String;

{** @abstract(������� ���������� ���� � ����� Windows)
    @return(������, ���������� ����)
    @code(s:=getWindowsFolder();) }
function  getWindowsFolder : String;

{** @abstract(������� ����������, ���� �� ��� �������� ��������� � ����� ����������)
    @param(psMutexName ���������)
    @return(@true, ���� ��������� ��� ���� ��������, @false � ��������� ������)
    @code(if detect2ndInstance('lztest') then //��������� ��� ��������)}
function  detect2ndInstance(psMutexName : String) : Boolean;

{** @abstract(������� ������� ����� ����� � ��������� �����������)
    @param(psPath ���� � ��������� �����)
    @param(psWorkFolder �����, � ������� ����� ������ �����)
    @param(psDesc �������� ������)
    @param(psIconPath ���� � �����, ����������� ������)
    @param(piIconIdx ������ ������ � �����, ���� �� ��� ���������)
    @param(psLinkName ��� ������)
    @return(@true, ���� ����� ������� ������, @false � ��������� ������)
    @code(if createLink('C:\Windows\System32\calc.exe','C:\','����� ��� Calc','C:\Windows\System32\calc.exe',0,'C:\Calc.lnk') then //link created)}
function  createLink(psPath, psWorkFolder, psDesc, psIconPath : String; piIconIdx : Integer; psLinkName : String): Boolean;

{** @abstract(������� ������������ ������ �� ��������� OEM � ��������� ANSI)
    @param(psLine �������� ������)
    @return(����������������� ������)
    @code(s:=oem2Ansi('�� ������ ������');) }
function  oem2Ansi(psLine : String) : String;

{** @abstract(������� ������������ ������ �� ��������� ANSI � ��������� OEM)
    @param(psLine �������� ������)
    @return ����������������� ������)
    @code(s:=ansi2Oem('��� �������� ������');) }
function  Ansi2Oem(psLine : String) : String;


{** @abstract(������� ���������� �������� ��������� ������)
    @return ������, ���������� �������� ������
    @code(fatalError('������!',getSystemErrorDescription))}
function  getSystemErrorDescription : String;

{** @abstract(������� �������� ������ ������ �����)
    @param poHandle
    @param psTitle ��������� ���� ������ �����
    @return ������, ���������� ���� � ��������� �����
    @code(folder:=selectFolder(TWin32WidgetSet(WidgetSet).AppHandle,'�������� �������'); //Lazarus)
    @html <br>**
    @code(folder:=selectFolder(Application.Handle,'�������� �������'); //Delphi) }
function  selectFolder(poHandle : THandle; psTitle : String = '�������� �������') : String;


{** @abstract(������� �������� ������ ��������� � ��������� ����.)
    @param(poForm - ����� ����)
    @param(psTrayHint - ���������, ������������ ��� ��������� ������� ���� �� ������ � ����)
    @param(poIcon - ������ ��� ���������� � ����)
    @param(piTrayMsg - ������������� ���������� �������, ������������ ��� ����� �� ������)
    @param(psTrayMsg - ��������� ������������� ���������� �������, ������������ ��� ����� �� ������)
    @code(putInTray(Form1,'����',Icon,WM_TRAY);) }
procedure putInTray(poForm : TForm; psTrayHint : String; poIcon : TIcon; piTrayMsg : Integer; psTrayMsg : String = 'WM_PROGTRAY');

{** @abstract(������� �������� ������ ��������� � ��������� ����.)
    @param(poForm - ����� ����)
    @param(psTrayHint - ���������, ������������ ��� ��������� ������� ���� �� ������ � ����)
    @param(poIcon - ������ ��� ���������� � ����)
    @param(piTrayMsg - ������������� ���������� �������, ������������ ��� ����� �� ������)
    @param(psTrayMsg - ��������� ������������� ���������� �������, ������������ ��� ����� �� ������)
    @code(changeIconInTray(Form1,'������ ����',Icon,WM_TRAY);) }
procedure changeIconInTray(poForm : TForm; psTrayHint : String; poIcon : TIcon; piTrayMsg : Integer; psTrayMsg : String = 'WM_PROGTRAY');

{** @abstract(������� ������� ������ ��������� �� ���������� ����.)
    @param(poForm - ����� ����)
    @code(removeFromTray(Form1);) }
procedure removeFromTray(poForm : TForm);

{** @abstract(������� ����������� ��������� ���� ���������)
    @param(pblTop ���� @true, �� ���� ���������� ������ ����, ���� @false - ������������ � ���������� ���������)
    @code(safeTop(True);) }
procedure safeTop(pblTop : Boolean = True);

{** @abstract(������� ���������� ���� � ��������� ���������)
    @param(piFolder ������������� ���������� ��������)
      piFolder ����� ��������� ���� �� ��������� ��������:
	  CSIDL_BITBUCKET Recycle bin
	  CSIDL_CONTROLS  Control Panel
	  CSIDL_DESKTOP   Windows desktop
	  CSIDL_DESKTOPDIRECTORY  Desktop files
	  CSIDL_DRIVES    My Computer
	  CSIDL_FONTS     fonts.
	  CSIDL_NETHOOD   network neighborhood files
	  CSIDL_NETWORK   Network Neighborhood
	  CSIDL_PERSONAL  documents.
	  CSIDL_PRINTERS  Printers
	  CSIDL_PROGRAMS  user's program groups
	  CSIDL_RECENT    recent documents.
	  CSIDL_SENDTO    Send To menu items.
	  CSIDL_STARTMENU Start menu items.
	  CSIDL_STARTUP   Startup program group.
	  CSIDL_TEMPLATES document templates.
    @return(���� � ��������� �����)
    @code(getSystemFolder(CSIDL_DRIVES);)}
function  getSystemFolder(piFolder : Integer) : String;

{** @abstract(������� ������������ ������ �������� ���������)
    @param(psCommand ����������� ���������)
    @param(psArgs ������������ ���������)
    @param(psFolder ������� �����)
    @param(pblShow @true - ���������� ��������� ����� ������, @false - ���)
    @return(@true � ������ �������� �������, ����� @false)
    @code(easyExec('C:\Windows\System32\calc.exe','','C:\',True))}
function  easyExec(psCommand, psArgs, psFolder : String; pblShow : Boolean=True) : Boolean;

{** @abstract(������� ���������� ������� ��� ����������.)
    @return(������ � ������ ����������)
    @code(name:=getComputerName) }
function getComputerNetName: string;

{** @abstract(������� ������������ ������ �������� ��������� � ������� � ����������)
    @param(psCommand ����������� ���������)
    @param(psArgs ������������ ���������)
    @param(psFolder ������� �����)
    @return(ciSuccessExecution, ���� ��������� ���������� � ������������ �����������.
             ciSuccessButStillRunning, ���� ��������� ���������� ��������.
             ciExecutionFailed, ���� ������ ��� ��������)
    @code(easyExecAndWait('C:\Windows\System32\calc.exe','','C:\'))}
function easyExecAndWait(psCommand, psArgs, psFolder : string) : Integer;

{** @abstract(������� ������� ������� � �������� ����������. ����� ������������� ������� ������ ����
    ������. ��� �������� ����������� CloseHandle(mutex_handle))
    @param(psMutexName ��������� ��������)
    @return(����� ��������)
    @code(hnd:=createNewMutex('m001FE23');) }
function createNewMutex(psMutexName : String) : THandle;

{** @abstract(������� ����������, ���� �� �������� �������� ��������. ���������� ����� ��
              createNewMutex.)
    @return(@true, ���� ����� ������� ��� ����������, ����� @false.)
    @code(hnd:=createNewMutex('m001FE23'); if isMutexAlreadyExists then //��� ���� ����� �������) }
function isMutexAlreadyExists : Boolean;


{** @abstract(������� ���������� ����� ���� �� ����� ��� ������))
    @param(psClassName ��� ������)
    @return(����� ����)
    @code(hnd:=findWindowByClassName('TForm1');) }
function findWindowByClassName(psClassName : String) : THandle;

{** @abstract(������� ������� ���� � �������� ������� �� �������� ����))
    @param(phWindowHandle ����� ����)
    @code(moveWindowToForeground(hnd);) }
procedure moveWindowToForeground(phWindowHandle : THandle);


//function  PlayWaveFromResource(phInstance : LongWord; psResourceName : String) : Boolean;
(*
{* abstract ������� ������ ���� ���������}
procedure hideProgram;

{* abstract ������� ���������� ���� ���������}
procedure showProgram;
{* abstract ������� ��������� ���������� ������ ����������� ����
    param(poForm - �����, ��� ������� ������� ����������� ������)
    code(flyIn(Form1);)}
procedure flyIn(poForm : TForm);

{* abstract ������� ��������� ���������� ������ ���������� ����
    param(poForm - �����, ��� ������� ������� ����������� ������)
    code(flyOut(Form1);)}
procedure flyOut(poForm : TForm);

{* abstract ������� ���������� ������ ������������ �������
   return(������������ �������� ��� ������ ��)
      wvUnknown, wvNT3, wvNT4, wv95, wv98, wvME, wvW2K, wvXP, wv2003}
function  DetectWinVersion : TWinVersion;
//procedure FlashForm(piCount : Integer = 3; pwFlags : DWord = 0);
*)

var ghAppHandle : THandle;


implementation


uses tlib,tstr, Controls;


var uoIconData : TNewNotifyIconData;

(*
function easyRun(psProgramm, psArguments : String; pblWait : Boolean = False; pblHideWindow : Boolean = False) : Boolean;
var loStartupInfo : TStartupInfo;
    loProcInfo    : TProcessInformation;
begin

  //***** �������������� ������ loStartupInfo � loProcInfo � �������������
  loProcInfo.hProcess:=0;
  loStartupInfo.cb:=0;
  FillChar(loStartupInfo, SizeOf(loStartupInfo),#0);
  loStartupInfo.cb := SizeOf(loStartupInfo);
  FillChar(loProcInfo, SizeOf(loProcInfo),#0);
  if pblHideWindow then begin

    loStartupInfo.wShowWindow:=SW_HIDE;            //����� �� ���� ����
    loStartupInfo.dwFlags:=STARTF_USESHOWWINDOW;   //����� �� ���� ����
  end;

  //***** ���������� ��������� ���������
  if CreateProcess(
    PChar(psProgramm),
    PChar(psArguments),
    nil,
    nil,                   { ������������ �� ��������� }
    false,                 { �� ����������� ������� }
    NORMAL_PRIORITY_CLASS, { ����� �������� �� ��������� }
    nil,                   { ���������� ����� �� ��������� }
    nil,                   { ������� ���������� �� ��������� }
    loStartupInfo,         { ��������� ���������� }
    loProcInfo)            { � � ��� ������ ������� ���������� � �������� �������� }
  then
  begin

    if pblWait then begin

      //***** ��������, ���� ��� ����������
      WaitForSingleObject(loProcInfo.hProcess, INFINITE);
    end;

    //***** ������� �����
    CloseHandle(loProcInfo.hProcess);
    CloseHandle(loProcInfo.hThread);
    EasyRun:=True;
  end else begin
  
    //***** ������ ��������� �� ������
    EasyRun:=False;
  end;  
end;
*)
(*
function notice(psTroubleDesc, psDetail : String) : Boolean;
begin

  Notice:=MessageBox(ghAppHandle,PChar(ToSystemCodepage(psTroubleDesc+LF+psDetail)),
    PChar('��������!'),MB_APPLMODAL OR MB_OK OR MB_ICONINFORMATION)=IDOK;
end;


function warnYesNo(psTroubleDesc, psDetail : String) : Boolean;
begin

  WarnYesNo:=MessageBox(ghAppHandle,PChar(ToSystemCodepage(psTroubleDesc+LF+psDetail)),
    PChar('��������!'),MB_APPLMODAL OR MB_YESNO OR MB_ICONQUESTION)=IDYes;
end;


function fatalError(psErrorDesc, psDetail : String) : Boolean;
begin

  FatalError:=MessageBox(ghAppHandle,PChar(ToSystemCodepage(psErrorDesc+LF+psDetail)),
    PChar('��������� ������'),MB_APPLMODAL OR MB_OK OR MB_ICONERROR)=IDOk;
end; 
*)

function getProgramFilesFolder : String;
var loReg : TRegistry;
    lsFolder : String;
begin

  Result:='';
  loReg:=TRegistry.Create;
  loReg.RootKey:=HKEY_LOCAL_MACHINE;
  if loReg.OpenKey('\SOFTWARE\Microsoft\Windows\CurrentVersion',False) then begin

    try

      lsFolder:=loReg.ReadString('ProgramFilesDir');
      Result:=SlashIt(lsFolder);
    finally

      loReg.CloseKey;
      FreeAndNil(loReg);
    end;
  end;
end;


function getWindowsFolder : String;
var lcBuf : array[1..MAX_PATH] of Char;
begin

  GetWindowsDirectory(@lcBuf,MAX_PATH);
  Result:=StrPas(@lcBuf);
end;


function detect2ndInstance(psMutexName : String) : Boolean;
var lhMutex   : integer;
begin

  Result:=False;
  lhMutex:=CreateMutex(nil,True,PChar(psMutexName));  { ������� ������� }
  if GetLastError<>0 then begin
  
    Detect2ndInstance:=True; { ������ - ������� ��� ������ }
  end;
  ReleaseMutex(lhMutex);
end; 


function createLink(psPath, psWorkFolder, psDesc, psIconPath : String; piIconIdx : Integer; psLinkName : String): Boolean;
var loLinkFile  : IPersistFile;
    loShellObj  : IUnknown;
    loShellLnk  : IShellLink;
    lwsLinkName : WideString;
begin

  loShellObj:=CreateComObject(CLSID_ShellLink);
  loLinkFile:=loShellObj as IPersistFile;
  loShellLnk:=loShellObj as IShellLink;
  loShellLnk.SetPath(PChar(psPath));
  loShellLnk.SetWorkingDirectory(PChar(psWorkFolder));
  loShellLnk.SetDescription(PChar(psDesc));
  loShellLnk.SetIconLocation(PChar(psIconPath),piIconIdx);
  lwsLinkName:=psLinkName;
  CreateLink:=loLinkFile.Save(PWideChar(lwsLinkName),False)=S_OK;
end;


function oem2Ansi(psLine : String) : String;
var lsLine : String;
    //lacBuf : array[1..1024] of Char;
    pcBuf  : PChar;
    liLen  : Integer;
begin

  Result:='';
  if not IsEmpty(psLine) then begin

    liLen:=Length(psLine);
    pcBuf:=StrAlloc(liLen+1);
    OemToCharBuff(PChar(psLine),pcBuf,liLen);
    lsLine:=StrPas(pcBuf);
    SetLength(lsLine,liLen);
    StrDispose(pcBuf);
    Result:=lsLine;
  end;
end;


function ansi2Oem(psLine : String) : String;
var lsLine : String;
    //lacBuf : array[1..1024] of Char;
    pcBuf  : PChar;
    liLen  : Integer;
begin

  Result:='';
  if not IsEmpty(psLine) then begin

    liLen:=Length(psLine);
    pcBuf:=StrAlloc(liLen+1);
    CharToOemBuff(PChar(psLine),pcBuf,liLen);
    lsLine:=StrPas(pcBuf);
    SetLength(lsLine,liLen);
    StrDispose(pcBuf);
    Result:=lsLine;
  end;
end;


function getSystemErrorDescription : String;
var ldwErrCode : DWord;
    lacMsg     : Array[1..256] of Char;
begin

  Result:='';
  ldwErrCode:=GetLastError;
  if ldwErrCode<>NoError then begin

    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM or FORMAT_MESSAGE_IGNORE_INSERTS,
                  nil,ldwErrCode,0,@lacMsg,
                  sizeof(lacMsg)-1,Nil);
    Result:=StrPas(@lacMsg);
  end;
end;


function selectFolder(poHandle : THandle; psTitle : String = '�������� �������') : String;
var lpoItemID      : PItemIDList;
    loBrowseInfo   : TBrowseInfo;
    lacDisplayName : array[0..MAX_PATH] of char;
    lacTempPath    : array[0..MAX_PATH] of char;
begin

  Result:='';
  FillChar({%H-}loBrowseInfo,sizeof(TBrowseInfo),#0);
  loBrowseInfo.hwndOwner:=poHandle;
  loBrowseInfo.pszDisplayName:=@lacDisplayName;
  loBrowseInfo.lpszTitle:=PChar(ToSystemCodepage(psTitle));
  loBrowseInfo.ulFlags:=BIF_RETURNONLYFSDIRS;// or BIF_RETURNFSANCESTORS;
  
  {$ifdef __DELPHI__}
  lpoItemID:=SHBrowseForFolder(loBrowseInfo);
  {$else}
  lpoItemID:=SHBrowseForFolder(@loBrowseInfo);
  {$endif}
  
  if lpoItemId<>nil then begin
  
    SHGetPathFromIDList(lpoItemID, lacTempPath);
    SelectFolder:=StrPas(@lacTempPath);
    GlobalFreePtr(lpoItemID);
  end; 
end;


procedure putInTray(poForm : TForm; psTrayHint : String; poIcon : TIcon; piTrayMsg : Integer; psTrayMsg : String = 'WM_PROGTRAY');
var lcpTrayHint : PChar;
begin

  lcpTrayHint:=PChar(psTrayHint);
  RegisterWindowMessage(PChar(psTrayMsg));
  with uoIconData do begin

    dwSize:=sizeof(uoIconData);
    hnWnd:=poForm.Handle;
    UId:=ciNormIconID;
    UFlags:=NIF_ICON or NIF_TIP or NIF_MESSAGE;
    uCallbackMessage:=piTrayMsg;
    hIcon:=poIcon.Handle;
    //szTip:=psTrayHint;
  end;
  StrCopy(uoIconData.szTip,lcpTrayHint);

  {$ifdef __DELPHI__}
  Shell_NotifyIcon(NIM_ADD,@uoIconData);
  {$else}
  Shell_NotifyIcon(NIM_ADD,PNOTIFYICONDATAW(@uoIconData));
  {$endif}

  //StrDispose(lcpTrayHint);
end;


procedure changeIconInTray(poForm : TForm; psTrayHint : String; poIcon : TIcon; piTrayMsg : Integer; psTrayMsg : String = 'WM_PROGTRAY');
var lcpTrayHint : PChar;
begin

  lcpTrayHint:=PChar(psTrayHint);
  RegisterWindowMessage(PChar(psTrayMsg));
  with uoIconData do begin

    dwSize:=sizeof(uoIconData);
    hnWnd:=poForm.Handle;
    UId:=ciNormIconID;
    UFlags:=NIF_ICON or NIF_TIP or NIF_MESSAGE;
    uCallbackMessage:=piTrayMsg;
    hIcon:=poIcon.Handle;
    //szTip:=psTrayHint;
  end;
  StrCopy(uoIconData.szTip,lcpTrayHint);

  {$ifdef __DELPHI__}
  Shell_NotifyIcon(NIM_MODIFY,@uoIconData);
  {$else}
  Shell_NotifyIcon(NIM_MODIFY,PNOTIFYICONDATAW(@uoIconData));
  {$endif}

  //rDispose(lcpTrayHint);
end;


procedure removeFromTray(poForm : TForm);
begin

  with uoIconData do begin
  
    hnWnd:=poForm.Handle;
    dwSize:=sizeof(uoIconData);
    UId:=ciNormIconID;
  end;
  
  {$ifdef __DELPHI__}
  Shell_NotifyIcon(NIM_DELETE,@uoIconData);
  {$else}
  Shell_NotifyIcon(NIM_DELETE,PNOTIFYICONDATAW(@uoIconData));
  {$endif}
  
end;


procedure safeTop(pblTop : Boolean = True);
begin

  if pblTop then
    SetWindowPos(ghAppHandle,HWND_TOPMOST,0,0,0,0,SWP_NOSIZE or SWP_NOMOVE)
  else
    SetWindowPos(ghAppHandle,HWND_NOTOPMOST,0,0,0,0,SWP_NOSIZE or SWP_NOMOVE);
	
end;


function getSystemFolder(piFolder : Integer) : String;
var lpoIdl : PItemIDList;
    lacBuf : array[1..MAX_PATH] of Char;
begin

  GetSystemFolder:='';
  FillChar({%H-}lacBuf,MAX_PATH,0);
  if SHGetSpecialFolderLocation(ghAppHandle,piFolder,lpoIdl)=NOERROR then begin

    if SHGetPathFromIDList(lpoIdl,@lacBuf) then begin

      GetSystemFolder:=StrPas(@lacBuf);
    end;
  end;
end;


function easyExec(psCommand, psArgs, psFolder : String; pblShow : Boolean=True) : Boolean;
var liStatus : Integer;
    lpcArgs,
    lpcFolder : PChar;
begin

  if pblShow then
    liStatus:=SW_SHOWDEFAULT
  else
    liStatus:=SW_HIDE;

  if IsEmpty(psFolder) then
    lpcFolder:=Nil
  else
    lpcFolder:=PChar(psFolder);  
   
  if IsEmpty(psArgs) then
    lpcArgs:=Nil
  else
    lpcArgs:=PChar(psArgs);

  EasyExec:= ShellExecute(HInstance,
                          PChar('open'),
                          PChar(psCommand),
                          lpcArgs,
                          lpcFolder,
                          liStatus)<=32;
end;


function getComputerNetName: string;
var loBuf : array[0..255] of Char;
    dwSize : DWord;
begin

  Result:='';
  dwSize:=256;
  if GetComputerName(loBuf, dwSize) then
    Result:=loBuf;
end;


function easyExecAndWait(psCommand, psArgs, psFolder : string): Integer;
var
  loInfo : TShellExecuteInfo;
  dwExitCode : DWORD;
begin

  dwExitCode:=0;
  FillChar({%H-}loInfo, SizeOf(loInfo), 0); //sizeof(TShellExecuteInfo)
  loInfo.cbSize:=SizeOf(loInfo);
  loInfo.fMask:=SEE_MASK_DOENVSUBST or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS; //SEE_MASK_NOCLOSEPROCESS
  //loInfo.Wnd:=ghAppHandle; ++
  loInfo.lpFile:=PChar(psCommand);
  loInfo.lpParameters:=PChar(psArgs);
  loInfo.lpDirectory:=PChar(psFolder);
  loInfo.nShow:=CmdShow; //ShowCommands[InitialState];
  {$T+} // --
  if ShellExecuteExA(@loInfo) then begin
    WaitForInputIdle(loInfo.hProcess, INFINITE); //--
    WaitforSingleObject(loInfo.hProcess,INFINITE);
    GetExitCodeProcess(loInfo.hProcess, dwExitCode);
    if dwExitCode=STILL_ACTIVE then
      Result:=ciSuccessButStillRunning
    else
      Result:=ciSuccessExecution;
  end
  else
    Result:=ciExecutionFailed;
  {$T-}
end;


function createNewMutex(psMutexName : String) : THandle;
begin

  Result:=CreateMutex(nil,True,PChar(psMutexName));
end;


function isMutexAlreadyExists : Boolean;
begin

  Result:=GetLastError=ERROR_ALREADY_EXISTS;
end;


function findWindowByClassName(psClassName : String) : THandle;
begin

   Result:=FindWindow(PChar(psClassName),Nil);
end;


procedure moveWindowToForeground(phWindowHandle : THandle);
begin

  if phWindowHandle<>0 then
  begin

    ShowWindow(phWindowHandle,SW_SHOWNORMAL);
    SetForegroundWindow(phWindowHandle);
  end;
end;


{$region '_Trash'}
(*
function PlayWaveFromResource(phInstance : LongWord; psResourceName : String) : Boolean;
var lhResource : THandle;
    lpData     : Pointer;
    lblOk      : Boolean;
begin

  lhResource:=LoadResource(phInstance, FindResource(phInstance, PChar(psResourceName), RT_RCDATA));
  try

    lpData := LockResource(lhResource);
    lblOk:=lpData <> nil;
    {---< ��������� ���� >---}
    if lblOk then
      PlaySound(lpData, 0, SND_MEMORY);

  finally

    FreeResource(lhResource);
  end;
  PlayWaveFromResource:=lblOk;
end;
procedure hideProgram;
var llStyle : LongInt;
begin

  llStyle:=GetWindowLong(ghAppHandle,GWL_EXSTYLE);
  SetWindowLong(ghAppHandle,GWL_EXSTYLE,llStyle or WS_EX_TOOLWINDOW or not WS_EX_APPWINDOW);
end;


procedure showProgram;
var llStyle : LongInt;
begin

  llStyle:=GetWindowLong(ghAppHandle,GWL_EXSTYLE);
  SetWindowLong(ghAppHandle,GWL_EXSTYLE,llStyle or not WS_EX_TOOLWINDOW);
end;

procedure flyIn(poForm : TForm);
var loRectSmall : TRect;
    loWinRect   : TRect;
begin

  loWinRect:=poForm.BoundsRect;
  loRectSmall:=Types.Rect(0, 0, 0, 0);
  DrawAnimatedRects(GetDesktopWindow, IDANI_CAPTION, loRectSmall, loWinRect);
end;


procedure flyOut(poForm : TForm);
var loRectSmall : TRect;
    loWinRect   : TRect;
begin

  loWinRect:=poForm.BoundsRect;
  loRectSmall:=Types.Rect(0, 0, 0, 0);
  DrawAnimatedRects(GetDesktopWindow, IDANI_CAPTION, loWinRect, loRectSmall);
end;


function DetectWinVersion : TWinVersion;
var loOSVersionInfo : TOSVersionInfo;
begin

  Result:=wvUnknown; // ����������� ������ ��
  loOSVersionInfo.dwOSVersionInfoSize:=sizeof(TOSVersionInfo);

  {$ifdef __DELPHI__}
  if GetVersionEx(loOSVersionInfo)
  {$else}
  if GetVersionEx(@loOSVersionInfo)
  {$endif}

  then begin

    case loOSVersionInfo.DwMajorVersion of

      3: Result:=wvNT3; // Windows NT 3
      4: case loOSVersionInfo.DwMinorVersion of

        0: if loOSVersionInfo.dwPlatformId=VER_PLATFORM_WIN32_NT then
             Result:=wvNT4   // Windows NT 4
           else
             Result:=wv95;   // Windows 95

        10: Result:=wv98;    // Windows 98
        90: Result:=wvME;    // Windows ME
      end;
      5: case loOSVersionInfo.DwMinorVersion of

	    0: Result:=wvW2K;    // Windows 2000
	    1: Result:=wvXP;     // Windows XP
	    2: Result:=wv2003;   // Windows 2003
      end;
    end;
  end;
end;
{ifdef __DELPHI__}
procedure FlashForm(piCount : Integer = 3; pwFlags : DWord = 0);
{ifdef __DELPHI__}
var loRec : TFlashWInfo;
{else}
var loRec : Flash_Info;
{endif}
begin

  loRec.cbSize:=sizeof(loRec);
  loRec.hwnd:=ghAppHandle;
  loRec.dwFlags:=pwFlags;
  loRec.uCount:=piCount;
  loRec.dwTimeout:=100;
  FlashWindowEx(loRec);
end;
{else}
{procedure FlashForm(poForm : TForm);
begin
   Application.MainFormOnTaskbar := True;
   FlashWindow(GetParent(poForm.Handle),True);
end;}
{endif}
function CopyDesktopRectToForm(p_oForm : TForm; p_oRect : TRect) : Boolean;
var
  DeskTopDC: HDc;
  DeskTopCanvas: TCanvas;
  DeskTopRect: TRect;
begin
  Result:=True;
  DeskTopDC := GetWindowDC(GetDeskTopWindow);
  DeskTopCanvas := TCanvas.Create;
  DeskTopCanvas.Handle := DeskTopDC;
  DeskTopRect := p_oRect;
  p_oForm.Canvas.CopyRect(DeskTopRect, DeskTopCanvas, DeskTopRect);
  ReleaseDC(GetDeskTopWindow, DeskTopDC);
  FreeAndNil(DeskTopCanvas);
end;

function ScaleBitmap(p_oSrcBitmap : TBitMap;p_iWidth,p_iHeight : Integer) : TBitmap;
var l_oDstBmp : TBitmap;
begin
  l_oDstBmp:=TBitmap.Create;
  StretchBlt(l_oDstBmp.Canvas.Handle,0,0,p_iWidth,p_iHeight,
             p_oSrcBitmap.Canvas.Handle,0,0,p_oSrcBitmap.Width, p_oSrcBitmap.Height,SRCCOPY);
  Result:=l_oDstBmp;
end;
function GetClipText : String;
var l_pcText : PChar;
    l_sText : String;
begin
  l_sText:='';
  ClipBoard.Open;
  try
    ClipHandle := Clipboard.GetAsHandle(CF_TEXT);
    l_pcTextPtr := GlobalLock(ClipHandle);
    l_sText:=StrPas(l_pcText);
    GlobalUnlock(ClipHandle);
  finally
    Clipboard.Close;
  end;
  GetClipText:=p_sText;
end;

function FreeClipHandle : Boolean;
begin
  FreeClipHandle:=True;
  GlobalUnlock(ClipHandle);
end;
*)
{$endregion}

begin

  {$ifdef __DELPHI__}
  ghAppHandle:=Application.Handle;
  {$else}
  ghAppHandle:=TWin32WidgetSet(WidgetSet).AppHandle;
  {$endif}
end.
