unit tlcl;


interface


uses FPImage, Graphics, Classes, Sysutils, FPReadPNG, Intfgraphics, LCLType
  ,ExtCtrls, StdCtrls,DBGrids,sqldb
  ,tlzdb,tstr;


type TLookUpIdxRec = Record
       msName  : String;
       miID    : Integer;
     end;

     TLookUpIdxArray = array of TLookUpIdxRec;


procedure LoadPNGInBitmap(poBitMap : TBitMap; psFile : String; piWidth,piHeight : Integer);

procedure ComboLookupSetup(poCombo : TComboBox; var paoArray : TLookUpIdxArray; poQuery : TSQLQuery; psListField, psKeyField : String);

function ComboLookupGetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray) : Integer;

procedure ComboLookupSetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray; piKeyValue : Integer);

procedure ComboLookupDestroy(var paoArray : TLookUpIdxArray);

function  ComboLookupFill(poCombo     : TComboBox;
                         var paiArray    : TLookUpIdxArray;
                             poQuery     : TSQLQuery;
                             psSQL       : String;
                             psListField : String = 'fname';
                             psKeyField  : String = 'id') : Boolean;

procedure ComboLookupRefresh(poCombo : TComboBox;
                       var paoArray : TLookUpIdxArray;
                       poQuery : TSQLQuery;
                       psSQL : String;
                       piID : Integer);

procedure GridColumnsWidthReset(poGrid : TDBGrid ; piWidth : Integer);

implementation


procedure LoadPNGInBitmap(poBitMap : TBitMap; psFile : String; piWidth,piHeight : Integer);
var loStream  : TFileStream;
    //loImg     : TFPCustomImage;
    loReader  : TFPCustomImageReader;
    loIntfImg : TLazIntfImage;
    loImgHandle,
    loImgMaskHandle : HBitmap;
begin

  if FileExists(psFile) then begin

    try

      loReader:=TFPReaderPNG.Create;
      //loReader := GetFPImageReaderForFileExtension(ExtractFileExt(psFile)).Create;
      loStream:=TFileStream.Create(psFile, fmOpenRead);
      //loImg:=TFPCustomImage.Create(piWidth,piHeight);
      //loReader.ImageRead(loStream,loImg);
      //loImg.LoadFromStream(loStream,loReader);
      //poBitMap.Assign(CreateBitmapFromFPImage(loImg));

      loIntfImg := TLazIntfImage.Create(piWidth, piHeight);
      //loIntfImg.GetDescriptionFromDevice(0);
      //Reader := GetFPImageReaderForFileExtension(ExtractFileExt(FileName)).Create;
      loIntfImg.LoadFromFile(psFile,loReader);
      loIntfImg.CreateBitmaps(loImgHandle, loImgMaskHandle, false);
      poBitmap.Handle:=loImgHandle;
      poBitmap.MaskHandle:=loImgMaskHandle;

    finally

      //FreeAndNil(loImg);
      FreeAndNil(loIntfImg);
      FreeAndNil(loReader);
      FreeAndNil(loStream);
    end;
  end;

end;


procedure ComboLookupSetup(poCombo : TComboBox; var paoArray : TLookUpIdxArray; poQuery : TSQLQuery; psListField, psKeyField : String);
var liIdx : Integer;
begin

  poQuery.Last;
  if poQuery.RecordCount>0 then begin

    // EOutOfMemory exception.
    ///liIdx:=poQuery.RecordCount;
    SetLength(paoArray,poQuery.RecordCount);
    liIdx:=0;
    poCombo.Items.Clear;
    poQuery.First;
    while not poQuery.Eof do begin

      paoArray[liIdx].msName:=poQuery.FieldByName(psListField).AsString;
      paoArray[liIdx].miID:=poQuery.FieldByName(psKeyField).AsInteger;
      poCombo.Items.Add(paoArray[liIdx].msName);
      inc(liIdx);
      poQuery.Next;
    end;
    poCombo.ItemIndex:=0;
  end else begin

    poCombo.ItemIndex:=-1;
  end;
end;


function ComboLookupGetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray) : Integer;
begin

  Result:=-1;
  if poCombo.ItemIndex>=0 then begin

    Result:=paoArray[poCombo.ItemIndex].miID;
  end;
end;


procedure ComboLookupSetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray; piKeyValue : Integer);
var liIdx : Integer;
begin

  poCombo.ItemIndex:=-1;
  for lIIdx:=Low(paoArray) to High(paoArray) do begin

    if paoArray[liIdx].miID=piKeyValue then begin

      poCombo.ItemIndex:=liIdx;
      break;
    end;
  end;
end;


procedure ComboLookupDestroy(var paoArray : TLookUpIdxArray);
begin

  SetLength(paoArray,0);
end;


function  ComboLookupFill(poCombo     : TComboBox;
                         var paiArray    : TLookUpIdxArray;
                             poQuery     : TSQLQuery;
                             psSQL       : String;
                             psListField : String = 'fname';
                             psKeyField  : String = 'id') : Boolean;
begin

  Result:=True;
  initializeQuery(poQuery,psSQL);
  try

    poQuery.Open;
  except

    Result:=False;
  end;

  if Result then
    ComboLookupSetup(poCombo,paiArray,poQuery,psListField,psKeyField);
end;


procedure ComboLookupRefresh(poCombo : TComboBox;
                       var paoArray : TLookUpIdxArray;
                       poQuery : TSQLQuery;
                       psSQL : String;
                       piID : Integer);

var lsSelectedValue : String;
    liItemIdx : Integer;
begin

  if (piID<0) and (poCombo.ItemIndex>=0) then
    lsSelectedValue:=poCombo.Items[poCombo.ItemIndex]
  else
    lsSelectedValue:='';

  ComboLookupDestroy(paoArray);
  ComboLookupFill(poCombo,paoArray,poQuery,psSQL);
  if piID<0 then
  begin

    if not IsEmpty(lsSelectedValue) then
    begin

      liItemIdx:=poCombo.Items.IndexOf(lsSelectedValue);
      if liItemIdx>=0 then
        poCombo.ItemIndex:=liItemIdx
      else
        poCombo.ItemIndex:=0;
    end;
  end
  else
  begin

    ComboLookupSetKey(poCombo,paoArray,piID);
  end;
end;

procedure GridColumnsWidthReset(poGrid : TDBGrid; piWidth : Integer);
var liColumn : Integer;
    loColumn : TColumn;
begin

  for liColumn:=0 to Pred(poGrid.Columns.Count) do begin

    loColumn:=poGrid.Columns.Items[liColumn] as TColumn;
    if loColumn.FieldName<>'' then begin

      loColumn.Width:=piWidth;
    end;
  end;
end;

end.
