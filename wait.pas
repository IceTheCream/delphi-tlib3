// TLibrary ver 3.5 by Pakhomenkov. A. P. pakhomenkov.ap@yandex.ru 2011
// Wait 
unit wait;


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons;

  
type
  TfmWait = class(TForm)
 
    Animate: TAnimate;
    bbtBreak: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure bbtBreakClick(Sender: TObject);
  private

    m_blEnable : Boolean;
    { Private declarations }
  public
  
    function setEnable(p_blValue : Boolean) : Boolean;
    { Public declarations }
  end;

  
var
  fmWait: TfmWait;

  
function WaitShow : Boolean;
function WaitHide : Boolean;


implementation


{$R *.dfm}


procedure TfmWait.bbtBreakClick(Sender: TObject);
begin
  Raise Exception.CreateFmt('��������� �������� �� ���������� ������������.', ['']);
end;


procedure TfmWait.FormHide(Sender: TObject);
begin
  Animate.Active:=False;
end;


procedure TfmWait.FormShow(Sender: TObject);
begin
  Animate.Active:=True;
end;


function TfmWait.SetEnable(p_blValue : Boolean): Boolean;
begin
  m_blEnable:=p_blValue;
  Result:=m_blEnable;
end;


function WaitHide: Boolean;
begin

  if fmWait.m_blEnable then
    fmWait.Hide;
  Result:=fmWait.m_blEnable;
end;


function WaitShow: Boolean;
begin
  if fmWait.m_blEnable then
    fmWait.Show;
  Result:=fmWait.m_blEnable;
end;


end.
