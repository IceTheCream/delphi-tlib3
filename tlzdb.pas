unit tlzdb;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, db, tstr, tlib, tsys;

const ciSelectSQLIdx           = 1;
      ciInsertSQLIdx           = 2;
      ciUpdateSQLIdx           = 3;
      ciDeleteSQLIdx           = 4;
      ciCheckSQLIdx            = 5;
      csSQLTableInfo           =
        'select'+
        '       R.RDB$FIELD_NAME as afieldname,'+
        '       F.RDB$CHARACTER_LENGTH as afieldlength, '+
        '       F.RDB$FIELD_TYPE as afieldtype '+
        '  from RDB$FIELDS F, RDB$RELATION_FIELDS R'+
        '  where (F.RDB$FIELD_NAME = R.RDB$FIELD_SOURCE) and '+
        '        (R.RDB$SYSTEM_FLAG = 0) and'+
        '        (R.RDB$RELATION_NAME=:ptablename) '+
        '  order by R.RDB$FIELD_POSITION';

      ciFBStringType           = 37;
      ciEnteredStringIsCorrect =  0;
      ciEnteredStringIsEmpty   = -1;
      ciEnteredStringIsTooLong =  1;


type  TDBMode=(dmInsert,dmUpdate);
      TSQLArray = array[1..ciCheckSQLIdx] of String;

      TDBFieldAttr = record

        msName   : String;
        mdwHash  : DWord;
        miLength : Integer;
        moType   : Integer; //TFieldType;
      end;

      { TTableGuard }

      TTableGuard = class(TObject)
        private

          miFieldsCount : Integer;
          maoFields     : array of TDBFieldAttr;
          moQuery       : TSQLQuery;
          msTableName   : String;
        public

          constructor create(poQuery : TSQLQuery; psTableName : String);
          destructor  destroy(); override;
          function    openQuery() : Boolean;
          procedure   closeQuery();
          procedure   allocateMemory();
          procedure   freeMemory();
          procedure   loadFields();
          function    findField(psFieldName : String) : Integer;
          function    isStringValueFit(psFieldName : String; psValue : String) : Boolean;
          function    isStringValueCorrect(psFieldName,psValue : String) : Integer;
          function    getFieldLength(psFieldName : String) : Integer;
      end;


procedure initializeQuery(poQuery : TSQLQuery; psSQL : String);

procedure restartTransaction(poTransaction : TSQLTransaction);

procedure refreshQuery(poQuery: TSQLQuery; pblRestPos : Boolean = True);


implementation


procedure initializeQuery(poQuery : TSQLQuery; psSQL : String);
begin

  if poQuery.State<>dsInactive then
    poQuery.Close;
  restartTransaction(TSQLTransaction(poQuery.Transaction));
  poQuery.SQL.Clear;
  poQuery.SQL.Add(psSQL);
end;


procedure restartTransaction(poTransaction : TSQLTransaction);
begin

  poTransaction.EndTransaction;
  poTransaction.StartTransaction;
end;


procedure refreshQuery(poQuery: TSQLQuery;  pblRestPos : Boolean = True);
var liID : Integer;
begin


  with poQuery do begin

    if poQuery.State<>dsInactive then begin

      liID:=poQuery.FieldByName('id').AsInteger;  // Передавать параметром!!!

      Close;

    end else begin

      pblRestPos:=False;
    end;
    restartTransaction(TSQLTransaction(poQuery.Transaction));
    Open;
    Last;
    //***** Если нужно восстановить положение, то делаем
    if pblRestPos then begin

      poQuery.Locate('id',liId,[]);
    end else begin

        First;
    end;
  end;
end;

{ TTableGuard }

constructor TTableGuard.create(poQuery: TSQLQuery; psTableName : String);
begin

  moQuery:=poQuery;
  msTableName:=UpperCase(psTableName);
  if openQuery() then begin

    allocateMemory();
    loadFields();
  end
  else
  begin

    raise EDatabaseError.Create('Таблица '+msTableName+' не может быть открыта.');
  end;
end;


destructor TTableGuard.destroy;
begin

  closeQuery();
  freeMemory();
  inherited;
end;


function TTableGuard.openQuery: Boolean;
begin

  Result:=True;
  try

    initializeQuery(moQuery,csSQLTableInfo);
    moQuery.ParamByName('ptablename').AsString:=msTableName;
    moQuery.Open;
  except

    Result:=False;
  end;
end;


procedure TTableGuard.closeQuery;
begin

  moQuery.Close;
  TSQLTransaction(moQuery.Transaction).EndTransaction;
end;


procedure TTableGuard.allocateMemory;
begin

  moQuery.Last;
  miFieldsCount:=moQuery.RecordCount; // тут
  SetLength(maoFields,miFieldsCount);
end;


procedure TTableGuard.freeMemory;
begin

  SetLength(maoFields,0);
end;


procedure TTableGuard.loadFields;
var liFieldIdx : Integer;
    //i : integer;
begin

  liFieldIdx:=0;
  moQuery.First;
  while not moQuery.EOF do
  begin

    maoFields[liFieldIdx].msName:=upperTrimLine(moQuery.FieldByName('afieldname').AsString);
    maoFields[liFieldIdx].mdwHash:=calcStringHash(maoFields[liFieldIdx].msName);
    maoFields[liFieldIdx].moType:=moQuery.FieldByName('afieldtype').AsInteger;
    maoFields[liFieldIdx].miLength:=moQuery.FieldByName('afieldlength').AsInteger;
    moQuery.Next;
    inc(liFieldIdx);
  end;
end;


function TTableGuard.findField(psFieldName: String): Integer;
var ldwHash    : DWord;
    liFieldIdx : Integer;
    lsFieldName : String;
begin

  Result:=-1;
  lsFieldName:=upperTrimLine(psFieldName);
  ldwHash:=CalcStringHash(lsFieldName);
  for liFieldIdx:=0 to miFieldsCount-1 do
  begin

    if maoFields[liFieldIdx].mdwHash=ldwHash then
    begin

      if maoFields[liFieldIdx].msName=lsFieldName then
      begin

        Result:=liFieldIdx;
        break;
      end;
    end;
  end;
end;


function TTableGuard.isStringValueFit(psFieldName: String; psValue: String): Boolean;
var liFieldIdx : Integer;
begin

  Result:=False;
  liFieldIdx:=findField(psFieldName);
  if maoFields[liFieldIdx].moType<>ciFBStringType then
    raise EDatabaseError.Create('Поле '+psFieldName+' не является строковым. '+#13+'Проверка не удалась.');
  Result:=Length(psValue)<=(maoFields[liFieldIdx].miLength div 2); // UTF8 же!
end;


function TTableGuard.isStringValueCorrect(psFieldName,psValue : String): Integer;
begin

  Result:=ciEnteredStringIsCorrect;
  //***** Проверим, содержит ли что-то это значение
  if isEmpty(psValue) then
  begin

    Result:=ciEnteredStringIsEmpty;
  end
  else
  begin

    if not isStringValueFit(psFieldName,psValue) then
    begin

      Result:=ciEnteredStringIsTooLong;
    end;
  end;
end;


function TTableGuard.getFieldLength(psFieldName: String): Integer;
begin

  Result:=maoFields[findField(psFieldName)].miLength div 2;
end;


end.

