// TLibrary ver 3.5 by Pakhomenkov. A. P. pakhomenkov.ap@yandex.ru 2011
unit tvcl;


interface

{$i profile.inc}

uses Classes,Types,SysUtils,Controls,ComCtrls,StdCtrls,DateUtils,Menus,Forms,
     DB,DBGrids,
     {$ifdef __DELPHI__}
     ADODB,
     tdb,
     {$endif}

     Graphics,
     tcfg,

     tstr;


type TLookUpIdxRec = Record
       msName  : String;
       miID    : Integer;
     end;

     TLookUpIdxArray = array of TLookUpIdxRec;

{ifndef __DELPHI__}
const c_eSortAscendingStyle  = [fsBold];
      c_eSortDescendingStyle = [fsUnderline]; //fsStrikeOut
{endif}


{** @abstract(�������������� ����������� )
    @param(poBar ��������� ������������)
    @param(plCount ���������� �����)
    @param(plStep ������ ����)
    @returns(����������� �������)
    @code(n:=ProgressSetup(bar,10,1)); }
function  ProgressSetup(poBar : TProgressBar; plCount : Longint; plStep : Integer = 1) : Integer;

{** @abstract(���������� ��������� ������������ )
    @param(poBar ��������� ������������)
    @param(plNumber ����� ����)
    @code(ProgressNext(bar,6)); }
procedure ProgressNext(poBar : TProgressBar; plNumber : LongInt);

{** @abstract(��������� ��������������� ����������� )
    @param(poBar ��������� ������������)
    @param(plNumber ����� ����)
    @code(ProgressBiDirect(bar)); }
procedure ProgressBiDirect(poBar : TProgressBar);

{** @abstract(������������� �������� Enabled ��� ��������� ���������� � ��� �����)
    @param(poControl ���������)
    @param(pblEnable ������ ��������� �������� Enabled ���������� � ��� ����� )
    @code(ChildsControl(Button,False)); }
procedure ChildsControl(poControl : TWinControl; pblEnable : Boolean = True);

//procedure ComboYearAndMonthInit(poYearsCombo, poMonthCombo : TComboBox; ciMaxYear : Integer);
//procedure ComboYearAndMonthSet(poYearsCombo, poMonthCombo : TComboBox; pdtDate : TDateTime; piYearShift : Integer = 2000);
//function  ComboYearAndMonthGet(poYearsCombo, poMonthCombo : TComboBox; piYearShift : Integer = 2000) : TDateTime;

procedure ComboLookupSetup(poCombo : TComboBox; var paoArray : TLookUpIdxArray; poDataSet : TDataSet; psListField, psKeyField : String);
function  ComboLookupGetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray) : Integer;
procedure ComboLookupSetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray; piKeyValue : Integer);
procedure ComboLookupDestroy(var paoArray : TLookUpIdxArray);
{$ifdef __DELPHI__}
function  ComboLookupFillADO(poCombo     : TComboBox;
                         var paiArray    : TLookUpIdxArray;
                             poDataSet   : TDataSet;
                             psSQL       : String;
                             psListField : String = 'fname';
                             psKeyField  : String = 'id') : Boolean;

procedure ComboRefresh(poCombo : TComboBox; var paoArray : TLookUpIdxArray;
                       poDataSet : TDataSet; psSQL : String; piID : Integer = -1);
{$endif}

function  PopUpPullDown(poPopUp : TPopupMenu; poForm : TForm; poControl : TControl) : Boolean;
procedure PopUpFillGridColumns(poPopUp : TPopUpMenu; poGrid : TDBGrid; poOnClick: TNotifyEvent);


procedure GridResetAllTitles(poGrid : TDBGrid);
function  GridFindColumnByFieldName(poGrid : TDBGrid; psFieldName : String) : TColumn;
procedure GridShowSelectedColumnState(poGrid : TDBGrid; psOrderField, psOrderDirection : String);


//procedure ResetColumnMarks(p_oGrid: TDBGrid);
//function ActualizeColumnMarks(p_oGrid : TDBGrid; p_sOrderField : String;p_blAscending : Boolean = True) : Boolean;
//function ManageColumnSorting(p_oQuery : TADOQuery; p_oColumn : TColumn;p_oGrid : TDBGrid) : Boolean;


implementation


var uiProgBarCoef : Integer = 1;
    ublBarDirection : Boolean = True;


function ProgressSetup(poBar : TProgressBar; plCount : Longint; plStep : Integer = 1) : Integer;
begin

  Result:=0;
  if poBar<>Nil then
  begin

    poBar.Step:=plStep;
    poBar.Position:=0;
    poBar.Min:=0;

    //***** ������
    if plCount div 1000000 >0 then
     uiProgBarCoef:=100000
    else
      if plCount div 100000 >0 then
        uiProgBarCoef:=10000
      else
        if plCount div 10000 >0 then
         uiProgBarCoef:=1000
        else
          if plCount div 1000 >0 then
            uiProgBarCoef:=100
          else
            if plCount div 100 >0 then
              uiProgBarCoef:=10
            else
              if plCount div 10 >0 then
                uiProgBarCoef:=1;

    poBar.Max:=plCount div uiProgBarCoef;
    poBar.Step:=1;
    Result:=uiProgBarCoef;
  end;
end;


procedure ProgressNext(poBar : TProgressBar; plNumber : LongInt);
begin

  if (plNumber mod uiProgBarCoef=0) and (plNumber>0) then begin

    poBar.StepIt;
    Application.ProcessMessages;
  end;
end;


procedure ProgressBiDirect(poBar : TProgressBar);
begin

  if ublBarDirection then begin

    if poBar.Position<poBar.Max then begin

      poBar.StepIt;
    end else begin

      ublBarDirection:=not ublBarDirection;
      ProgressBiDirect(poBar);
    end;
  end else begin

    if poBar.Position>poBar.Min then begin

      poBar.Position:=poBar.Position-poBar.Step;
    end else begin

      ublBarDirection:=not ublBarDirection;
      ProgressBiDirect(poBar);
    end;
  end;
  Application.ProcessMessages;
end;


procedure ChildsControl(poControl : TWinControl; pblEnable : Boolean = True);
var liIdx : Integer;
begin

  for liIdx:=0 to Pred(poControl.ControlCount) do
    poControl.Controls[liIdx].Enabled:=pblEnable;
end;

(*
procedure ComboYearAndMonthInit(poYearsCombo, poMonthCombo : TComboBox; ciMaxYear : Integer);
var liIdx : Integer;
begin

  for liIdx:=YearOf(Now) to ciMaxYear do
    poYearsCombo.Items.Add(IntToStr(liIdx));
  for liIdx:=1 to 12 do
    poMonthCombo.Items.Add(fromSystemCodepage(casMonths[liIdx]));
end;

procedure ComboYearAndMonthSet(poYearsCombo, poMonthCombo : TComboBox; pdtDate : TDateTime; piYearShift : Integer = 2000);
//var i : integer;
begin
  //i:=(YearOf(pdtDate)-piYearShift);
  poYearsCombo.ItemIndex:=(YearOf(pdtDate)-piYearShift);
  poMonthCombo.ItemIndex:=Pred(MonthOf(pdtDate));
end;


function ComboYearAndMonthGet(poYearsCombo, poMonthCombo : TComboBox; piYearShift : Integer = 2000) : TDateTime;
begin

  Result:=EncodeDate(poYearsCombo.ItemIndex+piYearShift,Succ(poMonthCombo.ItemIndex),1);
end;
*)

procedure ComboLookupSetup(poCombo : TComboBox; var paoArray : TLookUpIdxArray; poDataSet : TDataSet; psListField, psKeyField : String);
var liIdx : Integer;
begin

  if poDataSet.RecordCount>0 then begin

    // EOutOfMemory exception.
    SetLength(paoArray,poDataSet.RecordCount);
    liIdx:=0;
    poCombo.Items.Clear;
    poDataSet.First;
    while not poDataSet.Eof do begin

      paoArray[liIdx].msName:=poDataSet.FieldByName(psListField).AsString;
      paoArray[liIdx].miID:=poDataSet.FieldByName(psKeyField).AsInteger;
      poCombo.Items.Add(paoArray[liIdx].msName);
      inc(liIdx);
      poDataSet.Next;
    end;
    poCombo.ItemIndex:=0;
  end else begin

    poCombo.ItemIndex:=-1;
  end;
end;


function ComboLookupGetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray) : Integer;
begin

  Result:=-1;
  if poCombo.ItemIndex>=0 then begin

    Result:=paoArray[poCombo.ItemIndex].miID;
  end;
end;


procedure ComboLookupSetKey(poCombo : TComboBox; var paoArray : TLookUpIdxArray; piKeyValue : Integer);
var liIdx : Integer;
begin

  poCombo.ItemIndex:=-1;
  for lIIdx:=Low(paoArray) to High(paoArray) do begin

    if paoArray[liIdx].miID=piKeyValue then begin

      poCombo.ItemIndex:=liIdx;
      break;
    end;
  end;
end;


procedure ComboLookupDestroy(var paoArray : TLookUpIdxArray);
begin

  SetLength(paoArray,0);
end;

{$ifdef __DELPHI__}
function  ComboLookupFillADO(poCombo     : TComboBox;
                         var paiArray    : TLookUpIdxArray;
                             poDataSet   : TDataSet;
                             psSQL       : String;
                             psListField : String = 'fname';
                             psKeyField  : String = 'id') : Boolean;
begin

  Result:=True;
  ADOQueryInitialize(TADOQuery(poDataSet),psSQL);
  try

    poDataSet.Open;
  except
    Result:=False;
  end;

  if Result then
    ComboLookupSetup(poCombo,paiArray,poDataSet,psListField,psKeyField);
end;

procedure ComboRefresh(poCombo : TComboBox; var paoArray : TLookUpIdxArray;
                       poDataSet : TDataSet; psSQL : String; piID : Integer);
var lsSelectedValue : String;
    liItemIdx : Integer;
begin

  if (piID<0) and (poCombo.ItemIndex>=0) then
    lsSelectedValue:=poCombo.Items[poCombo.ItemIndex]
  else
    lsSelectedValue:='';

  ComboLookupDestroy(paoArray);
  ComboLookupFillADO(poCombo,paoArray,poDataSet,psSQL);
  if piID<0 then
  begin

    if not IsEmpty(lsSelectedValue) then
    begin

      liItemIdx:=poCombo.Items.IndexOf(lsSelectedValue);
      if liItemIdx>=0 then
        poCombo.ItemIndex:=liItemIdx
      else
        poCombo.ItemIndex:=0;
    end;

  end
  else
  begin

    ComboLookupSetKey(poCombo,paoArray,piID);
  end;

end;
{$endif}

function PopUpPullDown(poPopUp : TPopupMenu; poForm : TForm; poControl : TControl) : Boolean;
var loSrc,
    loDst : TPoint;
begin

  Result:=True;
  loSrc.X:=poControl.Left;
  loSrc.Y:=poControl.Top+poControl.Height+1;
  loDst:=poForm.ClientToScreen(loSrc);
  poPopUp.PopUp(loDst.X,loDst.Y);
end;


procedure PopUpFillGridColumns(poPopUp : TPopUpMenu; poGrid : TDBGrid; poOnClick: TNotifyEvent);
var liIdx  : Integer;
    loItem : TMenuItem;
begin

  poPopUp.Items.Clear;
  for liIdx:=0 to Pred(poGrid.Columns.Count) do
  begin

    loItem:=TMenuItem.Create(poPopUp);
    loItem.Tag:=liIdx;
    loItem.Caption:=poGrid.Columns.Items[liIdx].Title.Caption;
    loItem.Checked:=poGrid.Columns.Items[liIdx].Visible;
    loItem.OnClick:=poOnClick;
    poPopUp.Items.Add(loItem);
  end;
end;


procedure GridResetAllTitles(poGrid : TDBGrid);
var liIdx : Integer;
begin

  for liIdx:=0 to Pred(poGrid.Columns.Count) do
  begin

    poGrid.Columns.Items[liIdx].Title.Font.Style :=
      poGrid.Columns.Items[liIdx].Title.Font.Style - [fsBold,fsUnderLine];
  end;
end;


function GridFindColumnByFieldName(poGrid : TDBGrid; psFieldName : String) : TColumn;
var liIdx : Integer;
begin

  Result:=Nil;
  for liIdx:=0 to Pred(poGrid.Columns.Count) do
  begin

    if AnsiUpperCase(poGrid.Columns[liIdx].FieldName)=AnsiUpperCase(psFieldName) then
    begin

      Result:=poGrid.Columns[liIdx];
      break;
    end;
  end;
end;


procedure GridShowSelectedColumnState(poGrid : TDBGrid; psOrderField, psOrderDirection : String);
var poColumn : TColumn;
begin

  //***** ������ �������
  poColumn:=GridFindColumnByFieldName(poGrid,psOrderField);

  //***** ������� ����� ���������
  poColumn.Title.Font.Style:=poColumn.Title.Font.Style -
    (c_eSortAscendingStyle+c_eSortDescendingStyle);

  //***** ��� ��� ���������� �� ����� ����, ��������������� ����������� - ���������� ����
  poColumn.Title.Font.Style:=poColumn.Title.Font.Style+c_eSortAscendingStyle;

  //***** ���� ���������� � �������� ������� - ���������� �������������
  if psOrderDirection='DESC' then
    poColumn.Title.Font.Style:=poColumn.Title.Font.Style+c_eSortDescendingStyle;
end;


end.
