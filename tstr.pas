// @abstract ������ ��� ������ �� ��������
// @author(���������� �.�. pakhomenkov@gmail.com)

unit tstr;

{$X+}

interface

{$I profile.inc}

uses SysUtils, Classes, DateUtils, Forms //, StrUtils


    {$ifdef __WINDOWS__}
    ,windows
    {$endif}

    {$ifdef __DELPHI__}
    ,Types, StrUtils
    {$endif}

    {$ifdef __LAZARUS__}
    ,lazutf8
    {$endif}
    ,tmath
    ;


{$i tconst.inc}

type
     {$ifdef __LAZARUS__}
     uString = WideString;
     {$else}
     uString = String;
     {$endif}

{** @abstract(����������� ������ �� UTF-8 � ���������, ���� ������������ Lazarus. � Delphi �� ������ ������)
    @param(psLine ������ ��� �������������)
    @returns(���������������� ������)
    @code(s:=toSystemCodepage(strs)); }
function toSystemCodepage(psLine : String) : String;

{** @abstract(����������� ������ �� ��������� � UTF-8, ���� ������������ Lazarus. � Delphi �� ������ ������)
    @param(psLine ������ ��� �������������)
    @returns(���������������� ������)
    @code(s:=FromSystemCodepage(strs)); }
function fromSystemCodepage(psLine : String) : String;

{** @abstract(������������� ������ ������� Length (Delphi) � UTF8Length(Lazarus))
    @param(S ��������� ����������, ������������ ��� ����������� � �����)
    @returns(����� ������)
    @code(n:=uLength(S)); }
function uLength(S : String): Integer;

{** @abstract(������������� ������ ������� Delete (Delphi) � UTF8Delete(Lazarus))
    @param(S ��������� ����������, �������� �������� ������)
    @param(Index ������ ������� � ������, � �������� ���������� ��������� ���������)
    @param(Size ������ ��������� ��������� )
    @code(uDelete(S,5,2)); }
procedure uDelete (Var S : String; Index,Size: LongInt);

{** @abstract(������������� ������ ������� Copy (Delphi) � UTF8Copy(Lazarus))
    @param(S ��������� ����������, �������� �������� ������)
    @param(Index ������ ������� � ������, � �������� ���������� ���������� ���������)
    @param(Size ������ ���������� ��������� )
    @returns(���������� ���������)
    @code(ss:=uCopy(S,7,12)); }
function uCopy(S : String; Index: Integer; Count: Integer): string;

{** @abstract(������������� ������ ������� Pos (Delphi) � UTF8Pos(Lazarus))
    @param(SearchForText ������� ���������)
    @param(SearchInText ������, � ������� ������������ ����� ���������)
    @param(StartPos ��������� ������� ������, �� ��������� 1)
    @returns(0, ���� ��������� �� �������, ����� ������ ��������� � ������)
    @code(idx:=uPos('415','3,1415');) }
function uPos(const SearchForText, SearchInText : string; StartPos: LongInt = 1): LongInt;

{** @abstract(������������� ������ ������� Trim (Delphi) � UTF8Trim(Lazarus))
    @param(S �������� ������)
    @returns(�������� ������ ��� �������� � ������ � � �����)
    @code(s:=uTrim('  Let it be!   ');) }
function uTrim(const s: string): string;

{** @abstract(������� ��������� � ����� ������ ����)
    @param(psLine ��������� ����������, ���������� ����. ���������� �� ������.)
    @param(pcSlash - ����. ��� ����� - \ ��� ����� - / � ����������� �� ������������ ������� )
    @returns(������ �� ������ � �����. ��� �� ������������ � ���������� psLine)
    @code(slashIt(S)); }
function slashIt(var psLine : String; pcSlash : Char = ccSlashChar) : String;

{** @abstract(������� �������� �� �������� ������ ��������� � ��������� �����������)
    @param(psLine �������� ������, ���������� �� ������ (var))
    @param(piFrom ������ ������� � ������, � �������� ���������� ���������� ���������)
    @param(piHowMany ������ ���������� ��������� )
    @returns(���������� ���������)
    @code(ss:=cut(S,3,5)); }
function cut(var psLine : String; piFrom : Integer; piHowMany : Integer=-1) : String;

{** @abstract(������� ����������� �������� ������� � �����)
    @param(psLine �������� ������)
    @param(piCount ���������� ��������� ��������, �� ��������� 1)
    @returns(����������� ������)
    @code(ss:=chopTail(S)); }
function chopTail(psLine : String; piCount : Integer = 1) : String;

{** @abstract(������� ���������, �������� �� ������ ������. ������ ��������� ������,
              ���� ���� ��� �������� �������)
    @param(psLine ��������� ����������)
    @returns(@true, ���� ������ �����, ����� @false)
    @code(if isEmpty(S) then ...) }
function isEmpty(psLine : String) : Boolean;

{** @abstract(������������� ������� ������������ ������.)
    @param(psLine ������ ��� ������������)
    @param(piWidth ������ ������ ������)
    @param(piAlign ���� ������������ - ciAlLeft, ciAlRight,ciAlCenter)
    @param(pcFill ������ ����������, �� ��������� ������)
    @returns(����������� ������)
    @code(s:=alignFill('0,0',6,ciAlRight,'0');) }
function alignFill(psLine : String; piWidth : Integer; piAlign : Integer = ciAlLeft; pcFill : Char=#32) : String;

{** @abstract(������� ������������ ������ ������)
    @param(psLine ������ ��� ������������)
    @param(piWidth ������ ������ ������)
    @param(pcFill ������ ����������, �� ��������� ������)
    @returns(����������� ������)
    @code(s:=alignRight('283,61',6);) }
function alignRight(psLine : String; piWidth : Integer; pcFill : Char = #32) : String;

{** @abstract(������� ������������ ������ �����)
    @param(psLine ������ ��� ������������)
    @param(piWidth ������ ������ ������)
    @param(pcFill ������ ����������, �� ��������� ������)
    @returns(����������� ������)
    @code(s:=alignLeft('946,52',8,'0');) }
function alignLeft(psLine : String; piWidth : Integer; pcFill : Char = #32) : String;

{** @abstract(������� ������������ ������ �� ������)
    @param(psLine ������ ��� ������������)
    @param(piWidth ������ ������ ������)
    @param(pcFill ������ ����������, �� ��������� ������)
    @returns(����������� ������)
    @code(s:=alignCenter('���-��-��',15);) }
function alignCenter(psLine : String; piWidth : Integer; pcFill : Char = #32) : String;

{** @abstract(������� ������� ������� ������ ���������� � ����� �����)
    @param(psFileName ��� �����)
    @returns(������� ����� ����� �����������)
    @code(n:=FindExtension('index.html.tar.gz');) }
function findExtension(psFileName : String) : Integer;

{** @abstract(������� ���������� ��� ����� � ����� ��� ����������)
    @param(psFileName ������ ��� �����)
    @returns(��� ����� ��� ����������)
    @code(filename:=throwAwayExtension('c:\windows\system32\shells32.dll')) }
function throwAwayExtension(psFileName : String) : String;

{** @abstract(������� �������� ���������� ����� ����� �� ��������)
    @param(psFileName ������ ��� �����)
    @param(psNewExt ����� ����������)
    @returns(��� ����� � ����� �����������)
    @code(filename:=changeExtension('c:\windows\system32\shells32.dll','.exe')) }
function changeExtension(psFileName : String; psNewExt : String = '.7z' ) : String;

{** @abstract(������� ����������, �������� �� ���������� ������ ������)
    @param(pcChar ������ ��� ��������)
    @returns(@true, ���� ������ �������� ������, ����� @false)
    @code(if isDigit('7') then ...) }
function isDigit(pcChar : Char) : Boolean;

{** @abstract(������� ����������, �������� �� ������ ������������� ��������)
    @param(psText ������ ��� ��������)
    @returns(@true, ���� ������ �������� ������������� ��������, ����� @false)
    @code(if isInteger('123') then ...) }
function isInteger(psText : String) : Boolean;

{** @abstract(������� ���������, �������� �� ������ �������� ���� Float)
    @param(psText    - ������ ��� ��������)
    @returns(@true, ���� ������ �������� �������� ���� Float, ����� @false)
    @code(if isFloat('123,45') then ...) }
function IsFloat(psText : String) : Boolean;

{** @abstract(������� ����������, ����������� �� ���������� ������ ��������� � ��������� ANSI)
    @param(pcChar ������ ��� ��������)
    @returns(@true, ���� ������ ����������� ���������, ����� @false)
    @code(if isRussianANSIChar('�') then ...) }
function isRussianANSIChar(pcChar : Char) : Boolean;

{** @abstract(������� ����������, ����������� �� ���������� ������ ��������)
    @param(pcChar ������ ��� ��������)
    @returns(@true, ���� ������ ����������� ��������, ����� @false)
    @code(if isLatinChar('�') then ...) }
function isLatinChar(pcChar : Char) : Boolean;

{** @abstract(������� ������� �������� ������ � ������ � �������� ��� ������)
    @param(psLine c����� ��� ������)
    @param(pcSrc ������� ������)
    @param(pcTarget ������ ��� ������)
    @returns(���������� ������)
    @code(s:=findAndReplaceOneChar('173.32','.',',');) }
function findAndReplaceOneChar(psLine : String; pcSource,pcTarget : Char) : String;

{** @abstract(������� ������� ��� ��������� ��������� ������� �� ������)
    @param(psLine c����� ��� ������)
    @param(pcChar ������ ��� ��������)
    @returns(���������� ������)
    @code(s:=findAndDeleteAllEntries('3,1415','1');) }
function findAndDeleteAllEntries(psLine : String; pcChar : Char) : String;

{** @abstract(������� ����������� ����� ���� Real � ������ � ����������� ��������)
    @param(prSum ����� ��� ��������������)
    @returns(������, ���������� ����� � ������������ ���������)
    @code(s:=separateMils(31415.00); //31 415,00) }
function separateMils(prSum : Real) : String;

{** @abstract(������� ������������ ����������� ����� ������)
    @param(psLine �������� ������)
    @returns(����������� ����� ������)
    @code(s:=hashString('�� ������, ���������� �������!')); }
function hashString(psLine : String) : Longint;

{** @abstract(������� ������������ ����� �� ����������� ���� � �������������)
    @param(pcChar �������� �����)
    @returns(����� �� 0 �� 9, ���� ������� ������ �����, -1 �����)
    @code(digit:=charDigitToInt('7')); }
function charDigitToInt(pcChar : Char) : Integer;

{** @abstract(������� ���������� ����� ��������  )
    @param(pdValue �������� �����)
    @returns(����� ��������)
    @code(sum:=writeOutSum(2147483647)); }
function writeOutSum(pdValue : Double) : String;

{** @abstract(������� ���������� ������� ���� � ����� � ������)
    @param(pdValue �������� �����)
    @returns(������ � ������� ������_����)
    @code(s:=dateTimeToFileName(Now,True)); }
function dateTimeToFileName(pdtDate : TDateTime; pblTime : Boolean=True) : String;

{** @abstract(������� ���������� ����� ������ � ������� ����)
    @param(psLine �������� ������)
    @param(piCount ���������� ���������� ��������)
    @returns(���� ����� ������ ������ piCount - ����� ������ ������ piCount ����� �������� ������)
    @code(s:=copyFromRight('������ �������� ������ ���������',16)); }
function copyFromRight(psLine : String; piCount : Integer) : String;

{** @abstract(������� ������� �������� ������� ������ � �������� �� � �������� ��������)
    @param(psLine �������� ������)
    @returns(������ ��� �������� ��������, ����������� � �������� ��������)
    @code(s:=upperTrimLine('   ��� ����� ���... ����������.    ')); }
function upperTrimLine(psLine : String) : String;

{** @abstract(������� ���������� ������ ������������������)
    @param(psFirst ������ ������)
    @param(psSecond ������ ������)
    @returns(@true, ���� ������ ���������, ����� @false)
    @code(if isEqualString('���',' ���  ')) then ... }
function isEqualString(psFirst,psSecond : String) : Boolean;


implementation

//uses tlib;

function toSystemCodepage(psLine : String) : String;
begin

  {$ifdef __DELPHI__}
  Result:=psLine;
  {$else}
  Result:=UTF8ToSys(psLine);
  {$endif}
end;


function fromSystemCodepage(psLine : String) : String;
begin

  {$ifdef __DELPHI__}
  Result:=psLine;
  {$else}
  Result:=SysToUTF8(psLine);
  {$endif}
end;


function uLength(S : String): Integer;
begin

  {$ifdef __LAZARUS__}
  Result:=UTF8Length(S);
  {$else}
  Result:=Length(S);
  {$endif}
end;


function uCopy(S : String; Index: Integer; Count: Integer): string;
begin

  {$ifdef __LAZARUS__}
  Result:=UTF8Copy(S,Index,Count);
  {$else}
  Result:=Copy(S,Index,Count);
  {$endif}
end;


procedure uDelete (Var S : String; Index,Size: LongInt);
begin

  {$ifdef __LAZARUS__}
  UTF8Delete(S,Index,Size);
  {$else}
  Delete(S,Index,Size);
  {$endif}
end;


function uPos(const SearchForText, SearchInText: string; StartPos: LongInt = 1): LongInt;
begin

  {$ifdef __LAZARUS__}
  Result:=UTF8Pos(SearchForText,SearchInText,StartPos);
  {$else}
  Result:=PosEx(SearchForText,SearchInText,StartPos);
  {$endif}
end;


function uTrim(const s: string): string;
begin

  {$ifdef __LAZARUS__}
  Result:=UTF8Trim(S);
  {$else}
  Result:=Trim(S);
  {$endif}
end;


function slashIt(var psLine : String; pcSlash : Char = ccSlashChar) : String;
begin

  if not IsEmpty(psLine) then begin

    if psLine[Length(psLine)]<>pcSlash then
      psLine:=psLine+pcSlash;

  end;
  Result:=psLine;
end;


function cut(var psLine : String; piFrom : Integer; piHowMany : Integer=-1) : String;
var liLen : Integer;
begin

  liLen:=uLength(psLine);
  if (piFrom<1) or (piFrom>liLen) then begin
    piFrom:=1;
  end;

  if (piHowMany<1) or (piHowMany+piFrom>liLen) then begin
  
    piHowMany:=Succ(liLen-piFrom);
  end;

  if piHowMany>0 then begin
    Result:=uCopy(psLine,piFrom,piHowMany);
    uDelete(psLine,piFrom,piHowMany);
  end;
end;


function chopTail(psLine : String; piCount : Integer = 1) : String;
var lsLine : String;
begin

  lsLine:=psLine;
  if piCount>=1 then begin

    if (piCount>=uLength(psLine)) then begin

      lsLine:='';
    end else begin

      uDelete(lsLine,uLength(lsLine)-Pred(piCount),piCount);
    end;
  end;
  Result:=lsLine;
end;


function isEmpty(psLine : String) : Boolean;
begin

  Result:=uLength(uTrim(psLine))=0;
end;


function alignFill(psLine : String; piWidth : Integer; piAlign : Integer = ciAlLeft; pcFill : Char = ' ') : String;
var lsLine  : String;
    liWdt,
    liLen,
    liHalf  : Integer;
begin

  lsLine:=uTrim(psLine);
  liLen:=uLength(lsLine);
  if liLen>0 then begin

    if liLen<piWidth then begin

      liWdt:=piWidth-liLen;
      if piAlign=ciAlCenter then begin
	  
        liHalf:=liWdt div 2;
        lsLine:=StringOfChar(pcFill,liHalf)+lsLine+StringOfChar(pcFill,liWdt-liHalf);
      end else begin
	  
        if piAlign=ciAlLeft then begin
		
          lsLine:=lsLine+StringOfChar(pcFill,liWdt);
        end else begin
		
          if piAlign=ciAlRight then begin
		  
            lsLine:=StringOfChar(pcFill,liWdt)+lsLine;
          end;
        end
      end;
    end;
  end else begin
  
    lsLine:=StringOfChar(#32,piWidth);
  end;
  Result:=lsLine;
end;


function alignRight(psLine : String; piWidth : Integer; pcFill : Char = #32) : String;
begin

  Result:=AlignFill(psLine,piWidth,ciAlRight,pcFill);
end;


function alignLeft(psLine : String; piWidth : Integer; pcFill : Char = #32) : String;
begin

  Result:=AlignFill(psLine,piWidth,ciAlLeft,pcFill);
end;


function alignCenter(psLine : String; piWidth : Integer; pcFill : Char = #32) : String;
begin

  Result:=AlignFill(psLine,piWidth,ciAlCenter,pcFill);
end;


function findExtension(psFileName : String) : Integer;
var liPos,
    liPrevPos : Integer;
begin

  liPrevPos:=0;
  liPos:=uPos('.',psFileName);
  while liPos>0 do begin
  
    liPrevPos:=liPos;
    liPos:=uPos('.',psFileName,Succ(liPos));
  end;
  Result:=liPrevPos;
end;


function throwAwayExtension(psFileName : String) : String;
begin

  Result:=uCopy(psFileName,1,Pred(FindExtension(psFileName)));
end;


function changeExtension(psFileName : String; psNewExt : String = '.7z' ) : String;
begin

  Result:=extractFilePath(psFileName)+
      throwAwayExtension(ExtractFileName(psFileName))+
      psNewExt;
end;


function isDigit(pcChar : Char) : Boolean;
begin

  Result:=pcChar in ['0','1','2','3','4','5','6','7','8','9'];
end;


function isInteger(psText : String) : Boolean;
var liIdx,
    liLen  : Integer;
    lsLine : String;
begin

  lsLine:=uTrim(psText);
  liLen:=uLength(lsLine);
  if liLen>0 then begin
    Result:=True;
    for liIdx:=1 to liLen do begin

      if not isDigit(lsLine[liIdx]) then begin

        Result:=False;
        break;
      end;
    end;
  end else begin

    Result:=False;
  end;
end;

function isFloat(psText : String) : Boolean;
var liIdx,
    liLen  : Integer;
    lsLine : String;
begin

  Result:=False;
  lsLine:=psText;
  liLen:=uLength(lsLine);
  if liLen>0 then begin
    Result:=True;
    for liIdx:=1 to liLen do begin

      if not (lsLine[liIdx] in ['0','1','2','3','4','5','6','7','8','9',',']) then begin

	    Result:=False;
        break;
      end;
    end;
  end;

  // ���� � ������ ������ ����� ������� - ��� �� Float;
  if Result then begin

    liIdx:=uPos(',',lsLine);
    if liIdx>0 then
      uDelete(lsLine,liIdx,1);
    Result:=Pos(',',lsLine)=0;
  end;
end;


function isRussianANSIChar(pcChar : Char) : Boolean;
begin

  Result:=pcChar in cstANSIRussian;
end;


function isLatinChar(pcChar : Char) : Boolean;
begin

  IsLatinChar:=pcChar in cstLatin;
end;


function findAndReplaceOneChar(psLine : String; pcSource, pcTarget : Char) : String;
var liPos : Integer;
begin

  Result:='';
  if not IsEmpty(psLine) then begin
  
    liPos:=uPos(pcSource,psLine);
    if liPos>0 then begin

      psLine[liPos]:=pcTarget;
    end;
    Result:=psLine;
  end;
end; 


function findAndDeleteAllEntries(psLine : String; pcChar : Char) : String;
var liPos  : Integer;
    lsLine : String;
begin

  lsLine:=psLine;
  liPos:=uPos(pcChar,lsLine);
  While liPos>0 do begin
  
    uDelete(lsline,liPos,1);
    liPos:=uPos(pcChar,lsLine);
  end;
  Result:=lsLine;
end;


function separateMils(prSum : Real) : String;
var lsResult : String;
    liIdx    : Integer;
begin

  lsResult:=Format('%11.2f',[prSum]);
  liIdx:=uLength(lsResult)-3; // ������ ��� ������ ����� � ����� ������� ����� ���
  while liIdx>3 do begin

    Dec(liIdx,3);
    Insert(' ',lsResult,Succ(liIdx));
  end;
  Result:=uTrim(lsResult);
end;


function hashString(psLine : String) : Longint;
var liIdx : Integer;
begin

  Result:=0;
  for liIdx:=1 to uLength(psLine) do begin
  
    Result:=Result+Ord(psLine[liIdx]);
  end;
end;


function charDigitToInt(pcChar : Char) : Integer;
begin

  if pcChar in ['0'..'9'] then
    Result:=Ord(pcChar)-ciZeroCode
  else
    Result:=-1;
end;


function WriteOutSum(pdValue : Double) : String;
const ciGrpRubles    = 1;
      ciGrpTousends  = 2;
      ciGrpMillions  = 3;
      ciGrpMilliards = 4;

var lsSum,
    lsRoubles,
    lsCopecks,
    lsLine     : String;
    lasGroups  : array[1..5] of String;
    liPoint,
    liLength,
    liGroupLen,
    liGroup,
    liForm     : Integer;
    lblDone    : Boolean;
    lcOne,
    lcTen,
    lcHundred  : Char;
    lrValue    : Double;
begin

  lsLine:='';
  liForm:=ciFormOne;
  //lsSum:=FloatToStr(RoundEx(pdValue),10,2);

  //***** ����������� ����� � ������
  lrValue:=RoundEx(pdValue);
  Str(lrValue:10:2,lsSum);

  lsSum:=uTrim(lsSum);
  //***** ���������� ������ � ������������� ������
  if lsSum[1]='-' then begin
  
    uDelete(lsSum,1,1);
    lsLine:=lsLine+'����� ';
  end;

  //***** �������������� ������ �����.
  FillChar({%H-}lasGroups,sizeof(lasGroups),0);

  //***** �������� ����� � �������
  liPoint:=uPos(ccRoublesDelim,lsSum);
  if liPoint>0 then begin
  
    lsRoubles:=Cut(lsSum,1,Pred(liPoint));
    uDelete(lsSum,1,1);
    lsCopecks:=lsSum;
  end else begin
  
    lsCopecks:='0';
  end;

  //***** �������� ����� �� ������ �� ��� ����� � ������ 
  liGroup:=1;
  lblDone:=False;
  while not lblDone do begin

    liLength:=uLength(lsRoubles);
    if liLength<=3 then begin //& <4

      //***** ������ ������ ������ 
      lasGroups[liGroup]:=lsRoubles;
      lblDone:=True;
    end else begin

      //***** ������ ������ ������
      lasGroups[liGroup]:=Cut(lsRoubles,liLength-2,3);
    end;
    Inc(liGroup);
  end;

  //***** ������ ������ ��������� � ��������� ��� � ����������� ��������� 
  for liGroup:=High(lasGroups) downto Low(lasGroups) do begin

    liGroupLen:=uLength(lasGroups[liGroup]);

    //***** �������� ������ ������ �� ��������� �����
    if liGroupLen>0 then begin
	
      lcOne:='&'; // �����
      lcTen:='&'; // �������
      lcHundred:='&'; // �����
      case liGroupLen of
	  
        1: begin
		
          lcOne:=lasGroups[liGroup,1];
        end;
        2: begin
		
          lcTen:=lasGroups[liGroup,1];
          lcOne:=lasGroups[liGroup,2];
        end;
        3: begin
		
          lcHundred:=lasGroups[liGroup,1];
          lcTen:=lasGroups[liGroup,2];
          lcOne:=lasGroups[liGroup,3];
        end;
      end;

      //***** ���� ���� �����.. 
      if lcHundred in ['0'..'9'] then
      begin

        lsLine:=lsLine+casHundreds[CharDigitToInt(lcHundred)];
      end;

      //***** ���� ���� �������.. 
      if lcTen in ['0'..'9'] then
      begin

        lsLine:=lsLine+casTens[CharDigitToInt(lcTen)];
        if (lcTen='1') and (lcOne in ['0'..'9']) then
        begin

          lsLine:=lsLine+casTeeens[CharDigitToInt(lcOne)];
          lcOne:='&';
          liForm:=ciFormFive;
        end;
      end;

      if (liGroup=ciGrpMilliards) or
         (liGroup=ciGrpMillions) or
         (liGroup=ciGrpRubles) then begin
        if lcOne in ['0'..'9'] then
        begin

          lsLine:=lsLine+casOnesM[CharDigitToInt(lcOne)];
          liForm:=caiNumberForms[CharDigitToInt(lcOne)];
        end;
      end else begin
	  
        if liGroup=ciGrpTousends then begin

          if lcOne in ['0'..'9'] then
          begin

            lsLine:=lsLine+casOnesM[CharDigitToInt(lcOne)];
            liForm:=caiNumberForms[CharDigitToInt(lcOne)];
          end;
        end;
      end;

      //***** � ������ ����������� � ���������� 
      if liGroup=ciGrpRubles then begin

        lsLine:=lsLine+casOnesRoubles[liForm];
      end;
      if liGroup=ciGrpTousends then begin

        lsLine:=lsLine+casThousandsRoubles[liForm];
      end;
      if liGroup=ciGrpMillions then begin

        lsLine:=lsLine+casBillionsRoubles[liForm];
      end;
      if liGroup=ciGrpMilliards then begin

        lsLine:=lsLine+casMilliardsRoubles[liForm];
      end;
    end;
  end;

  //***** �������� ���������
  lcTen:=lsCopecks[1];
  lcOne:=lsCopecks[2];

  lsLine:=lsLine+casTensCopecks[CharDigitToInt(lcTen)];
  if (lcTen='1') and (lcOne in ['0'..'9']) then
  begin

    lsLine:=lsLine+casTeeens[CharDigitToInt(lcOne)];
    lcOne:='&';
  end;

  if lcOne in ['0'..'9'] then
  begin

    lsLine:=lsLine+casOnesW[CharDigitToInt(lcOne)];
    liForm:=caiNumberForms[CharDigitToInt(lcOne)];
  end;
  lsLine:=lsLine+casOnesCopecks[liForm];
  Result:=fromSystemCodepage(lsLine);
end;


function locWordToStrZero(pwDate : Word) : String;
begin

  if pwDate<10 then
    Result:='0'+IntToStr(pwDate)
  else
    Result:=IntToStr(pwDate);
end;


function dateTimeToFileName(pdtDate : TDateTime; pblTime : Boolean) : String;
begin

  Result:=
    locWordToStrZero(YearOf(pdtDate)-2000)+
    locWordToStrZero(MonthOf(pdtDate))+
    locWordToStrZero(DayOf(pdtDate));
  if pblTime then begin

    Result:=Result+'_'+
    locWordToStrZero(HourOf(pdtDate))+
    locWordToStrZero(MinuteOf(pdtDate));
  end;
end;


function copyFromRight(psLine : String; piCount : Integer) : String;
var liLen : Integer;
begin

  liLen:=uLength(psLine);
  if liLen>piCount then
    Result:=uCopy(psLine,Succ(liLen-piCount),liLen)
  else
    Result:=psLine;
end;


function upperTrimLine(psLine : String) : String;
begin

  {$ifdef __LAZARUS__}
  Result:=UTF8UpperCase(uTrim(psLine));
  {$else}
  Result:=AnsiUpperCase(uTrim(psLine));
  {$endif}
end;


function isEqualString(psFirst,psSecond : String) : Boolean;
begin

  Result:=upperTrimLine(psFirst)=upperTrimLine(psSecond);
end;


begin
  (*
  {$ifdef __LAZARUS__}
    g_sProgrammFolder:=ExtractFileDir(Application.Exename);
  {$else}
    g_sProgrammFolder:=ExtractFileDir(ParamStr(0));
  {$endif}
  SlashIt(g_sProgrammFolder);
  *)
end.





