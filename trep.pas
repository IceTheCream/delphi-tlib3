// TLibrary ver 3.5 by Pakhomenkov. A. P. pakhomenkov.ap@yandex.ru 2011
unit trep;


interface


uses Windows,Classes, DB, SysUtils, StrUtils,ShlObj,ShellApi
     ,ComCtrls
     ,forms
     ,tlib,tstr,twin,tvcl
     ;


const ciMaxFieldsInBand        = 256;
      ciMaxBandsInReport       = 32;
      ciHROrientationPortrait  = 1;
      ciHROrientationLandscape = 2;
      ciHRBand                 = 3;
      ciHRDetailBand           = 4;
      ciHRTHead                = 5;
      ciHRTFoot                = 6;
      ciHRBandsCount           = 7 ;
      ciHRFirstPageLength      = 8 ;
      ciHRPageLength           = 9;
      ciHRTDTag                = 10;
      ciHRDetailBandsCount     = 11;
      ciHRPageNumber           = 12;
      ciHRPageCount            = 13;
      ciHRHeader               = 14;
      ciHRFooter               = 15;
      ciHRRowNumber            = 16;
      ciHREmptyField           = 17;
      ciHRDetailRowNumber      = 18;
      ciHRBandsBegin           = 19;
      ciHRBandsEnd             = 20;
      //ciHRValue                = 21;
      ciHRMaxArguments         =  3;
      ccHRTokenChar            = '@';
      ccHRFieldChar            = '$';
      ccIDChar                 = '#';
      ccHRTokenDelimiter       = ' ';
      ccHRArgumentDelimiter    = ':';
      ccHRValueChar            = '"';
      ciMaxVars                = 16;

      ciHRTokenCount           = 20;
      casTokens : array[1..ciHRTokenCount] of String = (
        '@portrait','@landscape','@band',
        '@detailband','@thead','@tfoot','@bandscount',
        '@firstpagelen','@pagelen','@td','@detailbandscount',
        'pagenumber','pagecount','header','footer',
        '$_rownumber','$_emptyfield','$_detrownumber',
        '@begin','@end');


type //***** ���������� HTML ������
     THROrientation = (hroPortrait,hroLandScape);

     //***** ������������
     THRAlign = (hraLeft,hraCenter,hraRight);

     //***** �� �����, ����� ������� ������������ �����, �� �����?
     THRLineDir = (hrdNone,hrdBefore,hrdAfter);

     //***** ������ "���� ������ ������"
     THRBandField = class(TObject)
       private

         msClass     : String;
         msID        : String;
         msFieldName : String;
         msSpan      : String;
         mblValueFlg : Boolean;

       public

         constructor Create;
         procedure   SetData(psFieldName : String='$_emptyfield'; psClass : String=''; psID : String=''; psSpan:String='1');
         function    GetToken : String;
         function    GetFieldName : String;
         function    GetClass : String;
         function    GetID : String;
         function    GetStyleString : String;
         function    GetSpan : String;
         function    IsValue : Boolean;
     end;


     //***** ������ "������ ������"
     THRBand = class(TObject)
       protected

         msBeforeBand : String;
         msAfterBand  : String;

       public

         moFields     : TList;

         constructor Create;
         destructor  Destroy; override;
         function    Parse(psBeforeLine,psLine : String) : Boolean;
         function    GetBeforeBand : String;
         function    GetAfterBand : String;
     end;

     (*
     TAbstractReport = class(TObject)
       public
         constructor Create; abstract;
         destructor Destroy; abstract; override;
         function ProcessTemplate(psReport : String) : Boolean; abstract; dynamic;
         function SelectTemplate(psTemplate : String) : Boolean; abstract;
         function SetHeader(psHeader : String) : Boolean;  abstract;
         function SetFooter(psFooter : String) : Boolean;  abstract;
         function SetDataSet(poDataSet : TDataSet) : Boolean;  abstract; dynamic;
         function ParseLine(psLine : String) : Boolean; abstract; dynamic;
         function ProcessBands : Boolean;  abstract; dynamic;
     end;
     *)

     //***** ������ "HTML �����"
     THTMLReport = class{(TAbstractReport)}
       private

       protected

         moTemplate         : TStringList;
         moReport           : TStringList;
         moBeforeLines      : TStringList;
         moAfterLines       : TStringList;
         moBands            : TList;
         msHeader           : String;
         msFooter           : String;
         moOrientation      : THROrientation;
         moDataSet          : TDataSet;
         msErrorMessage     : String;
         miBandsCount       : Integer;
         miFirstPageLength  : Integer;
         miNextPageLength   : Integer;
         moLinesDirection   : THRLineDir;
         //masValues          : array[1..ciMaxVars] of String;
         //miValuesIdx        : Integer;
         //miLinesBeforeBands : Integer;
        //function ParseArguments(psTok  en : String) : Integer;

        function ParseArguments(var psToken : String) : Integer;
        function TokenHandler(psToken,psLine : String) : String; dynamic;
        function Identify(psToken : String) : Integer;
       public

         constructor Create;
         destructor Destroy; override;
         function GetErrorMessage : String;
         function SelectTemplate(psTemplate : String) : Boolean;
         function SetHeader(psHeader : String) : Boolean;
         function SetFooter(psFooter : String) : Boolean;
         function SetDataSet(poDataSet : TDataSet) : Boolean; dynamic;
         function ParseLine(psLine : String) : Boolean; dynamic;
         function ProcessBand(poDataSet : TDataSet; poBands : TList; piBandIdx, piRecordNum : Integer) : String;
         function ProcessReport(psTemplate,psReport : String; poProgressBar : TProgressBar = Nil) : Boolean; dynamic;
     end;


     //***** ������ "HTML Master-Detail �����"
     THTMLMasterDetailReport = class(THTMLReport)
       protected
         //moDetailDatasets   : TList;
         moDetailDataSet    : TDataSet;
         moDetailBands      : TList;
         miDetailBandsCount : Integer;
        //function TokenHandler : String; override;
        function TokenHandler(psToken,psLine : String) : String; override;
       public
         constructor Create;
         destructor Destroy; override;
         //function ProcessTemplate(psReport : String) : Boolean; override;
         //function ParseLine(psLine : String) : Boolean; override;
         //function SetMasterDataSet(poDataSet : TDataSet) : Boolean;
         function SetDetailDataSet(poDataSet : TDataSet) : Boolean;
         //function ProcessReport(psTemplate,psReport : String) : Boolean; override;
         function ProcessReport(psTemplate,psReport : String; poProgressBar : TProgressBar = Nil): Boolean; override;
         //function ProcessReport : Boolean; override;
     end;

function IsTokenExists(psLine : String) : Integer;
function GetNextToken(var psSrcLine,psPartBefore : String) : String;
function IsArgumentExists(psLine : String) : Integer;
function GetNextTokenArgument(var psToken : String) : String;



implementation

var uasArguments      : array[1..ciHRMaxArguments] of String;
    uiArgumentIdx     : Integer;

(*       moBands           : TList; //new
         moOrientation     : THROrientation;
         moDataSet         : TDataSet;
         msErrorMessage    : String;
         miFirstPageLength : Integer;
         miNextPageLength  : Integer;
*)

{===<< THRBandField >>===}
constructor THRBandField.Create;
begin
  inherited;
  msSpan:='1';
end; {- c THRBandField.Create; -}


function THRBandField.GetClass: String;
begin
  Result:=msClass;
end; {- f THRBandField.GetClass: String;-}


function THRBandField.GetFieldName : String;
begin
  if mblValueFlg then
    Result:=msFieldName
  else
    Result:=Copy(msFieldName,2,Pred(Length(msFieldName)));
end; {- f THRBandField.GetFieldName: String; -}


function THRBandField.GetID: String;
begin
  Result:=msID;
end; {- f THRBandField.GetID: String; -}


function THRBandField.GetStyleString: String;
begin
  Result:='class="'+msClass+'" id="'+msID+'"';
end; {- f THRBandField.GetStyleString: String; -}


function THRBandField.GetToken: String;
begin
  Result:=msFieldName;
end; function THRBandField.IsValue: Boolean;
begin
  Result:=mblValueFlg;
end;

{- f THRBandField.GetToken: String; -}


function THRBandField.GetSpan: String;
begin
  Result:=msSpan;
end; {- f THRBandField.GetSpan: String; -}


procedure THRBandField.SetData(psFieldName : String='$_emptyfield';psClass : String='';psID : String='';psSpan:String='1');
var liLen : Integer;
begin
  psFieldName:=Trim(psFieldName);
  liLen:=Length(psFieldName);
  msClass:=psClass;
  msID:=psID;
  //msFieldName:=psFieldName;
  msSpan:=psSPan;
  mblValueFlg:=(psFieldName[1]=ccHRValueChar) and
                 (psFieldName[liLen]=ccHRValueChar);
  if mblValueFlg then begin
    msFieldName:=Cut(psFieldName,2,liLen-2);
  end else begin
    msFieldName:=psFieldName;
  end;

                
end; {- p THRBandField.SetData(psFieldName : String='$_emptyfield';psClass : String='';psID : String='';psSpan:String='1'); -}


{===<< THRBand >>===}
constructor THRBand.Create;
begin
  inherited;
  moFields:=TList.Create;
end; {- c THRBand.Create; -}


destructor THRBand.Destroy;
begin
  FreeAndNil(moFields);
end; {- d THRBand.Destroy; -}


function THRBand.GetAfterBand: String;
begin
  Result:=msAfterBand;
end; {- f THRBand.GetAfterBand: String; -}


function THRBand.GetBeforeBand: String;
begin
  Result:=msBeforeBand;
end; {- f THRBand.GetBeforeBand: String; -}


function THRBand.Parse(psBeforeLine,psLine : String) : Boolean;
var lsLine        : String;
    lsToken       : String;
    lsResultLine  : String;
    liArgumentIdx : Integer;
    lasArguments  : array[1..ciHRMaxArguments] of String;
    loField       : THRBandField;
begin

  lsLine:=psLine;
  msBeforeBand:=psBeforeLine;

  while IsTokenExists(lsLine)>0 do begin
    lsToken:=GetNextToken(lsLine,lsResultLine);
    liArgumentIdx:=0;

    while (IsArgumentExists(lsToken)>0) and (liArgumentIdx<=ciHRMaxArguments) do begin
      lasArguments[Succ(liArgumentIdx)]:=Trim(GetNextTokenArgument(lsToken));

      if Pos(':',lasArguments[Succ(liArgumentIdx)])>0 then
        Raise EConvertError.Create(psLine+' - '+lasArguments[Succ(liArgumentIdx)]);
      Inc(liArgumentIdx)
    end; {- while (IsArgumentExists(lsToken)>0) and (liArgumentIdx<=ciHRMaxArguments) do begin -}
    loField:=THRBandField.Create;

    if liArgumentIdx=0 then begin
      {---< ������ ��� ���� >---}
      loField.SetData(lsToken);
    end else begin
      if liArgumentIdx=1 then begin
        {---< ��� ���� � ����� >---}
        loField.SetData(lsToken,lasArguments[1]);
      end else begin
        {---< ��� ����, ����� � id >---}
        if liArgumentIdx=2 then begin
          loField.SetData(lsToken,lasArguments[1],lasArguments[2]);
        end else begin
          {---< ��� ����, �����, id � span >---}
          loField.SetData(lsToken,lasArguments[1],lasArguments[2],lasArguments[3]);
        end;
      end;
    end;

    moFields.Add(loField);
  end; {- while IsTokenExists(lsLine)>0 do begin -}
  msAfterBand:=lsLine;
  Result:=True;
end; {- f THRBand.Parse(psBeforeLine,psLine : String) : Boolean; -}


{===<< THTMLReport >>===}
constructor THTMLReport.Create;
begin
  inherited;
  { ������������� }
  moTemplate:=TStringList.Create;
  moReport:=TStringList.Create;
  moBeforeLines:=TStringList.Create;
  moAfterLines:=TStringList.Create;
  moBands:=TList.Create;
  miBandsCount:=-1;
  msHeader:='';
  msFooter:='';
  {$ifdef _USE_LOG_}
  fmLog.Show;
  {$endif}
  moLinesDirection:=hrdBefore;
  //miValuesIdx:=0;
end; {- c THTMLReport.Create; -}


destructor THTMLReport.Destroy;
begin
  FreeAndNil(moTemplate);
  FreeAndNil(moReport);
  FreeAndNil(moBands);
  FreeAndNil(moBeforeLines);
  FreeAndNil(moAfterLines);
  inherited;
end; {- d THTMLReport.Destroy; -}


function THTMLReport.GetErrorMessage: String;
begin
  Result:=msErrorMessage;
end; {---< f THTMLReport.GetErrorMessage: String; >---}


function THTMLReport.Identify(psToken : String) : Integer;
var liIdx : Integer;
    lsToken : String;
begin
  Result:=0;
  lsToken:=LowerCase(psToken);
  for liIdx:=1 to ciHRTokenCount do begin
    if casTokens[liIdx]=lsToken then begin
      Result:=liIdx;
      break;
    end; {- if casTokens[liIdx]=lsToken then begin -}
  end; {- for liIdx:=1 to ciHRTokenCount do begin -}
end; {- f THTMLReport.Identify(psToken : String) : Integer; -}


function IsTokenExists(psLine : String) : Integer;
begin
  Result:=Pos(ccHRTokenChar,psLine);
end; {- f THTMLReport.IsTokenExists(psLine : String) : Integer; -}


function GetNextToken(var psSrcLine,psPartBefore : String) : String;
var liTokenPos : Integer;
    liDelimPos : Integer;
    liLineLen  : Integer;
    lsLine     : String;
    lsResult   : String;
begin
  lsResult:='';
  lsLine:=psSrcLine;
  liTokenPos:=IsTokenExists(lsLine);

  {---< ����� ���-��? >---}
  if liTokenPos>0 then begin

    {---< ���� ����� ������� ���-�� ����, ��������� ��� ����� ������ >---}
    if liTokenPos>1 then
      psPartBefore:=psPartBefore+Cut(lsLine,1,Pred(liTokenPos));

    liLineLen:=Length(lsLine);

    {---< ���� ����������� >---}
    liDelimPos:=Pos(ccHRTokenDelimiter,lsLine);
    if (liDelimPos>0) and (liDelimPos<liLineLen) then begin
      lsResult:=Cut(lsLine,1,Pred(liDelimPos))
    end else begin
      lsResult:=lsLine;
      lsLine:='';
    end; {- if (liDelimPos>0) and (liDelimPos<liLineLen) then else -}
    Delete(lsResult,1,1); { ������� ���� ������ }
    psSrcLine:=lsLine;
  end; {- if liTokenPos>0 then begin -}
  GetNextToken:=lsResult;
end;


function IsArgumentExists(psLine : String) : Integer;
begin
  IsArgumentExists:=Pos(ccHRArgumentDelimiter,psLine);
end;{- f THTMLReport.IsArgumentExists(psLine : String) : Integer; -}


function GetNextTokenArgument(var psToken : String) : String;
var liDelimPos    : Integer;
    lsToken       : String;
    lsResult      : String;
begin
  //$FIO:colst:coll
  lsResult:='';
  lsToken:=psToken;
  liDelimPos:=IsArgumentExists(lsToken);
  {---< ����� ���������� ������ ���� ����� >---}
  if liDelimPos>0 then begin

    {---< ���� ��. ���������� ��� >---}
    psToken:=Cut(lsToken,1,Pred(liDelimPos));
    Delete(lsToken,1,1); { ������� ����������� }

    {---< ���� ������ ���������� ��������� >---}
    liDelimPos:=IsArgumentExists(lsToken);

    {---< ���� �� ���� � �� � ����� ����� ������ (?)  >---}
    if (liDelimPos>0) and (liDelimPos<Length(lsToken)) then begin
      {---< ���������� �������� >---}
      lsResult:=Cut(lsToken,1,Pred(liDelimPos));
      {---< � var ���������� ���������� �����+������� ���������� >---}
      psToken:=psToken+lsToken;
    end else begin
      {---< ����� ���������� ������� ������ - ��������� �������� >---}
      {---< ��� ���� � var ���������� ������������ ������ ������ - ����� >---}
      // ^$#^$#%#^$!!!!! �� ��� ��� ���� ��� �����������???
      lsResult:=lsToken;
    end; {- if (liDelimPos>0) and (liDelimPos<Length(lsToken)) then -}
  end; {- if liDelimPos>0 then begin -}
  GetNextTokenArgument:=lsResult;
end; {- f THTMLReport.GetNextTokenArgument(var psToken : String) : String; -}


function THTMLReport.ParseArguments(var psToken : String) : Integer;
var liIdx : Integer;
begin
  liIdx:=0;
  while (IsArgumentExists(psToken)>0) and (liIdx<=ciHRMaxArguments) do begin
    uasArguments[Succ(liIdx)]:=GetNextTokenArgument(psToken);
    Inc(liIdx)
  end; {- while (IsArgumentExists(lsToken)>0) and (liIdx<=ciHRMaxArguments) do begin -}
  ParseArguments:=liIdx;
end; {- f THTMLReport.ParseArguments(psToken : String) : Integer; -}


function THTMLReport.TokenHandler(psToken,psLine : String) : String;
var lsResult : String;
    loBand   : THRBand;
begin
  lsResult:='';
  case Identify(psToken) of
    ciHROrientationPortrait:  moOrientation:=hroPortrait;
    ciHROrientationLandscape: moOrientation:=hroLandScape;
    ciHRFirstPageLength:      if uiArgumentIdx>0 then miFirstPageLength:=StrToIntDef(uasArguments[1],-1);
    ciHRPageLength:           if uiArgumentIdx>0 then miNextPageLength:=StrToIntDef(uasArguments[1],-1);
    ciHRHeader:               lsResult:=msHeader;
    ciHRFooter:               lsResult:=msFooter;
    ciHRBandsCount : begin
      if uiArgumentIdx>0 then begin
        miBandsCount:=StrToIntDef(uasArguments[1],-1);
        if miBandsCount<0 then begin
          msErrorMessage:='BandsCount: �������� ��������!';
          Raise EConvertError.Create(msErrorMessage);
        end; {- if miBandsCount<0 then begin -}
      end; {- if miArgumentIdx>0 then begin -}
    end; {- ciHRBandsCount -}
    ciHRBand: begin
      loBand:=THRBand.Create;
      loBand.Parse(lsResult,psLine);
      moBands.Add(loBand); // ��� �������� �������.
    end; {- ciHRBand -}
    ciHRBandsBegin : begin
      moLinesDirection:=hrdNone;
    end;
    ciHRBandsEnd : begin
      moLinesDirection:=hrdAfter;
    end;
    //ciHRTHead:
    //ciHRTFoot:
    //ciHRPageNumber:
    //ciHRPageCount:
  end; {- case Identify(psToken) of -}
  TokenHandler:=lsResult;
end; {- f THTMLReport.TokenHandler(psToken : String) : String; -}


function THTMLReport.ParseLine(psLine: String): Boolean;
var lsToken,
    lsLine,
    lsResultLine   : String;
    //loBand         : THRBand;
begin
  ParseLine:=True;
  lsLine:=psLine; { ������� �� ����������� ��������� �������� }
  lsResultLine:='';
  while IsTokenExists(lsLine)>0 do begin
    lsToken:=GetNextToken(lsLine,lsResultLine);
    uiArgumentIdx:=ParseArguments(lsToken);
    try
      lsResultLine:=TokenHandler(lsToken,lsLine);
    except
      ParseLine:=False;
      exit;
    end; {- try -}
  end; {- while IsTokenExists(lsLine)>0 do begin -}
  lsResultLine:=lsResultLine+lsLine;
  if not IsEmpty(lsResultLine) then begin
    case moLinesDirection of
      hrdBefore: moBeforeLines.Add(lsResultLine);
      hrdAfter:  moAfterLines.Add(lsResultLine);
    end;
  end;
end; {- f THTMLReport.ParseLine(psLine: String): Boolean; -}


function THTMLReport.ProcessBand(poDataSet : TDataSet; poBands : TList; piBandIdx, piRecordNum : Integer) : String;
var liJdx   : Integer;
    loBand  : THRBand;
    lsBand,
    lsSpan  : String;
    loField : THRBandField;
begin
  //for liIdx:=0 to Pred(poBands.Count) do begin
  {---< ������� ���� >---}
  loBand:=poBands.List[piBandIdx];
  {---< ������� ������ ������� >---}
  lsBand:=loBand.GetBeforeBand+'<tr>';
  {---< ���������� ���� � ������� ����� >---}
  for liJdx := 0 to Pred(loBand.moFields.Count) do begin
    {---< ������� ���� >---}
    loField:=loBand.moFields.List[liJdx];
    {---< ���������� ����������� ��� �� ������ >---}
    lsBand:=lsBand+'<td '+loField.GetStyleString;
    {---< ����� �� ������� �������? >---}
    lsSpan:=loField.GetSpan;
    if StrToIntDef(lsSpan,1) >1 then begin
      lsBand:=lsBand+' colspan="'+lsSpan+'"';
    end; {- if StrToIntDef(lsSpan,1) >1 then begin -}
    lsBand:=lsBand+'>';
    {---< ���������, ��� ��� �������� - ��������� ���� ��� �� ������� >---}
    case Identify(loField.GetToken) of
      {---< ����� ������ >---}
      ciHRRowNumber: begin
        lsBand:=lsBand+IntToStr(piRecordNum);
      end;
      {---< ������ ������ >---}
      // ��� !!! 25.01.10
      {
      ciHRConstant: begin
        lsBand:=lsBand+loField.GetFieldName;
      end;
      }
      ciHREmptyField: begin
        lsBand:=lsBand+' ';
      end else begin
        {---< ���� �� ������� >---}
        if loField.IsValue then
          lsBand:=lsBand+loField.GetFieldName
        else
          lsBand:=lsBand+poDataSet.FieldByName(loField.GetFieldName).AsString;
      end;
    end; {- case Identify(loField.GetToken) of -}
    {---< ������� ��� >---}
    lsBand:=lsBand+'</td>'
  end; {- for liFieldsIdx := 0 to Pred(loBand.moFields.Count) do begin -}
  ProcessBand:=lsBand+'</tr>'+loBand.GetAfterBand;
end;


function THTMLReport.ProcessReport(psTemplate,psReport : String; poProgressBar : TProgressBar = Nil) : Boolean;
//function THTMLReport.ProcessReport: Boolean;
var liBandsIdx  : Integer;
    //liFieldsIdx : Integer;
    //loBand      : THRBand;
    //loField     : THRBandField;
    liRecordNum : Integer;
    //lsBand      : String;
    //lsSpan      : String;
    liIdx       : Integer;
begin
  moTemplate.LoadFromFile(psTemplate);
  moReport.Clear;
  for liIdx:=0 to Pred(moBeforeLines.Count) do
    moReport.Add(moBeforeLines.Strings[liIdx]);

  {---< ���������� ��� ������ ������� ������  >---}
  for liIdx:=0 to Pred(moTemplate.Count) do begin
    ParseLine(moTemplate.Strings[liIdx]);
  end;

  { ���� �� ���� ���� ����? }
  if moBands.Count>0 then begin
    { �������������� ������� ����� }
    liRecordNum:=1;
    {---< ���� �� ���� ������� >---}
    moDataSet.First;
    if poProgressBar <>Nil then
      ProgressSetup(poProgressBar,moDataSet.RecordCount);
      //InitProgressBar(poProgressBar,moDataSet.RecordCount);
    while not moDataSet.Eof do begin
      {---< ���������� ����� >---}
      {$ifdef _USE_LOG_}
      fmLog.meLog.Lines.Add(moDataSet.FieldByName('FIO').AsString);
      {$endif}
      for liBandsIdx:=0 to Pred(moBands.Count) do begin
        moReport.Add(ProcessBand(moDataSet,moBands,liBandsIdx,liRecordNum));
      end; {- for liBandsIdx:=0 to Pred(moBands.Count) do begin -}
      moDataSet.Next;
      Inc(liRecordNum);
      if poProgressBar<>Nil then
      begin
        //NextStep(poProgressBar,liRecordNum);
        ProgressNext(poProgressBar,liRecordNum);
      end;
      Application.ProcessMessages;
    end; {- while not moDataSet.Eof do begin -}
  end; {- if moBands.Count>0 then begin -}
  for liIdx:=0 to Pred(moAfterLines.Count) do
    moReport.Add(moAfterLines.Strings[liIdx]);
  moReport.SaveToFile(psReport);
  Result:=True;
end; {- f THTMLReport.ProcessBands: Boolean; -}


function THTMLReport.SelectTemplate(psTemplate: String): Boolean;
begin
  Result:=True;
  moTemplate.Clear;
  if FileExists(psTemplate) then begin
    try
      moTemplate.LoadFromFile(psTemplate);
    except
      Result:=False;
    end; {- try -}
  end else begin
    Result:=False;
  end; {- if FileExists(psTemplate) then else -}
end; {- f THTMLReport.SelectTemplate(psTemplate: String): Boolean; -}


function THTMLReport.SetHeader(psHeader: String): Boolean;
begin
  msHeader:=psHeader;
  Result:=True;
end; {- f THTMLReport.SetHeader(psHeader: String): Boolean; -}


function THTMLReport.SetDataSet(poDataSet: TDataSet): Boolean;
begin
  moDataSet:=poDataSet;
  Result:=True;
end; {- f THTMLReport.SetDataSet(poDataSet: TDataSet): Boolean; -}


function THTMLReport.SetFooter(psFooter: String): Boolean;
begin
  msFooter:=psFooter;
  Result:=True;
end; {- f THTMLReport.SetFooter(psFooter: String): Boolean; -}


constructor THTMLMasterDetailReport.Create;
begin
  inherited;
  { ������������� }
  moDetailBands:=TList.Create;
  {$ifdef _USE_LOG_}
  fmLog.Show;
  {$endif}
end; {- c THTMLReport.Create; -}


destructor THTMLMasterDetailReport.Destroy;
begin
  FreeAndNil(moDetailBands);
  inherited;
end; {- d THTMLReport.Destroy; -}


function THTMLMasterDetailReport.TokenHandler(psToken,psLine : String) : String;
var lsResult : String;
    loBand   : THRBand;
begin
  lsResult:='';
  case Identify(psToken) of
    ciHRDetailBandsCount : begin
      if uiArgumentIdx>0 then begin
        miDetailBandsCount:=StrToIntDef(uasArguments[1],-1);
        if miDetailBandsCount<0 then begin
          msErrorMessage:='BandsCount: �������� ��������!';
          Raise EConvertError.Create(msErrorMessage);
        end;
      end;
    end;
    ciHRDetailBand: begin
      loBand:=THRBand.Create;
      loBand.Parse(lsResult,psLine);
      moDetailBands.Add(loBand); // ��� �������� �������.
    end;
  else
    inherited TokenHandler(psToken,psLine);
  end; {- case Identify(psToken) of -}
  TokenHandler:=lsResult;
end; {- f THTMLReport.TokenHandler(psToken : String) : String; -}


function THTMLMasterDetailReport.SetDetailDataSet(poDataSet: TDataSet): Boolean;
begin
  moDetailDataSet:=poDataSet;
  Result:=True;
end;


function THTMLMasterDetailReport.ProcessReport(psTemplate,psReport : String; poProgressBar : TProgressBar = Nil): Boolean; // ����
var liBandsIdx  : Integer;
    liRecordNum : Integer;
    liDetRecNum : Integer; // ?
    liIdx       : Integer;
begin
  //ProcessReport:=True;

  try
    moTemplate.LoadFromFile(psTemplate);
  except
    FatalError('������!','���������� ��������� ������ ������!');
  end;

  {---< ���������� ��� ������ ������� ������  >---}
  for liIdx:=0 to Pred(moTemplate.Count) do begin
    ParseLine(moTemplate.Strings[liIdx]);
  end;

  moReport.Clear;

  for liIdx:=0 to Pred(moBeforeLines.Count) do
    moReport.Add(moBeforeLines.Strings[liIdx]);

  { ���� �� ���� ���� ����? }
  if moBands.Count>0 then begin
    { �������������� ������� ����� }
    liRecordNum:=1;
    {---< ���� �� ���� ������� >---}
    moDataSet.First;
    moDetailDataSet.FindFirst;
    if poProgressBar <>Nil then
      //InitProgressBar(poProgressBar,moDataSet.RecordCount);
      ProgressSetup(poProgressBar,moDataSet.RecordCount);
    while not moDataSet.Eof do begin
      {---< ���������� ����� >---}
      {$ifdef _USE_LOG_}
      fmLog.meLog.Lines.Add(moDataSet.FieldByName('FIO').AsString);
      {$endif}
      for liBandsIdx:=0 to Pred(moBands.Count) do begin
        moReport.Add(ProcessBand(moDataSet,moBands,liBandsIdx,liRecordNum));
      end; {- for liBandsIdx:=0 to Pred(moBands.Count) do begin -}
      // ��� ���, ���� ���, ����� �������� ��������� ���������� ��������
      liDetRecNum:=1;
      //moDetailDataset.First;
      while not moDetailDataSet.Eof and
           (moDetailDataSet.FieldByName('CONTR_ID').AsInteger=
            moDataSet.FieldByName('ID').AsInteger)  do begin
        for liBandsIdx:=0 to Pred(moDetailBands.Count) do begin
          moReport.Add(ProcessBand(moDetailDataset,moDetailBands,liBandsIdx,liDetRecNum));
        end; {- for liBandsIdx:=0 to Pred(moBands.Count) do begin -}
        moDetailDataSet.Next;
      end;
      moDataSet.Next;
      Inc(liRecordNum);
      if poProgressBar<>Nil then
      begin
        //NextStep(poProgressBar,liRecordNum);
        ProgressNext(poProgressBar,liRecordNum);
      end;
      Application.ProcessMessages;
    end; {- while not moDataSet.Eof do begin -}
  end; {- if moBands.Count>0 then begin -}
  for liIdx:=0 to Pred(moAfterLines.Count) do
    moReport.Add(moAfterLines.Strings[liIdx]);
  moReport.SaveToFile(psReport);
  Result:=True;
end;


end.
