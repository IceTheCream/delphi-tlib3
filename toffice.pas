// TLibrary ver 4 by Pakhomenkov. A. P. pakhomenkov.ap@yandex.ru 2011

{ �������� ������� ����� �� ������ ������� ���������,www.kornjakov.ru,kvn@mail.ru,
 �������, �������! :) }

unit toffice;

 
interface

{$i profile.inc}

uses ComObj,Windows{,Word2000},Variants,SysUtils,Classes,ActiveX

  {$ifdef __LAZARUS__}
  ,FileUtil
  {$endif}
  ,tstr;

{$i excel.inc}
{$i tconst.inc}

type TOfficeType = (toMSOffice,toOpenOffice);

     { TEasyOffice }
     EOfficeError = class of Exception;

     TEasyOffice = class(TObject)
       
       private
         
         mblValid       : Boolean;
         moOffice       : TOfficeType;
         moStarOffice,
         moStarDesktop,
         moDocument,
         moWorkBook,
         moSheets,
         moSheet,
         moCell,
         moMyStruct     : Variant;
         moExcel        : OleVariant;
         moTextCursor   : Variant;
         miExcelSheet   : Integer;
         miSheetNumber  : Integer;
         {** @abstract(������� ������� ��������� ��� �����-�� ������ �����)
             @param(PropName)
             @param(PropValue)
             @returns(���������)
             @code(VarArr[0]:=MakePropertyValue('FilterName', 'MS Excel 97');) }
         function MakePropertyValue(PropName, PropValue:string):variant;
         {function Lazarus_OO_Create() : Boolean;
         function Delphi_OO_Create() : Boolean;
         }
         function Lazarus_OO_OpenSpreadSheet(psFileName : String) : Boolean;
         function Delphi_OO_OpenSpreadSheet(psFileName : String) : Boolean;
       public
         //***** ��� ������� ����� � ������.
         {$ifdef __DIRECT_EXCEL__}
         function  Excel_Create() : Boolean;
         function  Excel_GetSheetsCount() : Integer;
         function  Excel_GetSheetName(piIdx : Integer) : String;
         procedure Excel_setVisible(pblVisible : Boolean = True);
         procedure Excel_renameSheet(piSheetNumber : Integer; psNewName : String);
         //procedure Excel_WriteToRange(psRange : String; poData : Variant);
         procedure Excel_writeToStripeRange(piBeginCol,piBeginRow,piEndCol,piEndRow : Integer; poData: Variant);
         procedure Excel_save();
         function  Excel_get: variant;
         function  Excel_getSheet: variant;
         {$endif}
         function  Excel_GetExactValueFromCell(piCol, piRow : Integer) : Double;

         {** @abstract(�����������. ������� ������ ������ TEasyOffice)
             @param(poOffice �������� ���� TOfficeType. ���������, � �����
                    ����� ����� ������ �������� ������ - Open/Libre ���� MS)
             @code(office:=TEasyOffice.Create(toOpenOffice)) }
         constructor Create(poOffice : TOfficeType);

         {** @abstract(���������. ��������� ������ ����� ��������� ������)
             @param(piAfterWhichRow �������� ���� Integer. ���������, �����
                    ����� ������ ����� �������� ����� ������)
             @param(piRowCount �������� ���� Integer. �������� ����������
                    ����������� �����, �� ��������� 1)
             @code(office:=TEasyOffice.insertRow(4)) }
         ///procedure insertRow(piAfterWhichRow : Integer);
         procedure insertRow(piAfterWhichRow: Integer; piRowCount : Integer=1);


         function IsValid : Boolean;
         function CreateNewSpreadSheet : Boolean;
         function OpenSpreadSheet(psFileName : String) : Boolean;
         procedure CloseDocument;
         procedure Show;
         procedure OpenSheetByIdx(piSheet : Integer);
         procedure SaveDocument(const psFileName : String);
         procedure WriteStringInCell(piCol,piRow : Integer; psValue : String);
         procedure WriteValueInCell(piCol, piRow : Integer; pdValue : Double);
         procedure WriteValueInRange(psRange : String; pdValue : Double);
         procedure WriteStringsInCellRange(piStartCol, piStartRow : Integer; poList : TStringList);
         function  GetValueFromCell(piCol, piRow : Integer) : Double;
         function  GetExactValueFromCell(piCol, piRow : Integer) : Double;
         function  GetValueFromRange(psRange : String) : Double;
         function  getStringFromCell(piCol,piRow : Integer) : String;
         function  getRows() : Integer;
         function  getCols() : Integer;
         function  getSheetNumber() : Integer;
         //procedure setRowHeight(piRowIdx,piHeight : Integer);
         procedure setRowHeight(piRowIdx : Integer; prHeight : Real);
         procedure selectRange({%H-}psCellBegin, {%H-}psCellEnd : String);
         procedure drawCellBorder(piCol, piRow, piBorder, {piLine,} {%H-}piLineStyle, piWidth : Integer);
         function  convertColAndRowToCellAddress(piCol,piRow : Integer) : String;
         destructor Destroy; override;
     end;


const csStarOfficeManager  = 'com.sun.star.ServiceManager';
      csStarOfficeDesktop  = 'com.sun.star.frame.Desktop';
      csStarOfficeStruct   = 'com.sun.star.table.BorderLine';
      csStarOfficeCalcURL  = 'private:factory/scalc';
      csStarOfficeProperty = 'com.sun.star.beans.PropertyValue';
      csExcelApp           = 'Excel.Application';
      csODSExt             = '.ods';
      csXLSExt             = '.xls';
      csPropExcel          = 'MS Excel 97';
      csPropSCalc          = 'private:factory/scalc';

      {      ciAColumn = 0;
      ciBColumn = 1;
      ciCColumn = 2;
      ciDColumn = 3;
      ciEColumn = 4;
      ciFColumn = 5;
      ciGColumn = 6;
      ciHColumn = 7;
      ciIColumn = 8;
      ciJColumn = 9;
      ciKColumn = 10;
      ciLColumn = 11;
      ciMColumn = 12;
      ciNColumn = 13;
      ciOColumn = 14;
      ciPColumn = 15;
      ciQColumn = 16;
      ciRColumn = 17;
      ciSColumn = 18;
      ciTColumn = 19;
      ciUColumn = 20;
      ciVColumn = 21;
      ciWColumn = 22;
      ciXColumn = 23;
      ciYColumn = 24;
      ciZColumn = 25;
      ciAAColumn = 26;
      ciABColumn = 27;
      ciACColumn = 28;

      ciEdgeBottom       =  1;
      ciEdgeLeft         =  2;
      ciEdgeRight        =  4;
      ciEdgeTop          =  8;
      ciEdgeBox          = 16;
      ciEdgeHasBorder    =  1;
      ciEdgeHasNoBorder  =  2;

      ciInsideHorizontal = 5;
      ciInsideVertical   = 6;


xlDiagonalDown 5 - ������������ �� �������� ������ ���� � ������ ������ ������ ������ � ���������
xlDiagonalUp 6 - ������������ �� ������� ������ ���� � ������ ������� ������ ������ � ���������.
xlEdgeBottom 9 - ������� ��� ����� ��������� �����
xlEdgeLeft 7 - ����� ��� ����� ��������� �����.
xlEdgeRight 10 - ������ ��� ����� ��������� �����.
xlEdgeTop 8 - ������� ��� ����� ��������� �����.
xlInsideHorizontal 12 - �������������� ������� ���� ���������� ����� ���������
xlInsideVertical 11 - ������������ ������� ���� ���������� ����� ���������
xlContinuous = 1;
 //(_ _ _ _ _) � ���� ������������������ ����.
 xlDash = -4115;
 //(_._._._._) � ���� ���� � �����.
 xlDashDot = 4;
 //(_.._.._..) � ���� ���� � ������� �����.
 xlDashDotDot = 5;
 //(.........) � ���� �����.
 xlDot = -4118;
 //(=========) � ���� ������� �����.
 xlDouble = -4119;
 //(         ) ���������� �����.
 xlLineStyleNone = -4142;
 //(/././././) � ���� ��������� ���� � �����.
 xlSlantDashDot = 13;

 //������ �������� XlBorderWeight. ����� ������� �����.
 //
 //����� ������.
 xlHairline = 1;
 //������
 xlThin = 2;
 //�������.
 xlMedium = -4138;
 //�������.
 xlThick = 4;}

implementation


// thanks to Yuri (aka Yuric74)
function ConvertToURL(psFileName : String) : String;
var liIdx : Integer;
begin
  Result:='';
  // ������������!
  for liIdx:=1 to Length(psFileName) do begin

    case psFileName[liIdx] of

      ' ':Result:=Result+'%20';
      '\':Result:=Result+'/';
      //':':Result:=Result+'|'; //������ ��� �������
    else
      Result:=Result+psFileName[liIdx];
    end;
  end;
  {$ifdef __LAZARUS__}

  Result:=SysToUTF8('file:///'+Result);
  {$else}
  Result:='file:///'+Result;
  {$endif}
end;

{$Region '_Excel'}

{$ifdef __DIRECT_EXCEL__}

function TEasyOffice.Excel_Create: Boolean;
var loClassID : TCLSID;
begin

  if CLSIDFromProgID(PWideChar(WideString(csExcelApp)), loClassID)=S_OK then begin

    moExcel:=CreateOleObject(csExcelApp);

    {$ifdef __LAZARUS__}
    moExcel.Application.EnableEvents:=True;
    {$endif}

    moExcel.Visible:=True;
    mblValid:=True;
  end;
  Result:=mblValid;
end;

function TEasyOffice.Excel_GetSheetsCount() : Integer;
begin

  Result:=moExcel.WorkBooks.Item[1].WorkSheets.Count;
end;


function TEasyOffice.Excel_GetSheetName(piIdx : Integer) : String;
begin

  Result:=moExcel.WorkBooks.Item[1].WorkSheets.Item[piIdx].name
end;




procedure TEasyOffice.Excel_setVisible(pblVisible: Boolean);
begin

  moExcel.Visible:=pblVisible;
end;


procedure TEasyOffice.Excel_renameSheet(piSheetNumber : Integer; psNewName : String);
var loWorkSheet : OLEVariant;
    loWorkBook : OLEVariant;

begin
  loWorkBook:=moExcel.Workbooks[1];
  loWorkSheet:=loWorkBook.Worksheets[piSheetNumber+1];
  loWorkSheet.Activate;
  {$ifdef __DELPHI__}
  loWorkSheet.Name:=WideString(psNewName);
  {$else}
  loWorkSheet.Name:=WideString(UTF8ToSys(psNewName));
  {$endif}

  // ASheet: ExcelWorksheet;
  //liSheet.ConnectTo(moExcel.ActiveSheet as _Worksheet);
  //liSheet.Name:='����� ���';

  //liSheet:=moWorkbook.Worksheets.Get_Item(piSheetNumber+1);
  //liSheet.Name:=psNewName;

  //moExcel.WorkBooks[1].WorkSheets[piSheetNumber+1].setName(psNewName);
  //excel.WorkBooks[1].WorkSheets[1].Name := '�����1';
end;


//procedure TEasyoffice.Excel_writeToRange(psRange: String; poData: Variant);
procedure TEasyoffice.Excel_writeToStripeRange(piBeginCol,piBeginRow,piEndCol,piEndRow : Integer; poData: Variant);
var loRange : Variant;
begin

  loRange:=moSheet.Range[moSheet.Cells[piBeginCol,piBeginRow],moSheet.Cells[piEndCol,piEndRow]];
  loRange.Value:=poData;
end;

procedure TEasyOffice.Excel_save;
begin

  moWorkbook.Save;
end;


function teasyoffice.excel_get: variant;
begin

  Result := moExcel;
end;


function teasyoffice.excel_getSheet: variant;
begin

  Result:=moSheet;
end;


{$endif}

function TEasyOffice.Excel_GetExactValueFromCell(piCol, piRow : Integer) : Double;
var lsStr : String;
begin

  lsStr:=moSheet.Cells[Succ(piRow),Succ(piCol)].Text;
  lsStr:=FindAndDeleteAllEntries(Trim(lsStr),#32);
  Result:=StrToFloatDef(lsStr,0.00)
end;
{$EndRegion}

{$Region '_OpenOffice_'}
(*

function TEasyOffice.Delphi_OO_Create: Boolean;
begin

  {---< OpenOffice.org >---}
  if VarIsEmpty(moStarOffice) then begin

    moStarOffice:=CreateOleObject(csStarOfficeManager);
  end;

  if not (VarIsEmpty(moStarOffice) {or VarIsNull(moStarOffice)})  then begin

    if (VarType(moStarOffice)=varDispatch) then begin

      moStarDesktop := moStarOffice.createInstance(csStarOfficeDesktop);
      mblValid:=True;
    end;
  end;
  Result:=mblValid;
end;



function TEasyOffice.Lazarus_OO_Create: Boolean;
begin

  try

    moStarOffice:=CreateOleObject(csStarOfficeManager);
    moStarDesktop := moStarOffice.createInstance(csStarOfficeDesktop);
    mblValid:=True;
    Result:=True;
  except

    Result:=False;
  end;
end;


*)
function TEasyOffice.Delphi_OO_OpenSpreadSheet(psFileName: String): Boolean;
var  lvVariantArray : Variant;
begin

  lvVariantArray:=VarArrayCreate([0,0],varVariant);
  moDocument:=moStarDesktop.LoadComponentFromURl(ConvertToUrl(psFileName),'_blank',0,lvVariantArray);
  moSheets:=moDocument.getSheets;
  Result:=not (VarIsEmpty(moStarOffice) or VarIsNull(moStarOffice));
end;


function TEasyOffice.Lazarus_OO_OpenSpreadSheet(psFileName: String): Boolean;
var  lvVariantArray : Variant;
begin

  lvVariantArray:=VarArrayCreate([0,0],varVariant);
  moTextCursor:=moDocument.Text.CreateTextCursor;
  moTextCursor.InsertDocumentFromURL(ConvertToUrl(psFileName),lvVariantArray);
  Result:=True;
end;
{$Endregion}


function TEasyOffice.MakePropertyValue(PropName, PropValue:string):variant;
var Struct: variant;
begin
    Struct := moStarOffice.Bridge_GetStruct('com.sun.star.beans.PropertyValue');
    Struct.Name := PropName;
    Struct.Value := PropValue;
    Result := Struct;
end;


constructor TEasyOffice.Create(poOffice : TOfficeType);
var loClassID: TCLSID;
begin

  inherited Create;
  mblValid:=False;
  moOffice:=poOffice;
  miSheetNumber:=-1;

  {---< � ����� ������ �� ����� ����? >---}
  if moOffice=toOpenOffice then begin

    //***** Open Office/Libre Office. ������ ��� �� ������?
    if VarIsEmpty(moStarOffice) then
    begin

      //***** ������� ������
      {$ifdef __DELPHI__}
      moStarOffice := CreateOleObject(csStarOfficeManager);
      {$endif}

      {$ifdef __LAZARUS__}
      moStarOffice := CreateOleObject(WideString(csStarOfficeManager));
      {$endif}
    end;

    //***** ������� ��������� �����
    {$ifdef __DELPHI__}
    moStarDesktop := moStarOffice.createInstance(csStarOfficeDesktop);
    {$endif}

    {$ifdef __LAZARUS__}
    moStarDesktop := moStarOffice.createInstance(WideString(csStarOfficeDesktop));
    {$endif}
  end else begin

    //***** MS Office. �� ������ ����������?
    if CLSIDFromProgID(PWideChar(WideString(csExcelApp)), loClassID) = S_OK then
    begin

      //***** ������� ������
      {$ifdef __DELPHI__}
      moExcel := CreateOleObject(csExcelApp);
      {$endif}

      {$ifdef __LAZARUS__}
      moExcel := CreateOleObject(WideString(csExcelApp));
      moExcel.Application.EnableEvents := True;
      {$endif}
    end;
  end;
end;


procedure TEasyOffice.insertRow(piAfterWhichRow: Integer; piRowCount : Integer=1);
var loRange : Variant;
    lwsCell : WideString;
    liRow   : Integer;
begin

  if moOffice=toOpenOffice then begin

    moSheet.GetRows.InsertByIndex(piAfterWhichRow,piRowCount);
  end else begin

    //moSheet.Range[piAfterWhichRow].EntireRow.Insert(xlDown);
    //moExcel.ActiveSheet.Rows[].Insert
    lwsCell:=WideString('A'+IntToStr(piAfterWhichRow+1));
    loRange:=moWorkbook.ActiveSheet.Range[lwsCell, lwsCell].EntireRow;
    for liRow:=1 to piRowCount do
      loRange.Insert(xlShiftDown, Null);
  end;
end;


function TEasyOffice.IsValid : Boolean;
begin

  Result:=mblValid;
end;


function TEasyOffice.CreateNewSpreadSheet : Boolean;
begin

  Result:=True;
  if moOffice=toOpenOffice then begin
  
    moDocument:=moStarDesktop.LoadComponentFromURL(
      csStarOfficeCalcURL, '_blank', 0,VarArrayCreate([0, -1], varVariant));
    moMyStruct:=moStarOffice.Bridge_GetStruct(csStarOfficeStruct);
    moSheets:=moDocument.getSheets;
    Result:=not(VarIsEmpty(moStarOffice) or VarIsNull(moStarOffice));
  end else begin
  
    moExcel.WorkBooks.Add(EmptyParam);
  end;
end;


function TEasyOffice.OpenSpreadSheet(psFileName : String) : Boolean;
var lwsFileName    : WideString;
    lsName         : String;
    lvVariantArray : Variant;
begin
  
  if moOffice=toOpenOffice then begin

    {  vArr := VarArrayCreate([0, 2], varVariant);
  vArr[0] := MakePropertyValue('FilterName', 'MS Excel 97');
  vArr[1] := MakePropertyValue('Hidden', True);}
    lvVariantArray:=VarArrayCreate([0,0],varVariant);
    {
    lvVariantArray[1] := MakePropertyValue('Hidden', True);
    }
    //lvVariantArray[0]:=MakePropertyValue('FilterName', 'MS Word 97');

    {$ifdef __DELPHI__}
    moDocument:=moStarDesktop.LoadComponentFromURl(ConvertToUrl(psFileName),'_blank',0,lvVariantArray);
    {$endif}

    {$ifdef __LAZARUS__}
    lsName:=ConvertToUrl(psFileName);
    lwsFileName:=WideString(lsName);
    moDocument:=moStarDesktop.LoadComponentFromURl(lwsFileName,'_blank',0,lvVariantArray);
    {$endif}
    moSheets:=moDocument.getSheets;
    Result:=not (VarIsEmpty(moStarOffice) or VarIsNull(moStarOffice));
  end else begin
    {$ifdef __DELPHI__}
    moWorkBook:=moExcel.Workbooks.Open(psFileName);
    {$else}
    lwsFileName:=UTF8ToSys(psFileName);
    moWorkBook:=moExcel.Workbooks.Open(lwsFileName);
    {$endif}
    Result:=True;
  end;
  if Result then
    OpenSheetByIdx(0);
end;


procedure TEasyOffice.CloseDocument;
begin

  if moOffice=toOpenOffice then begin
  
    moDocument.Close(True);
    moDocument:=unassigned;
  end else begin
  
    moExcel.Workbooks.close;
  end;
end;


procedure TEasyOffice.Show;
begin

  if moOffice<>toOpenOffice then begin
    moExcel.Visible:=True;
  end
end;


procedure TEasyOffice.OpenSheetByIdx(piSheet : Integer);
begin
  if moOffice=toOpenOffice then begin

    miSheetNumber:=piSheet;
    moSheet:=moDocument.GetSheets.getByIndex(piSheet);
  end else begin

    miSheetNumber:=piSheet;
    miExcelSheet:=piSheet+1;
    moSheet:=moWorkbook.Worksheets[miExcelSheet];
  end
end;


procedure TEasyOffice.SaveDocument(const psFileName : String);
var lvVariantArray : Variant;
    //lsURL : String;
begin

  if moOffice=toOpenOffice then begin

    lvVariantArray:=VarArrayCreate([0, 0], varVariant);
    lvVariantArray[0] :=  MakePropertyValue('FilterName', 'MS Excel 97');
    //lvVariantArray[1] :=  MakePropertyValue('Overwrite','True');
    //lsURL:=ConvertToUrl(psFileName);
    //moDocument.StoreToURL(lsURL,lvVariantArray);
    moDocument.StoreToURL(ConvertToUrl(psFileName), lvVariantArray);


    //lvVariantArray := VarArrayCreate([0, 0], varVariant);
    //lvVariantArray[0] := MakePropertyValue('FilterName', 'MS Word 97');
     //Document.StoreToURL(FileName, VariantArray);
  end else begin
  
    //moExcel.Workbooks[1].SaveAs(psFileName);
    moWorkbook.SaveAs(psFileName);
  end;
end;


procedure TEasyOffice.WriteStringInCell(piCol,piRow : Integer; psValue : String);
begin

  if moOffice=toOpenOffice then begin
  
    moCell:=moSheet.GetCellByPosition(piCol,piRow);
    {$ifdef __DELPHI__}
    moCell.SetString(psValue);
    {$endif}
    {$ifdef __LAZARUS__}
    moCell.SetString(WideString(toSystemCodepage(psValue)));
    {$endif}
  end else begin
  
    //moExcel.Workbooks[1].WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)]:=psValue;
    //moWorkbook.WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)]:=psValue;
    {$ifdef __DELPHI__}
    moSheet.Cells[Succ(piRow),Succ(piCol)]:=psValue;
    {$else}
    moSheet.Cells[Succ(piRow),Succ(piCol)]:=WideString(UTF8ToSys(psValue));

    {$endif}
  end;
end;


procedure TEasyOffice.WriteValueInCell(piCol,piRow : Integer; pdValue : Double);
begin

  if moOffice=toOpenOffice then begin
  
    moCell:=moSheet.GetCellByPosition(piCol,piRow);
    moCell.SetValue(pdValue);
  end else begin
  
    //moExcel.Workbooks[1].WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)]:=pdValue;
    //moWorkbook.WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)]:=pdValue;
    //moWorkSheet[miExcelSheet].Cells[Succ(piRow),Succ(piCol)]:=pdValue;
    moSheet.Cells[Succ(piRow),Succ(piCol)]:=pdValue;
  end;
end;


procedure TEasyOffice.WriteValueInRange(psRange : String; pdValue : Double);
begin

  if moOffice=toOpenOffice then begin
  
    moSheet.GetRangeByName(psRange).SetValue(pdValue);
  end else begin
  
    //moExcel.Workbooks[1].WorkSheets[miExcelSheet].Range[psRange].Value:=pdValue;
    //moWorkbook.WorkSheets[miExcelSheet].Range[psRange].Value:=pdValue;
    moSheet.Range[psRange].Value:=pdValue;
  end;
end;


function TEasyOffice.GetValueFromCell(piCol,piRow : Integer) : Double;
begin
  if moOffice=toOpenOffice then begin

    moCell:=moSheet.GetCellByPosition(piCol,piRow);
    Result:=moCell.GetValue;
  end else begin

    //Result:=moExcel.Workbooks[1].WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)].Value;
    //Result:=moWorkbook.WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)].Value;
    Result:=moSheet.Cells[Succ(piRow),Succ(piCol)].Value;
  end;
end;


function TEasyOffice.GetExactValueFromCell(piCol, piRow: Integer): Double;
var lsValue : String;
begin

  if moOffice=toOpenOffice then begin

    moCell:=moSheet.GetCellByPosition(piCol,piRow);
    lsValue:=moCell.GetString;
    Result:=StrToFloatDef(lsValue,0.00);
  end else begin

    //Result:=moExcel.Workbooks[1].WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)].Value;
    //Result:=moWorkbook.WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)].Value;
    //Result:=moSheet.Cells[Succ(piRow),Succ(piCol)].Value;
    Result:=Excel_GetExactValueFromCell(piCol,piRow);
  end;
end;


function TEasyOffice.getStringFromCell(piCol,piRow : Integer) : String;
begin

  if moOffice=toOpenOffice then begin

    moCell:=moSheet.GetCellByPosition(piCol,piRow);
    Result:=moCell.GetString;
    //Value;
  end else begin

    //Result:=moExcel.Workbooks[1].WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)].Value;
    //Result:=moWorkbook.WorkSheets[miExcelSheet].Cells[Succ(piRow),Succ(piCol)].Value;
    //Result:=moWorkSheet.Cells[Succ(piRow),Succ(piCol)].Value;
    Result:=moSheet.Cells[Succ(piRow),Succ(piCol)].Text;
  end;

end;


function TEasyOffice.GetValueFromRange(psRange : String) : Double;
begin
  
  if moOffice=toOpenOffice then begin

    Result:=moSheet.GetRangeByName(psRange).Value;
  end else begin

    //Result:=moExcel.Workbooks[1].WorkSheets[miExcelSheet].Range[psRange].Value;
    //Result:=moWorkbook.WorkSheets[miExcelSheet].Range[psRange].Value;
    Result:=moSheet.Range[psRange].Value;
  end;
end;


procedure TEasyOffice.WriteStringsInCellRange(piStartCol, piStartRow : Integer; poList : TStringList);
var liIdx,
    liCol,
    liRow   : Integer;
    lsLine  : String;
begin

  liRow:=piStartRow;
  liCol:=piStartCol;
  for liIdx:=0 to Pred(poList.Count) do begin

    lsLine:=poList.Strings[liIdx];
    if moOffice=toOpenOffice then begin
  
      moCell:=moSheet.GetCellByPosition(liCol,liRow);
      moCell.SetString(lsLine);
    end else begin
    
      //moExcel.Workbooks[1].WorkSheets[miExcelSheet].Cells[liRow,liCol]:=lsLine;
      //moWorkbook.WorkSheets[miExcelSheet].Cells[liRow,liCol]:=lsLine;
      moSheet.Cells[liRow,liCol]:=lsLine;
    end;
    inc(liRow);
  end;
end;


function TEasyOffice.getRows: Integer;
var loCursor : Variant;
    loEndAddr : Variant;
begin

  if moOffice=toOpenOffice then begin

    loCursor:=moSheet.createCursor;
    loCursor.gotoEndOfUsedArea(true);
    loEndAddr:=loCursor.getRangeAddress;
    Result:=loEndAddr.EndRow;
  end
  else
  begin
    Result:=moExcel.ActiveSheet.UsedRange.Rows.Count;
  end;
end;


function TEasyOffice.getCols: Integer;
var loCursor : Variant;
    loEndAddr : Variant;
begin

  if moOffice=toOpenOffice then begin

    loCursor:=moSheet.createCursor;
    loCursor.gotoEndOfUsedArea(true);
    loEndAddr:=loCursor.getRangeAddress;
    Result:=loEndAddr.EndColumn;
  end
  else
  begin

    Result:=moExcel.ActiveSheet.UsedRange.Columns.Count;
  end;
end;


function TEasyOffice.getSheetNumber: Integer;
begin

  Result:=miSheetNumber;
end;


procedure TEasyOffice.setRowHeight(piRowIdx : Integer; prHeight : Real);
begin

  if moOffice=toOpenOffice then
  begin

    moSheet.getCellByPosition(0, piRowIdx).getRows.getByIndex(0).Height:=prHeight;
  end else begin

    moSheet.Rows.Item[piRowIdx+1].RowHeight:=prHeight;
  end;
end;


procedure TEasyOffice.selectRange(psCellBegin, psCellEnd: String);
begin

  //
end;

(*
procedure TEasyOffice.selectRange(psCellBegin, psCellEnd: String);
begin

  if moOffice=toOpenOffice then
  begin

    //moSheet.getCellByPosition(0, piRowIdx).getRows.getByIndex(0).Height:=prHeight;
    moSheet.getCellByRange[];
  end else begin

    moSheet.Range[WideString(psCellBegin),WideString(psCellEnd)].Select;
  end;
end;

*)

{    procedure TForm0.oo_cell_Border(ooRange:string; ooTop,ooBottom,ooLeft,ooRight,ooVertical,ooHorizontal:integer; oSheet:OLEVariant);
    Var allBorders, aBorder: Variant;
    begin
    allBorders := oSheet.getCellRangeByName(oorange).TableBorder;
    aBorder := allBorders.TopLine;
    //aBorder.Color := clBlack ;
    aBorder.OuterLineWidth := ooTop;
    allBorders.TopLine := aBorder;
    aBorder := allBorders.BottomLine;
    //aBorder.Color := clBlack ;
    aBorder.OuterLineWidth := ooBottom;
    allBorders.BottomLine:= aBorder;
    aBorder := allBorders.LeftLine;
    //aBorder.Color := clBlack ;
    aBorder.OuterLineWidth := ooLeft;
    allBorders.LeftLine := aBorder;
    aBorder := allBorders.RightLine;
    //aBorder.Color := clBlack ;
    aBorder.OuterLineWidth := ooRight;
    allBorders.RightLine := aBorder;
    aBorder := allBorders.VerticalLine;
    aBorder.Color := 0;
    aBorder.OuterLineWidth := ooVertical;
    allBorders.VerticalLine := aBorder;
    aBorder := allBorders.HorizontalLine;
    aBorder.Color := 0;
    aBorder.OuterLineWidth := ooHorizontal;
    allBorders.HorizontalLine := aBorder;
    allBorders.IsTopLineValid := True;
    allBorders.IsBottomLineValid := True;
    allBorders.IsLeftLineValid := True;
    allBorders.IsRightLineValid := True;
    allBorders.IsVerticalLineValid := True;
    allBorders.IsHorizontalLineValid := True;
    oSheet.getCellRangeByName(oorange).TableBorder:= allBorders;
    end;
}
procedure TEasyOffice.drawCellBorder(piCol, piRow, piBorder, {piLine,} piLineStyle, piWidth: Integer);
var loBorder, loCell : Variant;
begin

  //***** ����� ����� �� ��������������
  //***** ������� ������ 0 � 1
  if moOffice=toOpenOffice then
  begin

    loCell:=moSheet.getCellByPosition(piCol,piRow);
    loBorder:=moStarOffice.Bridge_GetStruct('com.sun.star.table.BorderLine');
    loBorder.color:=0;
    loBorder.lineDistance:= 0;
    loBorder.innerLineWidth:= 0;
    loBorder.outerLineWidth:=piWidth;
    //loBorder.LineStyle
    if (piBorder and ciEdgeBottom)>0 then
      loCell.bottomBorder:=loBorder;

    if (piBorder and ciEdgeTop)>0 then
      loCell.topBorder:=loBorder;

    if (piBorder and ciEdgeLeft)>0 then
      loCell.leftBorder:=loBorder;

    if (piBorder and ciEdgeRight)>0 then
      loCell.rightBorder:=loBorder;

    if (piBorder and ciEdgeBox)>0 then
    begin

      loCell.leftBorder:=loBorder;
      loCell.rightBorder:=loBorder;
      loCell.topBorder:=loBorder;
      loCell.bottomBorder:=loBorder;
    end;
    loBorder:=unAssigned;
    loCell:=unAssigned;
  end else begin

    if piWidth>0 then
      inc(piWidth); //� ������ 1 ������������� ����� �� �����.

    moSheet.Range[WideString(convertColAndRowToCellAddress(piCol,piRow))].Select;
    moExcel.Selection.Borders.LineStyle:=xlContinuous;

    if (piBorder and ciEdgeBottom)>0 then
      moExcel.Selection.Borders.Item[xlEdgeBottom].Weight:=piWidth;

    if (piBorder and ciEdgeTop)>0 then
      moExcel.Selection.Borders.Item[xlEdgeTop].Weight:=piWidth;

    if (piBorder and ciEdgeLeft)>0 then
      moExcel.Selection.Borders.Item[xlEdgeLeft].Weight:=piWidth;

    if (piBorder and ciEdgeRight)>0 then
      moExcel.Selection.Borders.Item[xlEdgeRight].Weight:=piWidth;

    if (piBorder and ciEdgeBox)>0 then
    begin
      moExcel.Selection.Borders.Item[xlEdgeBottom].Weight:=piWidth;
      moExcel.Selection.Borders.Item[xlEdgeLeft].Weight:=piWidth;
      moExcel.Selection.Borders.Item[xlEdgeRight].Weight:=piWidth;
      moExcel.Selection.Borders.Item[xlEdgeTop].Weight:=piWidth;
    end;

    {xlThin
    case piBorder of


    end;
    moExcel.Selection.Borders.LineStyle
    }
  end;
  {
  //loExcel.Selection.Borders.LineStyle:=ciCyanLinesStyle;
  //loExcel.Selection.Borders.Weight:=ciCyanLinesWeight;

ExcelApplication.Selection - ���������� ���������� ������. ���� ��� ������, �� ������������� � Range.
Range.Borders- ������� ������.
Range.Borders.Colour - ���� �������.
Range.borders.weight- ������.
range.borders.linestyle-����� �����
range.borders.item[index]-���� �� ������: index ����� ���� : xlDiagonalDown, xlDiagonalUp, xlEdgeBottom, xlEdgeLeft, xlEdgeRight, or xlEdgeTop, xlInsideHorizontal, or xlInsideVertical.
���������� Excel, ���� Alt-F11, ����� F1 � ������� �� ���...
IR.Borders.Item[xlEdgeBottom].LineStyle:=xlContinuous;
IR.Borders.Item[xlEdgeLeft].LineStyle:=xlContinuous;
IR.Borders.Item[xlEdgeRight].LineStyle:=xlContinuous;
IR.Borders.Item[xlEdgeTop].LineStyle:=xlContinuous;
if IR.Rows.Count>1 then IR.Borders.Item[xlInsideHorizontal].LineStyle:=xlContinuous;
if IR.Columns.Count>1 then IR.Borders.Item[xlInsideVertical].LineStyle:=xlContinuous;end;

  }
end;


function TEasyOffice.convertColAndRowToCellAddress(piCol, piRow : Integer): String;
begin

  inc(piRow);
  inc(piCol);
  Result:=casExcelCols[piCol]+IntToStr(piRow);
end;


destructor TEasyOffice.Destroy;
begin

  try

    if moOffice=toOpenOffice then begin

      moDocument.Close(True);
      moDocument:=unAssigned;
      moStarOffice:=Unassigned;
    end else begin

      if moExcel.Visible then
        moExcel.Visible:=false;
      moExcel.Quit;
      moExcel:=Unassigned;
    end;
  except

    // ������ �� ������
  end;
  inherited Destroy;
end;

{
OOCalc
Cell := Sheet.getCellByPosition(j, i);
 // Cell.charColor := 255;
  Cell.charHeight :=  14;
  Cell.CharWeight:= 150;
  Cell.charUnderline := true;
  Cell.charPosture := true;
  Cell.charStrikeout  := true;

  Cell := Sheet.getCellByPosition(j, i+1);
        //MyStruct.color := 255;
      MyStruct.lineDistance  := 0;
      MyStruct.innerLineWidth := 0;
      MyStruct.outerLineWidth := 1;
      Cell.leftBorder :=  MyStruct;
      Cell.rightBorder := MyStruct;
      Cell.topBorder := MyStruct;
      Cell.bottomBorder := MyStruct;

      !!!
      OuterBorder : variant;
      OuterBorder := StarOffice.Bridge_GetStruct('com.sun.star.table.BorderLine');
      OuterBorder.color := 155;
        OuterBorder.lineDistance  := 0;
        OuterBorder.innerLineWidth := 0;
        OuterBorder.outerLineWidth := 1;
        Cell.leftBorder :=  OuterBorder;
        Cell.rightBorder := OuterBorder;
        Cell.topBorder := OuterBorder;
        Cell.bottomBorder := OuterBorder;

      ].Border.Right.OuterLineWidth = 0)
       B.Color := 0;
      B.InnerLineWidth := 0;
      B.OuterLineWidth := 2;
      B.LineDistance := 0;

      Cell := Sheet.getCellByPosition(j, i+1);
         Cell := Sheet.getCellRangeByName('A4:D4').Merge(true);
          //MyStruct.color := 255;
        MyStruct.lineDistance  := 0;
        MyStruct.innerLineWidth := 0;
        MyStruct.outerLineWidth := 1;
        Cell.leftBorder :=  MyStruct;
        Cell.rightBorder := MyStruct;
        Cell.topBorder := MyStruct;
        Cell.bottomBorder := MyStruct;

  }

end.
