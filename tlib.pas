// @abstract ������ ��� ������ � ������� � ��������
// @author(���������� �.�. (http://www.icecorner.ru))

unit tlib;

{$X+}

interface


{$I profile.inc}
																																																																																{$I id.inc}
uses SysUtils,{Math,}Classes{,StrUtils},DateUtils//, Types


     {ifndef __LINUX__}
     //   ,windows
     //   ,twin
     {endif}

    {$ifdef __WINDOWS__}
        ,windows
        ,twin
    {$endif} // __WINDOWS__

    {$ifdef __LAZARUS__}
        ,forms
    {$endif}
    ,tsys
    ;


{$i tconst.inc}


{***** ����� ������� }
{** @abstract ������� �������� FillChar ��� ������� ������.
    @param(poBuffer : ����� ��� �������)
    @returns ������  }
procedure PurgeBuffer(var poBuffer);


{** @param(ppBuffer - �����, CRC �������� ����� ���������)
    @param(pdwByteCount - ������ ������)
    @param(pdwCRCValue - ������������ ��������)
    @link(CalcStringCRC32)
    @link(CalcFileCRC32)  }
procedure CalcCRC32(ppBuffer : Pointer; pdwByteCount : DWORD; var pdwCRCValue : DWORD);


{** @abstract ������� ������������ ����������� ����� ������. 
    @param(psLine - ������, CRC ������� ����� ���������)
    @param(pdwCRC32 - ������������ �������� ����������� �����)
    @returns(� ���� � ����, ��� ��� ���������� :] )
    @link(CalcCRC32)
    @link(CalcFileCRC32)  }
function CalcStringCRC32(psLine: String; var pdwCRC32: DWORD): Boolean;


function CalcStringHash(psLine : string) : DWord;


{** @abstract ������� ������������ ����������� ����� �����.
    @param(psFromName - ��� �����, CRC �������� ����� ���������)
    @param(pdwCRCvalue - ������������ �������� ����������� �����)
    @returns(@true � ������ ��������� ����������, ����� @false)
    @link(CalcCRC32)
    @link(CalcStringCRC32) }
function CalcFileCRC32(psFromName: String; var pdwCRCvalue: DWORD) : Boolean;


{===<< �������� ������� >>===}
{** @abstract ������� ����������, ���������� �� �������� �������.
    @param(psPath - ���� � ��������)
    @returns(@true, ���� ������� ����������, ����� @false) }
function IsFolderExists(psPath : String) : Boolean;


{** @abstract ������� ���������� ������ ���������� �����.
    @param(psName - ��� ����� � ����)
    @returns(������ �����) }
function GetFileSize(psName : String) : Cardinal;


{** @abstract ������� ���������� ������� �������� ������ �����/������
    @returns(@true, ���� ��� � �������, ����� @false) }
function IsIOError : Boolean;


{** @abstract ������� ������� ����� �������
    @param(psDirName - ���� � ��������)
    @returns(@true, ���� ������� ������� ������, ����� @false) }
function EasyMkDir(psDirName : String) : Boolean;


{** @abstract ������� ������� �������� ������� �� ������, �������� ������
    @param(psFolder - ������� ��� �������)
    @param(psMask - ����� ��������� ������, �� ��������� _QSQL*.DBF - ��������� �����, ��������� BDE)
    @returns(������ @true) }
function PurgeFolder(psFolder : String; psMask : String = '_QSQL*.DBF') : Boolean;

{** @abstract ������� ������������ ����������� �����
    @param(psFromFile - �������� ����)
    @param(psToFile - ������� ����)
    @returns(@true � ������ ��������� �����������, ����� @false) }
function EasyCopyFile(psFromFile,psToFile : String) : Boolean;


{** @abstract ������� ������������ �������� �����
    @param(psFromFile - �������� ����)
    @returns(@true � ������ ��������� �����������, ����� @false) }
function EasyDeleteFile(psFileName : String) : Boolean;


{** @abstract ������� ������������ ����� ����� �� �������� ����� � �������� ��������
    @param(psFolder - �������, � ������� ������ ����)
    @param(psMask - ����� �����, �� ��������� *.*)
    @param(piAttr - ������� �����, �� ��������� faAnyFile)
    @link EasyFindNext
    @link EasyFindClose
    @link EasyFindGetAttr
    @link EasyFindGainRec
    @link IsEasyFindOpen
    @returns(������ ��� �����, ���� �� ������, ����� ������ ������)  }
function EasyFindFirst(psFolder : String; psMask : String = '*.*'; piAttr : Integer = faAnyFile) : String;


{** @abstract ������� ���������� ����� �����, ������� EasyFindFirst
    @link EasyFindFirst
    @link EasyFindClose
    @link EasyFindGetAttr
    @link EasyFindGainRec
    @link IsEasyFindOpen
    @returns(������ ��� �����, ���� �� ������, ����� ������ ������) }
function EasyFindNext : String;


{** @abstract ������� ��������� �������� ������ �����, ������� EasyFindFirst
    @link EasyFindFirst
    @link EasyFindNext
    @link EasyFindGetAttr
    @link EasyFindGainRec
    @link IsEasyFindOpen
    @returns(@true, ���� �� �������� ������, ����� @false) }
function EasyFindClose : Boolean;


{** @abstract ������� ���������� �������� ���������� ���������� �����
    @link EasyFindFirst
    @link EasyFindNext
    @link EasyFindClose
    @link EasyFindGainRec
    @link IsEasyFindOpen
    @returns(�������� �����) }
function EasyFindGetAttr : Integer;


{** @abstract ������� ���������� ������ ���� TSearchRec � ����������� ���������� ���������� �����
    @link EasyFindFirst
    @link EasyFindNext
    @link EasyFindClose
    @link EasyFindGetAttr
    @link IsEasyFindOpen
    @returns(������ ���� TSearchRec) }
function EasyFindGainRec : TSearchRec;


{** @abstract ������� ���������� ��������� ������
    @link EasyFindFirst
    @link EasyFindNext
    @link EasyFindClose
    @link EasyFindGetAttr
    @link EasyFindGainRec
    @returns(@true, ���� ����� ������, ����� @false) }
function IsEasyFindOpen : Boolean;


{** @abstract ������� ������� ��������� ����� ��������
    @param(psFrom - ������� ��� ���������� �����������.)
    @param(psTo - �������, � ������� ����� ������ ����� � ��������� ������ ��������.)
    @param(psFolder - ��� �������� ��� ���������� ���������� ������. ����� ������ � �������� psTo�)
    @param(psBackupName - ��� ������������ ������.)
    @param(psFileList - ������������ ������ ������ ��� ���������� �����������. ���� ������ - ����� ���������� ���� �������.)
    @param(pblWait - ���� @true, �� ������ ��������� ����� �������������� �� ��������� ���������� ���������� �����������. �� ��������� @false)
    @param(psFileMask - ����� ������, ������� ����� �����������. �� ��������� *.*)
    @returns(@true � ������ ��������� ��������������, ����� @false)  }
{$ifdef __WINDOWS__}
function EasyBackupFolder(psFrom, psTo, psFolder, psBackupName : String; psFileList : String; pblWait : Boolean = False; psFileMask : String = '*.*') : Boolean;
{$endif}

{** @abstract ������� ���������� ���� �������� �����.
    @param(psFileName - ��� ����� )
    @returns(�������� ���� TDateTime)  }
function EasyFileAge(psFileName : String) : TDateTime;


{** @abstract ������� ������� ���� � ���������� � ���� ������.
    @param(psFileName - ��� ����� )
    @param(psContent - ������� ���������� �����, �� ��������� ������ ������)
    @returns(@true � ������ ��������� �������� �����, ����� @false)  }
function Touch(psFileName : String; psContent : String='') : Boolean;
{$ifdef __DELPHI__}
function ExtractFileFromResource(p_sResName,p_sFileName : String) : Boolean;
function EasyClipboardPasteStr : String;
procedure EasyClipboardCopyStr(psValue : String);

function EasyPack(ps7zPath,psSrcFile,psDstFile : String; piGrade : Integer=9) : Boolean;
function EasyUnPack(ps7zPath,psSrcFile,psPath : String) : Boolean;
{$endif}

procedure PastMonth(var piMonth,piYear : Integer);

function RoundEx(pdValue : Double) : Double;

function RoundEx2(X: Double; Y:Integer): Double;

function hex2int(psHex : string) : LongInt;


var g_sProgrammFolder : String;


implementation

uses
    {$ifndef __FP__}
    Clipbrd,
    {$endif}
    tstr;



var uoEasySearchRec : TSearchRec;
    ublEasySearchOpen : Boolean = False;


procedure PurgeBuffer(var poBuffer);
begin

  FillChar(poBuffer,sizeof(poBuffer),0);
end;


procedure CalcCRC32(ppBuffer : Pointer; pdwByteCount : DWORD; var pdwCRCValue : DWORD);
{ Use CalcCRC32 as a procedure so CRCValue can be passed in but
  also returned. This allows multiple calls to CalcCRC32 for
  the "same" CRC-32 calculation.
  The following is a little cryptic (but executes very quickly).
    The algorithm is as follows:
    1. exclusive-or the input byte with the low-order byte of
       the CRC register to get an INDEX
    2. shift the CRC register eight bits to the right
    3. exclusive-or the CRC register with the contents of Table[INDEX]
    4. repeat steps 1 through 3 for all bytes  }
var ldwIdx    : DWORD;
    lpBuffer  : ^BYTE;
begin

  lpBuffer:=ppBuffer;
  for ldwIdx:=0 to Pred(pdwByteCount) do begin

    pdwCRCvalue := (pdwCRCvalue shr 8) xor cadwTable[lpBuffer^ xor (pdwCRCvalue and $000000FF)];
    Inc(lpBuffer);
  end;
end; 


function CalcStringCRC32(psLine : string; var pdwCRC32 : DWORD) : Boolean;
var ldwCRC32Table : DWORD;
begin

  CalcStringCRC32:=False;
  ldwCRC32Table:=$FFFFFFFF;
  CalcCRC32(Addr(cadwTable[0]), SizeOf(cadwTable), ldwCRC32Table);
  ldwCRC32Table:=not ldwCRC32Table;

  if ldwCRC32Table=$6FCF9E13 then begin

    pdwCRC32:=$FFFFFFFF; // To match PKZIP
    if Length(psLine)>0 then  // Avoid access violation in D4
      CalcCRC32(Addr(psLine[1]), Length(psLine), pdwCRC32);

    pdwCRC32 := not pdwCRC32; // To match PKZIP
    CalcStringCRC32:=True
  end;
end;


function CalcStringHash(psLine : string) : DWord;
var ldwCRC32 : DWORD;
begin

  ldwCRC32:=$FFFFFFFF;
  if Length(psLine)>0 then
    CalcCRC32(Addr(psLine[1]), Length(psLine), ldwCRC32);
  Result:=ldwCRC32;
end;


function CalcFileCRC32(psFromName : string; var pdwCRCvalue : DWORD) : Boolean;
var loStream : TMemoryStream;
begin

  CalcFileCRC32:=True;
  pdwCRCValue:=$FFFFFFFF;
  loStream:=TMemoryStream.Create;
  
  try
    loStream.LoadFromFile(psFromName);
    if loStream.Size>0 then CalcCRC32(loStream.Memory,loStream.Size,pdwCRCvalue)
  except
    CalcFileCRC32:=False
  end;
  
  pdwCRCvalue:=not pdwCRCvalue
end;


{===<< �������� ������� >>===}
function IsFolderExists(psPath : String) : Boolean;
var loSearch : TSearchRec;
    liRes    : Integer;
    lsPath   : String;
begin

  lsPath:=psPath;
  SlashIt(lsPath);
  lsPath:=lsPath+'*.*';
  liRes:=SysUtils.FindFirst(lsPath,faDirectory,loSearch);
  IsFolderExists:=(liRes=0);
  SysUtils.FindClose(loSearch);
end; 


function GetFileSize(psName : String) : Cardinal;
var loRec : TSearchRec;
begin
  
  GetFileSize:=0;
  if SysUtils.FindFirst(psName,faAnyFile, loRec) = 0 then begin
    
    GetFileSize:=loRec.Size;
    SysUtils.FindClose(loRec);
  end; 
end; 


function IsIOError : Boolean;
begin

  IsIOError:=IOResult<>0;
end;


function EasyMkDir(psDirName : String) : Boolean;
begin
  
  if IsFolderExists(psDirName) then begin
    
    EasyMKDir:=True;
  end else begin

    MkDir(psDirName);
    EasyMkDir:=not IsIOError;
  end; {- if IsFolderExists(p_sDirName) then begin -}
end; {- EasyMkDir(p_sDirName : String) : Boolean; -}


function PurgeFolder(psFolder : String; psMask : String = '_QSQL*.DBF') : Boolean;
var loSearchRec  : TSearchRec;
    liRes        : Integer;
begin

  PurgeFolder:=True;
  SlashIt(psFolder);
  liRes:=SysUtils.FindFirst(psFolder+psMask, faAnyFile, loSearchRec);
  while liRes=0 do begin
    
    if (loSearchRec.Name<>'.') and (loSearchRec.Name<>'..')then begin

      EasyDeleteFile(psFolder+loSearchRec.Name);
    end;
    liRes:=SysUtils.FindNext(loSearchRec);
  end;
  SysUtils.FindClose(loSearchRec);
end;


function EasyCopyFile(psFromFile,psToFile : String) : Boolean;
{$ifndef __WINDOWS__} var loSource, loTarget : TStream; {$endif}
begin

  if FileExists(psFromFile) then begin
    {$ifndef __WINDOWS__}
    try
      loSource:=TFileStream.Create(psFromFile, fmOpenRead);
      loTarget:=TFileStream.Create(psToFile, fmCreate);
      loTarget.CopyFrom(loSource,GetFileSize(psFromFile));
      EasyCopyFile:=not IsIOError;
    except
      EasyCopyFile:=False;
    end;
    FreeAndNil(loSource);
    FreeAndNil(loTarget);
    {$else}
    Result:=CopyFile(PChar(psFromFile),PChar(psToFile),False);
    {$endif}
  end else begin
    EasyCopyFile:=False;
  end;
end;


function EasyDeleteFile(psFileName : String) : Boolean;
begin

  if FileExists(psFileName) then begin

    EasyDeleteFile:=SysUtils.DeleteFile(psFileName);

  end else begin
  
    EasyDeleteFile:=False;
  end;
end;


function EasyFindFirst(psFolder : String; psMask : String = '*.*'; piAttr : Integer = faAnyFile) : String;
var lsFilename : String;
begin

  EasyFindFirst:='';
  if not ublEasySearchOpen then begin

    SlashIt(psFolder);
    if FindFirst(psFolder+psMask,piAttr,uoEasySearchRec)=0 then begin

      lsFileName:=uoEasySearchRec.Name;
      ublEasySearchOpen:=True;
      while (lsFileName='.') or (lsFileName='..') do begin

        lsFileName:=EasyFindNext;
      end;

      if not IsIOError then begin

        EasyFindFirst:=lsFileName;
      end;
    end;
  end;
end;


function EasyFindNext : String;
begin

  EasyFindNext:='';
  if ublEasySearchOpen then begin

    if FindNext(uoEasySearchRec)=0 then begin

      if not IsIOError then begin

        EasyFindNext:=uoEasySearchRec.Name;
      end;
    end;
  end;
end;


function EasyFindClose : Boolean;
begin

  EasyFindClose:=True;
  if ublEasySearchOpen then begin

    SysUtils.FindClose(uoEasySearchRec);
    if not IsIOError then begin

      EasyFindClose:=True;
      ublEasySearchOpen:=False;
    end;
  end;
end;


function EasyFindGetAttr : Integer;
begin

  EasyFindGetAttr:=uoEasySearchRec.Attr;
end;


function EasyFindGainRec : TSearchRec;
begin

  EasyFindGainRec:=uoEasySearchRec;
end;


function IsEasyFindOpen : Boolean;
begin

  IsEasyFindOpen:=ublEasySearchOpen;
end;

{$ifdef __WINDOWS__}
function EasyBackupFolder(psFrom, psTo, psFolder, psBackupName : String; psFileList : String; pblWait : Boolean = False; psFileMask : String = '*.*') : Boolean;
var lsCmdLine  : String;
    lsBakName  : String;
    lsFileName : String;
    lblOk      : Boolean;
begin
  EasyBackupFolder:=True;
  SlashIt(psFrom);
  SlashIt(psTo);
  SlashIt(psFolder);
  lblOk:=True;
  psFileList:=UpperCase(psFileList);

  //***** ��������  �����
  if EasyMkDir(psTo+psFolder) then begin

    //***** ������� ��� ����� � ��������
    lsFileName:=UpperCase(EasyFindFirst(psFrom,psFileMask));
    while (lsFileName<>'') and lblOk do begin

      //***** ���� ���������� ������ �� ����..
      if not IsEmpty(psFileList) then begin

        //***** ���� ���� � ������
        if Pos(lsFileName,psFileList)>0 then begin

          //***** �����? �������� ���.
          lblOk:=EasyCopyFile(psFrom+lsFileName,psTo+psFolder+lsFileName);
        end;
      end else begin

        //***** �������� ���� �������
        lblOk:=EasyCopyFile(psFrom+lsFileName,psTo+psFolder+lsFileName);
      end;
      lsFileName:=UpperCase(EasyFindNext);
    end;

    //***** ��������� �����
    if EasyFindClose then begin

      //***** ��� �� � ������� �� �����?
      if lblOk then begin

        //***** ��������� ��������� ������
        lsCmdLine:=' -9 -r -D -j '+psTo+psBackupName+' '+psTo+psFolder+' >archive.log';
        lsBakName:=psTo+ExtractFileName(psBackupName)+csBakExt;

        //***** ��� �� ��� ������ �����?
        if FileExists(psTo+psBackupName) then begin

          //***** ����. � ��� �� ������ ��, �� � ����������? �� ���� .bak?
          if FileExists(lsBakName) then begin

            //***** ����. � ������� .bak?
            if EasyDeleteFile(lsBakName) then begin

              //***** ��������. ��������������� ��������� ���� � .bak
              if RenameFile(psTo+psBackupName,lsBakName) then begin

                //***** ��������������
              end else begin

                //***** �� ��������������. �������� ��������.
              end;
            end else begin

              //***** �� �������� .bak - �� ���������������. ������ ������
            end;
          end else begin

            //***** �������� ���������������
            if RenameFile(psTo+psBackupName,lsBakName) then begin

              //***** ��������������
            end else begin

              //***** �� ��������������. �� � ��� � ���. ��������.
            end;
          end;
        end;

        //***** ���������� �� ������� �����?
        if IsFolderExists(ChopTail(psTo)) then
        begin

          //***** ��������� �������� ������
          Result:=EasyExec(g_sProgrammFolder+csDefaultArc,lsCmdLine,psTo+psFolder,True);
          if not Result then begin

            //***** �������...
            Notice('','���������� ��������� �������� ��������� ����� ��!');
            //***** �� ����������. ��������������� .bak �����
            if FileExists(lsBakName) then begin

              if RenameFile(lsBakName,psTo+psBackupName) then begin

                //***** ��������������
              end else begin

                //***** �� ��������������. �������� ��������.
              end;
            end;
          end else begin

            //***** ����������
            EasyDeleteFile(lsBakName);
            // !!! ������� ���������� �����
            //RmDir(p_sTo+p_sFolder);
          end;
        end
        else
        begin

          //***** �������...
          Notice('','�� ������� ����� ��� ��������� ����� '+psTo);
        end; {- if FolderExists(DelLastChar(p_sTo)) then/else -}
      end
      else
      begin
        //***** �������...
        Notice('','���������� ����������� ����� � '+psTo);
      end;
    end;
  end;
end;
{$endif}

function EasyFileAge(psFileName : String) : TDateTime;
var ldtDateTime : TDateTime;
begin

  {$ifdef __LAZARUS__}
  ldtDateTime:=FileAge(psFileName); // �����, �� ��������
  {$else}
  FileAge(psFileName,ldtDateTime);
  {$endif}
  EasyFileAge:=ldtDateTime;
end;


function Touch(psFileName : String; psContent : String='') : Boolean;
var lfFile : Text;
begin

  Result:=False;
  AssignFile(lfFile,psFileName);
  {$i-}
  Rewrite(lfFile);
  if not IsIOError then
  begin

    writeln(lfFile,psContent);
    Result:=not IsIOError;
    CloseFile(lfFile);
  end;
end;

{$ifdef __DELPHI__}
function ExtractFileFromResource(p_sResName,p_sFileName : String) : Boolean;
var l_oResStream: TResourceStream;
    l_oFileStream: TFileStream;
begin
  Result:=True;
  try
    l_oResStream:=TResourceStream.Create(hInstance, p_sResName, RT_RCDATA) ;
    l_oFileStream:=TFileStream.Create(p_sFileName, fmCreate);
    l_oFileStream.CopyFrom(l_oResStream, 0) ;
  except
    Result:=False;
  end;
  FreeAndNil(l_oFileStream);
  FreeAndNil(l_oResStream);
end;

{$endif}
procedure pastMonth(var piMonth, piYear: Integer);
begin

  dec(piMonth);
  if piMonth<ciJanuary then begin

    piMonth:=ciDecember;
    dec(piYear);
  end;
end;


{$ifdef __DELPHI__}
procedure EasyClipboardCopyStr(psValue : String);
var loClip : TClipBoard;
begin

  loCLip:=TClipBoard.Create;
  loClip.SetTextBuf(PChar(psValue));
  FreeAndNil(loClip);
end;


function EasyClipboardPasteStr : String;
var loClip : TClipBoard;
begin

  loCLip:=TClipBoard.Create;
  if loClip.HasFormat(CF_TEXT) then
    Result:=loClip.AsText
  else
    Result:='';
  FreeAndNil(loClip);
end;
function EasyPack(ps7zPath,psSrcFile,psDstFile : String; piGrade : Integer=9) : Boolean;
begin

  Result:=EasyExecAndWait(ps7zPath,' a -mx='+IntToStr(piGrade)+' "'+psDstFile+'" "'+psSrcFile+'"',
    ExtractFilePath(psDstFile){,False})>-1;
  //Result:=EasyRun(ps7zPath,' a -mx='+IntToStr(piGrade)+' "'+psDstFile+'" "'+psSrcFile+'"',True,True);
end;


function EasyUnPack(ps7zPath,psSrcFile,psPath : String) : Boolean;
begin

  Result:=EasyExecAndWait(ps7zPath,' e "'+psSrcFile+'" -o"'+psPath+'"','')>-1;
//tools\7z.exe e "������ ��������� 26.04.2010.7z" -o"D:\WORK\SRC\DELPHI\FBCOURTS\DOC\
end;
{$endif}


function roundEx(pdValue : Double) : Double;
var ldScaledFractPart,
    ldValue1         : Double;
    liValue2         : integer;
begin

  ldScaledFractPart:=Frac(pdValue)*100;
  ldValue1:=Frac(ldScaledFractPart);
  ldScaledFractPart:=Int(ldScaledFractPart);
  liValue2:=Round(ldValue1*10);
  if liValue2>=5 then begin

    ldScaledFractPart:=ldScaledFractPart+1;
  end;
  if liValue2<=-5 then begin

    ldScaledFractPart := ldScaledFractPart - 1;
  end;
  RoundEx := Int(pdValue) + ldScaledFractPart/100;
end;


function DecMonth(pdtDate : TDateTime; pwMonths : Word) : TDateTime;
var lwYear, lwMonth, lwDay, lwDeltaYear : Word;
begin

  DecodeDate(pdtDate,lwYear,lwMonth,lwDay);

  //***** ������� ������� ��� ������?
  lwDeltaYear:=pwMonths div 12;
  if lwDeltaYear=0 then begin

    //***** ������ ����
    lwMonth:=lwMonth-pwMonths;
  end else begin

    //***** ������ ����
    lwYear:=lwYear-lwDeltaYear;
    lwMonth:=lwMonth-(pwMonths mod 12);
  end;
  Result:=EncodeDate(lwYear,lwMonth,lwDay);
end;


function RoundEx2(X: Double; Y:Integer): Double;
var ScaledFractPart,
    T1: Double;
    T2:integer;
    I:Double;
begin
    I:=exp(Y*ln(10));
    ScaledFractPart := Frac(X) * ROUND(I);
    T1 := Frac(ScaledFractPart);
    ScaledFractPart := Int(ScaledFractPart);
    T2:=round(T1*10);
    if  ( T2 >= 5 ) then
    begin
      ScaledFractPart := ScaledFractPart + 1;
    end;
    if (T2 <= -5) then
    begin
      ScaledFractPart := ScaledFractPart - 1;
    end;
    RoundEx2 := Int(X) + ScaledFractPart / ROUND(I);
end;


function hex2int(psHex : string) : LongInt;
begin

  if (psHex <> '') and (psHex[1] <> '$') then
    result := strToInt('$' + psHex)
  else
    result := strToInt(psHex);
end;



begin
  {$ifdef __LAZARUS__}
    g_sProgrammFolder:=ExtractFileDir(Application.Exename);
  {$else}
    g_sProgrammFolder:=ExtractFileDir(ParamStr(0));
  {$endif}
  SlashIt(g_sProgrammFolder);
end.





