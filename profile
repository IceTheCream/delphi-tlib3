{define __LAZARUS__}
{define __DELPHI__}
{define __VP__}
{define __BP__}
{define __FP__}
{define __WINDOWS__}
{define __LINUX__}
{define __DOS__}
{define __CLASSES__}


{$ifndef __LINUX__}
    {$ifndef __DOS__}
        {$undef __BP__}
        {$define __WINDOWS__}
        {$ifndef __FP__}
            {$ifndef __VP__}
                {$define __DELPHI__}
                {$define __NOXML__}
            {$else}
                {$undef __DELPHI__}
            {$endif}
        {$else}
        {$define __LAZARUS__}
        {$endif}
    {$else}
        {$define __BP__}
    {$endif}    
{$else}
    {$undef __WINDOWS__}
    {$undef __BP__}
    {$define __FP__}
    {define __LAZARUS__}
{$endif}
