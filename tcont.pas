unit tcont;


interface


uses SysUtils,Classes;

{$ifdef __VP__}


{$else}
type TUStackType = (stUndefined,stString,stLongint,stBoolean,stFloat);

     EStackError = class of EInOutError;

     TUStack = class(TObject)
       private
         m_oStack : TStringList;
         m_oType  : TUStackType;
       public
         constructor Create;
         destructor Destroy; override;
         procedure PushStr(p_sLine : String);
         function PopStr : String;
         function Top : Integer;
         function Empty : Boolean;
     end;
{$endif}

implementation

{ TUStack }

{$ifndef __VP__}

constructor TUStack.Create;
begin
  m_oStack:=TStringList.Create;
  m_oType:=stUndefined;
end;


destructor TUStack.Destroy;
begin
  FreeAndNil(m_oStack);
end;


function TUStack.Empty: Boolean;
begin
  Result:=Top<0;
end;

function TUStack.Top: Integer;
begin
  Result:=Pred(m_oStack.Count);
end;


function TUStack.PopStr: String;
begin
  if m_oType=stString then
  begin
    if Top>=0 then begin
      Result:=m_oStack.Strings[Top];
      m_oStack.Delete(Top);
    end
    else
    begin
      Raise EStackError.Create('UStack: �⥪ ���௠�!');
    end;
  end
  else
  begin
    Raise EStackError.Create('UStack: �訡�� ⨯��');
  end;
end;


procedure TUStack.PushStr(p_sLine: String);
begin
  if m_oType=stUndefined then
    m_oType:=stString;
  if m_oType=stString then
    m_oStack.Add(p_sLine)
  else Raise EStackError.Create('UStack: �訡�� ⨯��');
end;

{$endif}

end.
